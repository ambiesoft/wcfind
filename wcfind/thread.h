#pragma once

#include "../../lsMisc/stdosd/OpParser.h"
#include "../common/WcfindWord.h"
#include "../common/CLParse.h"
#include "ThreadResults.h"

enum {
	WM_APP_SEARCHHIT_OBSOLETE = (WM_APP + 1),
	WM_APP_SEARCHERROR,
	WM_APP_SEARCHPROGRESS,
	WM_APP_SEARCHFINISHED,
	WM_APP_AFTERENDEDIT,
	WM_APP_CLOSE,
};



class CThreadResults;
class CThreadArg {
private:
	HWND hWnd_;
	int workId_;
	WCOpParserType opParser_;
	CThreadResults& results_;
	HANDLE hRun_;
public:
	CThreadArg(const WCOpParserType& opParser, CThreadResults& results, HANDLE hRun) : 
		opParser_(opParser),
		results_(results),
		hRun_(hRun) {
		m_tProcessStart=time(NULL);
	}
	WCOpParserType& opParser() {
		return opParser_;
	}
	int workId() const {
		return workId_;
	}
	void SetWorkID(int id) {
		workId_ = id;
	}
	HWND hWnd() const {
		return hWnd_;
	}
	void SetHwnd(HWND h) {
		hWnd_ = h;
	}

	time_t m_tProcessStart;
	vector<wstring> vFolders_;
	
	void InsertResult(const std::shared_ptr<CLVData>& data);

	HANDLE GetRunEvent() const {
		return hRun_;
		ASSERT(hRun_);
	}
};

unsigned  __stdcall startOfHeadSearch(void * p);