﻿#if !defined(AFX_ERRORDIALOG_H__20F43C47_8D14_4A86_B0BC_338E3D62C52A__INCLUDED_)
#define AFX_ERRORDIALOG_H__20F43C47_8D14_4A86_B0BC_338E3D62C52A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CLVData;
class CErrorDialog : public CResizableDialog
{
	int count_ = 0;
public:
	CErrorDialog(CWnd* pParent = NULL);

	//{{AFX_DATA(CErrorDialog)
	enum { IDD = IDD_DIALOG_ERROR };
		// 
	//}}AFX_DATA


	// ClassWizard
	//{{AFX_VIRTUAL(CErrorDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CErrorDialog)
		// 
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();

public:
	void Add(CLVData& lvData);
	int GetErrorCount() const;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++

#endif // !defined(AFX_ERRORDIALOG_H__20F43C47_8D14_4A86_B0BC_338E3D62C52A__INCLUDED_)
