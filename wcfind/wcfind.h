
#if !defined(AFX_WCFIND_H__E5C8B512_F1F6_4830_BAB2_9056D88432DF__INCLUDED_)
#define AFX_WCFIND_H__E5C8B512_F1F6_4830_BAB2_9056D88432DF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "../../lsMisc/stdosd/OpParser.h"

#include "../common/CLParse.h"
#include "../../lsMisc/Logging.h"

class CChildView;
class CWcfindApp : public CWinApp
{
public:
	CWcfindApp();

	// ClassWizard
	//{{AFX_VIRTUAL(CWcfindApp)
	public:
	virtual BOOL InitInstance();
	virtual BOOL InitApplication();
	//}}AFX_VIRTUAL

public:
	//{{AFX_MSG(CWcfindApp)
	afx_msg void OnAppAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


private:
	CCLParse m_clParse;
	CTime m_timeStart;

	Ambiesoft::Logging* m_pLogging;

private:
	BOOL PreInit();
public:
	CString GetTitleFromParser()
	{
		return m_clParse.getTitle();
	}
	void WriteLog(LPCTSTR p)
	{
		if (!m_pLogging)
			return;
		m_pLogging->Write(p);
	}
	const CStringArray& getFolders() {
		return m_clParse.getFolders();
	}

	void UsageMessageBox();

	virtual int ExitInstance();

	CString getSortColumn() const {
		return m_clParse.m_strSortColumn;
	}
	SORTMODE getSortMode() const {
		return m_clParse.m_sortMode;
	}

	const WCOpParserType& getOpParser() {
		return m_clParse.opParser_;
	}

	bool closed_ = false;
	bool IsClosed() const {
		return closed_;
	}
	void SetClosed() {
		closed_ = true;
	}
	CChildView* m_pChildView = nullptr;
	void SetChildViewForIdle(CChildView* pView) {
		m_pChildView = pView;
	}
	virtual BOOL OnIdle(LONG lCount);
	afx_msg void OnFileCloseallinstances();
	
	void CloseApp();
	afx_msg void OnFileCloseotherinstances();
	void DoCloseOtherInstances();
};

extern CWcfindApp theApp;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++

#endif // !defined(AFX_WCFIND_H__E5C8B512_F1F6_4830_BAB2_9056D88432DF__INCLUDED_)
