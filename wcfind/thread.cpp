#include "stdafx.h"
#include <string.h>

#include <memory>

#include "../../lsMisc/UTF16toUTF8.h"
#include "../../lsMisc/MappedFile.h"

#include "thread.h"
#include "LVData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

volatile int gworkId;

static UINT sCodePages[] = {
	// Identifier		.NET Name	Additional information
	65001	,    //	utf-8	Unicode (UTF-8)
	932	,    //	shift_jis	ANSI/OEM Japanese; Japanese (Shift-JIS)
	51932	,    //	euc-jp	EUC Japanese
	20932	,    //	EUC-JP	Japanese (JIS 0208-1990 and 0212-1990)
	50220	,    //	iso-2022-jp	ISO 2022 Japanese with no halfwidth Katakana; Japanese (JIS)
	65000	,    //	utf-7	Unicode (UTF-7)

	37	,    //	IBM037	IBM EBCDIC US-Canada
	437	,    //	IBM437	OEM United States
	500	,    //	IBM500	IBM EBCDIC International
	708	,    //	ASMO-708	Arabic (ASMO 708)
	709	,    //		Arabic (ASMO-449+, BCON V4)
	710	,    //		Arabic - Transparent Arabic
	720	,    //	DOS-720	Arabic (Transparent ASMO); Arabic (DOS)
	737	,    //	ibm737	OEM Greek (formerly 437G); Greek (DOS)
	775	,    //	ibm775	OEM Baltic; Baltic (DOS)
	850	,    //	ibm850	OEM Multilingual Latin 1; Western European (DOS)
	852	,    //	ibm852	OEM Latin 2; Central European (DOS)
	855	,    //	IBM855	OEM Cyrillic (primarily Russian)
	857	,    //	ibm857	OEM Turkish; Turkish (DOS)
	858	,    //	IBM00858	OEM Multilingual Latin 1 + Euro symbol
	860	,    //	IBM860	OEM Portuguese; Portuguese (DOS)
	861	,    //	ibm861	OEM Icelandic; Icelandic (DOS)
	862	,    //	DOS-862	OEM Hebrew; Hebrew (DOS)
	863	,    //	IBM863	OEM French Canadian; French Canadian (DOS)
	864	,    //	IBM864	OEM Arabic; Arabic (864)
	865	,    //	IBM865	OEM Nordic; Nordic (DOS)
	866	,    //	cp866	OEM Russian; Cyrillic (DOS)
	869	,    //	ibm869	OEM Modern Greek; Greek, Modern (DOS)
	870	,    //	IBM870	IBM EBCDIC Multilingual/ROECE (Latin 2); IBM EBCDIC Multilingual Latin 2
	874	,    //	windows-874	ANSI/OEM Thai (ISO 8859-11); Thai (Windows)
	875	,    //	cp875	IBM EBCDIC Greek Modern
	
	936	,    //	gb2312	ANSI/OEM Simplified Chinese (PRC, Singapore); Chinese Simplified (GB2312)
	949	,    //	ks_c_5601-1987	ANSI/OEM Korean (Unified Hangul Code)
	950	,    //	big5	ANSI/OEM Traditional Chinese (Taiwan; Hong Kong SAR, PRC); Chinese Traditional (Big5)
	1026	,    //	IBM1026	IBM EBCDIC Turkish (Latin 5)
	1047	,    //	IBM01047	IBM EBCDIC Latin 1/Open System
	1140	,    //	IBM01140	IBM EBCDIC US-Canada (037 + Euro symbol); IBM EBCDIC (US-Canada-Euro)
	1141	,    //	IBM01141	IBM EBCDIC Germany (20273 + Euro symbol); IBM EBCDIC (Germany-Euro)
	1142	,    //	IBM01142	IBM EBCDIC Denmark-Norway (20277 + Euro symbol); IBM EBCDIC (Denmark-Norway-Euro)
	1143	,    //	IBM01143	IBM EBCDIC Finland-Sweden (20278 + Euro symbol); IBM EBCDIC (Finland-Sweden-Euro)
	1144	,    //	IBM01144	IBM EBCDIC Italy (20280 + Euro symbol); IBM EBCDIC (Italy-Euro)
	1145	,    //	IBM01145	IBM EBCDIC Latin America-Spain (20284 + Euro symbol); IBM EBCDIC (Spain-Euro)
	1146	,    //	IBM01146	IBM EBCDIC United Kingdom (20285 + Euro symbol); IBM EBCDIC (UK-Euro)
	1147	,    //	IBM01147	IBM EBCDIC France (20297 + Euro symbol); IBM EBCDIC (France-Euro)
	1148	,    //	IBM01148	IBM EBCDIC International (500 + Euro symbol); IBM EBCDIC (International-Euro)
	1149	,    //	IBM01149	IBM EBCDIC Icelandic (20871 + Euro symbol); IBM EBCDIC (Icelandic-Euro)
	1200	,    //	utf-16	Unicode UTF-16, little endian byte order (BMP of ISO 10646); available only to managed applications
	1201	,    //	unicodeFFFE	Unicode UTF-16, big endian byte order; available only to managed applications
	1250	,    //	windows-1250	ANSI Central European; Central European (Windows)
	1251	,    //	windows-1251	ANSI Cyrillic; Cyrillic (Windows)
	1252	,    //	windows-1252	ANSI Latin 1; Western European (Windows)
	1253	,    //	windows-1253	ANSI Greek; Greek (Windows)
	1254	,    //	windows-1254	ANSI Turkish; Turkish (Windows)
	1255	,    //	windows-1255	ANSI Hebrew; Hebrew (Windows)
	1256	,    //	windows-1256	ANSI Arabic; Arabic (Windows)
	1257	,    //	windows-1257	ANSI Baltic; Baltic (Windows)
	1258	,    //	windows-1258	ANSI/OEM Vietnamese; Vietnamese (Windows)
	1361	,    //	Johab	Korean (Johab)
	10000	,    //	macintosh	MAC Roman; Western European (Mac)
	10001	,    //	x-mac-japanese	Japanese (Mac)
	10002	,    //	x-mac-chinesetrad	MAC Traditional Chinese (Big5); Chinese Traditional (Mac)
	10003	,    //	x-mac-korean	Korean (Mac)
	10004	,    //	x-mac-arabic	Arabic (Mac)
	10005	,    //	x-mac-hebrew	Hebrew (Mac)
	10006	,    //	x-mac-greek	Greek (Mac)
	10007	,    //	x-mac-cyrillic	Cyrillic (Mac)
	10008	,    //	x-mac-chinesesimp	MAC Simplified Chinese (GB 2312); Chinese Simplified (Mac)
	10010	,    //	x-mac-romanian	Romanian (Mac)
	10017	,    //	x-mac-ukrainian	Ukrainian (Mac)
	10021	,    //	x-mac-thai	Thai (Mac)
	10029	,    //	x-mac-ce	MAC Latin 2; Central European (Mac)
	10079	,    //	x-mac-icelandic	Icelandic (Mac)
	10081	,    //	x-mac-turkish	Turkish (Mac)
	10082	,    //	x-mac-croatian	Croatian (Mac)
	12000	,    //	utf-32	Unicode UTF-32, little endian byte order; available only to managed applications
	12001	,    //	utf-32BE	Unicode UTF-32, big endian byte order; available only to managed applications
	20000	,    //	x-Chinese_CNS	CNS Taiwan; Chinese Traditional (CNS)
	20001	,    //	x-cp20001	TCA Taiwan
	20002	,    //	x_Chinese-Eten	Eten Taiwan; Chinese Traditional (Eten)
	20003	,    //	x-cp20003	IBM5550 Taiwan
	20004	,    //	x-cp20004	TeleText Taiwan
	20005	,    //	x-cp20005	Wang Taiwan
	20105	,    //	x-IA5	IA5 (IRV International Alphabet No. 5, 7-bit); Western European (IA5)
	20106	,    //	x-IA5-German	IA5 German (7-bit)
	20107	,    //	x-IA5-Swedish	IA5 Swedish (7-bit)
	20108	,    //	x-IA5-Norwegian	IA5 Norwegian (7-bit)
	20127	,    //	us-ascii	US-ASCII (7-bit)
	20261	,    //	x-cp20261	T.61
	20269	,    //	x-cp20269	ISO 6937 Non-Spacing Accent
	20273	,    //	IBM273	IBM EBCDIC Germany
	20277	,    //	IBM277	IBM EBCDIC Denmark-Norway
	20278	,    //	IBM278	IBM EBCDIC Finland-Sweden
	20280	,    //	IBM280	IBM EBCDIC Italy
	20284	,    //	IBM284	IBM EBCDIC Latin America-Spain
	20285	,    //	IBM285	IBM EBCDIC United Kingdom
	20290	,    //	IBM290	IBM EBCDIC Japanese Katakana Extended
	20297	,    //	IBM297	IBM EBCDIC France
	20420	,    //	IBM420	IBM EBCDIC Arabic
	20423	,    //	IBM423	IBM EBCDIC Greek
	20424	,    //	IBM424	IBM EBCDIC Hebrew
	20833	,    //	x-EBCDIC-KoreanExtended	IBM EBCDIC Korean Extended
	20838	,    //	IBM-Thai	IBM EBCDIC Thai
	20866	,    //	koi8-r	Russian (KOI8-R); Cyrillic (KOI8-R)
	20871	,    //	IBM871	IBM EBCDIC Icelandic
	20880	,    //	IBM880	IBM EBCDIC Cyrillic Russian
	20905	,    //	IBM905	IBM EBCDIC Turkish
	20924	,    //	IBM00924	IBM EBCDIC Latin 1/Open System (1047 + Euro symbol)
	
	20936	,    //	x-cp20936	Simplified Chinese (GB2312); Chinese Simplified (GB2312-80)
	20949	,    //	x-cp20949	Korean Wansung
	21025	,    //	cp1025	IBM EBCDIC Cyrillic Serbian-Bulgarian
	21027	,    //		(deprecated)
	21866	,    //	koi8-u	Ukrainian (KOI8-U); Cyrillic (KOI8-U)
	28591	,    //	iso-8859-1	ISO 8859-1 Latin 1; Western European (ISO)
	28592	,    //	iso-8859-2	ISO 8859-2 Central European; Central European (ISO)
	28593	,    //	iso-8859-3	ISO 8859-3 Latin 3
	28594	,    //	iso-8859-4	ISO 8859-4 Baltic
	28595	,    //	iso-8859-5	ISO 8859-5 Cyrillic
	28596	,    //	iso-8859-6	ISO 8859-6 Arabic
	28597	,    //	iso-8859-7	ISO 8859-7 Greek
	28598	,    //	iso-8859-8	ISO 8859-8 Hebrew; Hebrew (ISO-Visual)
	28599	,    //	iso-8859-9	ISO 8859-9 Turkish
	28603	,    //	iso-8859-13	ISO 8859-13 Estonian
	28605	,    //	iso-8859-15	ISO 8859-15 Latin 9
	29001	,    //	x-Europa	Europa 3
	38598	,    //	iso-8859-8-i	ISO 8859-8 Hebrew; Hebrew (ISO-Logical)
	50221	,    //	csISO2022JP	ISO 2022 Japanese with halfwidth Katakana; Japanese (JIS-Allow 1 byte Kana)
	50222	,    //	iso-2022-jp	ISO 2022 Japanese JIS X 0201-1989; Japanese (JIS-Allow 1 byte Kana - SO/SI)
	50225	,    //	iso-2022-kr	ISO 2022 Korean
	50227	,    //	x-cp50227	ISO 2022 Simplified Chinese; Chinese Simplified (ISO 2022)
	50229	,    //		ISO 2022 Traditional Chinese
	50930	,    //		EBCDIC Japanese (Katakana) Extended
	50931	,    //		EBCDIC US-Canada and Japanese
	50933	,    //		EBCDIC Korean Extended and Korean
	50935	,    //		EBCDIC Simplified Chinese Extended and Simplified Chinese
	50936	,    //		EBCDIC Simplified Chinese
	50937	,    //		EBCDIC US-Canada and Traditional Chinese
	50939	,    //		EBCDIC Japanese (Latin) Extended and Japanese
	
	51936	,    //	EUC-CN	EUC Simplified Chinese; Chinese Simplified (EUC)
	51949	,    //	euc-kr	EUC Korean
	51950	,    //		EUC Traditional Chinese
	52936	,    //	hz-gb-2312	HZ-GB2312 Simplified Chinese; Chinese Simplified (HZ)
	54936	,    //	GB18030	Windows XP and later: GB18030 Simplified Chinese (4 byte); Chinese Simplified (GB18030)
	57002	,    //	x-iscii-de	ISCII Devanagari
	57003	,    //	x-iscii-be	ISCII Bangla
	57004	,    //	x-iscii-ta	ISCII Tamil
	57005	,    //	x-iscii-te	ISCII Telugu
	57006	,    //	x-iscii-as	ISCII Assamese
	57007	,    //	x-iscii-or	ISCII Odia
	57008	,    //	x-iscii-ka	ISCII Kannada
	57009	,    //	x-iscii-ma	ISCII Malayalam
	57010	,    //	x-iscii-gu	ISCII Gujarati
	57011	,    //	x-iscii-pa	ISCII Punjabi
};

static BOOL WaitPostMessage(
	HWND hWnd,
	UINT Msg,
	WPARAM wParam,
	LPARAM lParam)
{
	int nRetry = 10;
	do {
		if (::PostMessage(hWnd, Msg, wParam, lParam))
		{
			return TRUE;
		}
		::Sleep(1000);
	} while (nRetry-- > 0);

	return FALSE;
}


// http://codereview.stackexchange.com/questions/30456/wildcard-search-in-c
enum {
	WILDCARD_PATTERN_MULTICHAR = L'*',
	WILDCARD_PATTERN_ONECHAR = L'?',
};
bool wildcardW(LPCWSTR subject, LPCWSTR pattern) {
	LPCWSTR s = NULL;
	for (; *pattern; pattern++) {
		switch (*pattern) {
		case WILDCARD_PATTERN_MULTICHAR:
			if (*(pattern + 1) == L'\0') {
				/* This wildcard appears at the end of the pattern.
				If we made it this far already, the match succeeds
				regardless of what the rest of the subject looks like. */
				return true;
			}
			for (s = subject; *s; s++) {
				if (wildcardW(s, pattern + 1)) {
					return true;
				}
			}
			return false;

		case WILDCARD_PATTERN_ONECHAR:
			if (*(subject++) == L'\0') {
				return false;
			}
			break;

		default:
			if (*subject == L'\0' || *pattern != *(subject++)) {
				return false;
			}
		}
	}

	/* End of pattern reached.  If this also the end of the subject, then
	match succeeds. */
	return *subject == L'\0';
}
bool hitFileName(const WIN32_FIND_DATA& wfd, const wstring& findee)
{
	if (findee.empty())
		return true;

	CString strFilename = wfd.cFileName;
	strFilename.MakeLower();

	CString strFindee = findee.c_str();
	strFindee.MakeLower();
	
	if(strFindee.Find(L'*') < 0)
		strFindee = L"*" + strFindee + L"*";

	// remove double *
	while (0 != strFindee.Replace(L"**", L"*"))
		;

	return wildcardW(strFilename, strFindee);
}
bool hitExactFileName(const WIN32_FIND_DATA& wfd, const wstring& findee)
{
	wstring name = wfd.cFileName;
	if (name.length() != findee.length())
		return false;
	return _wcsicmp(name.c_str(), findee.c_str()) == 0;
}

using RegexStorage = map<wstring, boost::wregex>;
static __declspec(thread) RegexStorage* gregexes;


bool hitRegFileName(const WIN32_FIND_DATA& wfd, const wstring& findee)
{
	if (findee.empty())
		return true;

	wstring name = stdStringLower(wstring(wfd.cFileName));
	
	if (!gregexes)
	{
		// TODO: leak. must call dtor on thread-exit
		gregexes = new RegexStorage;
	}
	if (gregexes->find(findee) == gregexes->end())
		(*gregexes)[findee] = boost::wregex(findee, boost::wregex::icase);
	
	// Regex must be cached
	ASSERT(gregexes->find(findee) != gregexes->end());
	
	return boost::regex_search(name, gregexes->at(findee));
}

bool hitFullName(const wstring& strPath, const WIN32_FIND_DATA& wfd, const wstring& findee)
{
	if (findee.empty())
		return true;

	CString strFullname = stdStringLower(stdCombinePath(strPath, wstring(wfd.cFileName))).c_str();
	CString strFindee = findee.c_str();
	strFindee.MakeLower();

	strFindee = L"*" + strFindee + L"*";

	// remove double *
	while (0 != strFindee.Replace(L"**", L"*"))
		;

	return wildcardW(strFullname, strFindee);
}

bool hitRegFullName(const wstring& strPath, const WIN32_FIND_DATA& wfd, const wstring& findee)
{
	if (findee.empty())
		return true;

	// TODO: should not be here
	// wstring regFindee = stdStringReplace(findee, L"/", L"\\\\");

	wstring fullname = stdStringLower(stdCombinePath(strPath, wstring(wfd.cFileName)));

	if (!gregexes)
	{
		// TODO: leak. must call dtor on thread-exit
		gregexes = new RegexStorage;
	}
	if (gregexes->find(findee) == gregexes->end())
		(*gregexes)[findee] = boost::wregex(findee, boost::wregex::icase);

	// Regex must be cached
	ASSERT(gregexes->find(findee) != gregexes->end());

	return boost::regex_search(fullname, gregexes->at(findee));
}



char simplow(char c)
{
	switch (c)
	{
	case 'A': return 'a';
	case 'B': return 'b';
	case 'C': return 'c';
	case 'D': return 'd';
	case 'E': return 'e';
	case 'F': return 'f';
	case 'G': return 'g';
	case 'H': return 'h';
	case 'I': return 'i';
	case 'J': return 'j';
	case 'K': return 'k';
	case 'L': return 'l';
	case 'M': return 'm';
	case 'N': return 'n';
	case 'O': return 'o';
	case 'P': return 'p';
	case 'Q': return 'q';
	case 'R': return 'r';
	case 'S': return 's';
	case 'T': return 't';
	case 'U': return 'u';
	case 'V': return 'v';
	case 'W': return 'w';
	case 'X': return 'x';
	case 'Y': return 'y';
	case 'Z': return 'z';
	}
	return c;
}
bool iequals(char c1, char c2)
{
	if (c1 == c2)
		return true;

	return simplow(c1) == simplow(c2);
}
// binary search in memory 
// http://stackoverflow.com/questions/8584644/strstr-for-a-string-that-is-not-null-terminated
LONGLONG memsearch(const char *hay, LONGLONG haysize, const char *needle, LONGLONG needlesize) {
	LONGLONG haypos, needlepos;
	haysize -= needlesize;
	for (haypos = 0; haypos <= haysize; haypos++) {
		for (needlepos = 0; needlepos < needlesize; needlepos++) {
			//if (hay[haypos + needlepos] != needle[needlepos]) {
			if ( !iequals(hay[haypos + needlepos],needle[needlepos])) {
					// Next character in haystack.
				break;
			}
		}
		if (needlepos == needlesize) {
			return haypos;
		}
	}
	return -1;
}

BSTR CreateBSTR(const char* p, size_t bytesize)
{
	return SysAllocStringByteLen(p, bytesize);
}

bool hitContent(const wstring& strPath, const WIN32_FIND_DATA& wfd, const wstring& findee, DWORD& dwError)
{
	if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		return false;

	if(wfd.nFileSizeLow==0 && wfd.nFileSizeHigh==0)
		return false;

	// wstring strFileName = stdwin32::stdCombinePath(strFolder.c_str(), wfd.cFileName);
	CMappedFile mapped;
	if (!mapped.Open(stdCombinePath(strPath.c_str(), wfd.cFileName).c_str())) //   strFileName.c_str()))
	{
		dwError = mapped.getError();
		return false;
	}

	const size_t utf16Bytelen = findee.length() * sizeof(WCHAR);

	LONGLONG lret;

	struct STSearched {
		vector<BSTR> v;
		bool IsSearched(const char* p, size_t size)  
		{
			vector<BSTR>::iterator it ;
			for(it=v.begin() ; it != v.end() ;++it)
			{
				if(SysStringByteLen(*it) == size)
				{
					if(0 == memcmp(*it, p, size))
						return true;
				}
			}
			return false;
		}			
		~STSearched()
		{
			vector<BSTR>::iterator it ;
			for(it=v.begin() ; it != v.end() ;++it)
			{
				SysFreeString(*it);
			}
		}
	} searched;
	
	const char* pUtf16 = (const char*)findee.c_str();
		
	{
		lret = memsearch(mapped.ptr(), mapped.size(), pUtf16, utf16Bytelen);
		searched.v.push_back(CreateBSTR(pUtf16, utf16Bytelen));
		if (lret != -1)
			return true;
	}
	

	{
		unique_ptr<wchar_t> pUtf16Converted(UTF16ConvertEndianEx(findee.c_str()));
		lret = memsearch(mapped.ptr(), mapped.size(), (char*)pUtf16Converted.get(), utf16Bytelen);
		searched.v.push_back(CreateBSTR((char*)pUtf16Converted.get(), utf16Bytelen));
		if (lret != -1)
			return true;
	}


	for(int i=0 ; i < _countof(sCodePages) ; ++i)
	{
		const UINT cp = sCodePages[i];
		int outlen=0;
		unique_ptr<const char> pOut(UTF16toMultiByteEx(cp, findee.c_str(), findee.size(), &outlen));
		
		if(!pOut)
			continue;

		unique_ptr<const wchar_t> pOut2(MultiBytetoUTF16Ex(cp, pOut.get(), outlen));
		if(!pOut2)
			continue;

		if(findee != pOut2.get())
			continue;

		ASSERT(outlen==strlen(pOut.get()));
		if(searched.IsSearched(pOut.get(), outlen))
			continue;

		lret = memsearch(mapped.ptr(), mapped.size(), pOut.get(), outlen);
		searched.v.push_back(CreateBSTR(pOut.get(), outlen));
		if (lret != -1)
			return true;
	}


	return false;
}

BOOL IsPeriod(LPCTSTR pFileName)
{
	if(pFileName[0] == _T('\0'))
		return FALSE;

	if(pFileName[0] != _T('.'))
		return FALSE;

	if(pFileName[1] == _T('\0'))
		return TRUE;	// one period

	if(pFileName[1] != _T('.'))
		return FALSE;

	if(pFileName[2] == _T('\0'))
		return TRUE;	// two period

	return FALSE;
}

void GetFiles(const wstring& strFolder, vector<WIN32_FIND_DATA>& retvec, CThreadArg* pArg)
{
	WIN32_FIND_DATA wfd = {0};

	vector<WIN32_FIND_DATA> files;
	vector<WIN32_FIND_DATA> dirs;

	wstring strPath = strFolder + _T('\\');
	HANDLE hFind = FindFirstFile ( (strPath + TEXT("*.*")).c_str(), &wfd);
	if (hFind == INVALID_HANDLE_VALUE)
	{
		DWORD dwLastError = GetLastError();
		CLVData* plvData = new CLVData(strPath.c_str(), wfd, dwLastError);
		if (!WaitPostMessage(pArg->hWnd(),
			WM_APP_SEARCHERROR,
			pArg->workId(),
			(LPARAM)plvData))
		{
			dwLastError = GetLastError();
			ASSERT(FALSE);
			return;
		}
		return;
	}
	
	BOOL bResult = TRUE;
	while (bResult)
	{
		if (!IsPeriod(wfd.cFileName))
		{
			if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				dirs.emplace_back(wfd);
			}
			else
			{
				files.emplace_back(wfd);
			}
		}
		bResult = FindNextFile(hFind, &wfd);
	}
	FindClose (hFind);

	retvec = files;
	retvec.insert(retvec.end(), dirs.begin(), dirs.end());
}

bool WcEvaluator(const WcfindWord& ww, 
	void* pVoid,
	const WIN32_FIND_DATA& wfd,
	const wstring& strPath,
	DWORD& dwLastError,
	bool& cancelled)
{
	CThreadArg* pArg = (CThreadArg*)pVoid;
	switch(ww.tt_)
	{
		// type
		case WcfindWord::TYPE: // pArg->m_bType)
		{
			if (ww.GetType() & FILE_ATTRIBUTE_DIRECTORY)
			{
				return (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
			}
			else if (ww.GetType() & FILE_ATTRIBUTE_NORMAL)
			{
				return (wfd.dwFileAttributes & (FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_ARCHIVE)) != 0;
			}
			else
			{
				ASSERT(false);
			}
		}
		break;

		// mtime
		case WcfindWord::MTIME:  // ->m_bMtime)
		{
			CTime timeStart(ww.currentTime_);
			CTime timeLA = CTime(wfd.ftLastWriteTime);
			CTimeSpan span = timeStart - timeLA;
			if (span.GetDays() < ww.GetMTime()) // pArg->m_nMtime)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		break;


		// size
		case WcfindWord::SIZE: // (pArg->m_bSize)
		{
			//                        87654321
			// Is large file?
			if (wfd.nFileSizeHigh & 0x80000000)
			{
				CLVData* plvData = new CLVData(strPath.c_str(), wfd, ERROR_BUFFER_OVERFLOW);
				if (!::PostMessage(pArg->hWnd(),
					WM_APP_SEARCHERROR,
					pArg->workId(),
					(LPARAM)plvData))
				{
					dwLastError = GetLastError();
					return false;
				}
			}
			__int64 size = (__int64)(((__int64)wfd.nFileSizeHigh << 32) | (__int64)wfd.nFileSizeLow);
			if ( ww.GetSizeSign() < 0 ) // pArg->m_nSizeSign < 0)
			{
				if (size <= ww.GetSize()) // size <= pArg->m_lSize)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else if (ww.GetSizeSign()==0) //pArg->m_nSizeSign == 0)
			{
				if (size==ww.GetSize()) //size == pArg->m_lSize)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else if (ww.GetSizeSign() > 0) //pArg->m_nSizeSign > 0)
			{
				if (size >= ww.GetSize()) //size >= pArg->m_lSize)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				ASSERT(FALSE);
				return false;
			}
		}
		break;

		// filename
		case WcfindWord::FILENAME:
		{
			return hitFileName(wfd, ww.GetFileName());
		}
		break;

		// regex filename
		case WcfindWord::RFILENAME:
		{
			return hitRegFileName(wfd, ww.GetFileName());
		}
		break;

		// exact filename
		case WcfindWord::EXACTFILENAME:
		{
			return hitExactFileName(wfd, ww.GetFileName());
		}
		break;

		// regex exact filename
		case WcfindWord::REXACTFILENAME:
		{
			return hitRegFileName(wfd, ww.GetFileName());
		}
		break;

		case WcfindWord::FULLNAME:
		{
			return hitFullName(strPath, wfd, ww.GetFileName());
		}
		break;
		case WcfindWord::RFULLNAME:
		{
			return hitRegFullName(strPath, wfd, ww.GetFileName());
		}
		break;

		// content
		case WcfindWord::FILECONTENT:
		{
			return hitContent(strPath, wfd, ww.GetFileContent(), dwLastError);
		}
		break;
	} // switch

	ASSERT(false);
	return false;
}


// TRUE : success and continue
// FALSE : interrupted and stop
bool searchFolder(const wstring& strFolder, CThreadArg* pArg)
{
	wstring strPath = strFolder + _T('\\');

	vector<WIN32_FIND_DATA> fileanddirs;
	GetFiles(strFolder, fileanddirs, pArg);
	
	bool bCancel = false;
	

	for(const WIN32_FIND_DATA& wfd : fileanddirs)
	{
		WaitForSingleObject(pArg->GetRunEvent(), INFINITE);
		if (bCancel || (gworkId != pArg->workId()))
			return false;

		ASSERT(pArg->opParser().HasEvaluator());
		DWORD dwLastError = 0;
		if (pArg->opParser().Evaluate(pArg, wfd, strPath, dwLastError, bCancel))
		{
			// Success,
			shared_ptr<CLVData> splvData(new CLVData(strPath.c_str(), wfd));
			pArg->InsertResult(splvData);
		}
		else
		{
			// false returned
			// There is chance this is
			// Cancelled, Errored, or just 'not found'

			if (bCancel)
				break;

			if (dwLastError != NO_ERROR)
			{
				// Error happens, report it
				CLVData* plvData = new CLVData(strPath.c_str(), wfd, dwLastError);
				if (!WaitPostMessage(pArg->hWnd(),
					WM_APP_SEARCHERROR,
					pArg->workId(),
					(LPARAM)plvData))
				{
					MessageBox(NULL, L"Failed to post message", L"wcfind", MB_ICONERROR);
					ExitProcess(-1);

					return false;
				}
			}
		}

		// Report and goto next folder
		if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			wstring nextfol = strFolder + _T('\\') + wfd.cFileName;
			static DWORD lastSentTick;
			DWORD nowTick = GetTickCount();
			if ((nowTick - lastSentTick) > 100)
			{
				lastSentTick = nowTick;

				if (!WaitPostMessage(pArg->hWnd(),
					WM_APP_SEARCHPROGRESS,
					pArg->workId(),
					// This leaks at closing phase, but ignores it
					(LPARAM)(new wstring(nextfol))))
				{
					MessageBox(NULL, L"Failed to post message", L"wcfind", MB_ICONERROR);
					ExitProcess(-1);

					return false;
				}
			}
			if (!searchFolder(nextfol, pArg))
				return false;
		}
	}  // iterating folder
	
	return !bCancel;
}
unsigned  __stdcall startOfHeadSearch(void * p)
{
	CThreadArg* pArg = (CThreadArg*)p;
	pArg->opParser().ResetEvaluator(&WcEvaluator);

	BOOL bCancelled = FALSE;
	vector<wstring>::iterator itFolder;
	for(itFolder = pArg->vFolders_.begin() ; 
		!bCancelled && (itFolder != pArg->vFolders_.end()) ;
		++itFolder)
	{
		wstring strFolder = stdExpandEnvironmentStrings(itFolder->c_str());
		if(!searchFolder(strFolder, pArg) )
		{
			break;
		}
	}

	::PostMessage(pArg->hWnd(),
		WM_APP_SEARCHFINISHED,
		pArg->workId(),
		0);
	delete pArg;
	return 0;
}


void CThreadArg::InsertResult(const std::shared_ptr<CLVData>& data)
{
	results_.InsertResult(data);
}
