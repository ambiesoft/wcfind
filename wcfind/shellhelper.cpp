#include "stdafx.h"
#include <wincred.h>

#pragma comment(lib,"Credui.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace std;

HRESULT mySHParseDisplayName(
	LPCWSTR          pszName,
	IBindCtx         *pbc,
	LPITEMIDLIST	*ppidl,
	SFGAOF           sfgaoIn,
	SFGAOF           *psfgaoOut)
{
	IShellFolderPtr pDesk;
	if(FAILED(SHGetDesktopFolder(&pDesk)))
		return E_FAIL;

	ULONG eaten=0;
	if(FAILED(pDesk->ParseDisplayName(
		NULL,
		NULL,
		(LPWSTR)pszName,
		&eaten,
		ppidl,
		psfgaoOut)))
	{
		return E_FAIL;
	}

	return S_OK;
}

HRESULT GetUIObjectOfFile(HWND hwnd, LPCWSTR pszPath, REFIID riid, void **ppv)
{
	*ppv = NULL;
	HRESULT hr;
	LPITEMIDLIST pidl;
	SFGAOF sfgao;
	if (SUCCEEDED(hr = mySHParseDisplayName(
		pszPath, 
		NULL,
		&pidl,
		0,
		&sfgao))) 
	{
		IShellFolderPtr pSF;
		LPCITEMIDLIST pidlChild;
		if (SUCCEEDED(hr = SHBindToParent(pidl,
			IID_IShellFolder,
			(void**)&pSF,
			&pidlChild))) 
		{
			hr = pSF->GetUIObjectOf(hwnd, 1, &pidlChild, riid, NULL, ppv);
		}
		CoTaskMemFree(pidl);
	}
	return hr;
}


HRESULT IContextMenu_GetCommandString(
									  IContextMenu *pcm, UINT_PTR idCmd, UINT uFlags,
									  UINT *pwReserved, LPWSTR pszName, UINT cchMax)
{
	// Callers are expected to be using Unicode.
	if (!(uFlags & GCS_UNICODE))
		return E_INVALIDARG;

	// Some context menu handlers have off-by-one bugs and will
	// overflow the output buffer. Let's artificially reduce the
	// buffer size so a one-character overflow won't corrupt memory.
	if (cchMax <= 1) 
		return E_FAIL;

	cchMax--;

	// First try the Unicode message.  Preset the output buffer
	// with a known value because some handlers return S_OK without
	// doing anything.
	pszName[0] = L'\0';

	HRESULT hr = pcm->GetCommandString(idCmd, uFlags, pwReserved,
		(LPSTR)pszName, cchMax);
	if (SUCCEEDED(hr) && pszName[0] == L'\0') {
		// Rats, a buggy IContextMenu handler that returned success
		// even though it failed.
		hr = E_NOTIMPL;
	}

	if (FAILED(hr))
	{
		// try again with ANSI - pad the buffer with one extra character
		// to compensate for context menu handlers that overflow by
		// one character.
		LPSTR pszAnsi = (LPSTR)LocalAlloc(LMEM_FIXED,
			(cchMax + 1) * sizeof(CHAR));
		if (pszAnsi) {
			pszAnsi[0] = '\0';
			hr = pcm->GetCommandString(idCmd, uFlags & ~GCS_UNICODE,
				pwReserved, pszAnsi, cchMax);
			if (SUCCEEDED(hr) && pszAnsi[0] == '\0')
			{
				// Rats, a buggy IContextMenu handler that returned success
				// even though it failed.
				hr = E_NOTIMPL;
			}
			if (SUCCEEDED(hr))
			{
				if (MultiByteToWideChar(CP_ACP, 0, pszAnsi, -1,
					pszName, cchMax) == 0)
				{
					hr = E_FAIL;
				}
			}
			LocalFree(pszAnsi);

		} else
		{
			hr = E_OUTOFMEMORY;
		}
	}
	return hr;
}


_COM_SMARTPTR_TYPEDEF(IShellLinkDataList, IID_IShellLinkDataList);
static BOOL GetShortcutFileInfo(LPCTSTR pszShortcutFile, 
							   tstring& targetFile,
							   tstring& curDir,
							   tstring& arg,
							   BOOL* pbIsAdmin=NULL)
{
	BOOL bFailed = TRUE;
	HRESULT hr;
	IShellLinkWPtr pShellLink = NULL;
	CoInitialize(NULL);
	TCHAR buffer[MAX_PATH];
    hr = CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER, IID_IShellLink, (LPVOID*)&pShellLink);
	if(SUCCEEDED(hr) && pShellLink != NULL)
	{
		IPersistFilePtr pPFile = NULL;
		hr = pShellLink->QueryInterface(IID_IPersistFile, (LPVOID*)&pPFile);
		if(SUCCEEDED(hr) && pPFile != NULL)
		{
			hr=pPFile->Load(pszShortcutFile, 0);
			if(SUCCEEDED(hr))
			{
				bFailed = FALSE;
				if(FAILED(pShellLink->GetPath(buffer, _countof(buffer), NULL,0)))
					return FALSE;
				targetFile = buffer;

				int size=16;
				do
				{
					//TCHAR* pBuff = (TCHAR*)malloc(size);
					//stlsoft::scoped_handle<void*> ma(pBuff, free);
					unique_ptr<TCHAR[]> buff((TCHAR*)malloc(size));
					if( FAILED(pShellLink->GetArguments(buff.get(), size/sizeof(TCHAR))) )
						return FALSE;

					size *= 2;
					//TCHAR* pBuff2 = (TCHAR*)malloc(size);
					//stlsoft::scoped_handle<void*> ma2(pBuff2, free);
					unique_ptr<TCHAR[]> buff2((TCHAR*)malloc(size));
					if( FAILED(pShellLink->GetArguments(buff2.get(), size/sizeof(TCHAR))) )
						return FALSE;

					if(lstrcmp(buff.get(),buff2.get()) == 0)
					{
						arg = buff2.get();
						break;
					}
				} while(true);
				
				
				if (FAILED(pShellLink->GetWorkingDirectory(buffer, _countof(buffer))))
					return FALSE;

				curDir=buffer;
			}
		}

		if(pbIsAdmin)
		{
			*pbIsAdmin = FALSE;
			IShellLinkDataListPtr pSLDL;
			hr = pShellLink->QueryInterface(IID_IShellLinkDataList, (void**)&pSLDL);
			if(pSLDL)
			{
				DWORD dwFlags = 0;
				hr = pSLDL->GetFlags(&dwFlags);
				if (SUCCEEDED(hr)) 
				{
					if(SLDF_RUNAS_USER & dwFlags)
					{
						*pbIsAdmin = TRUE;
					}
				}
			}
		}
	}

	CoUninitialize();
	return !bFailed;
}
BOOL OpenCommonShortcutSpecial(HWND hWnd, LPCTSTR pApp, LPCTSTR pCommand=NULL, LPCTSTR pDirectory=NULL)
{
	tstring targetFile;
	tstring curDir;
	tstring arg;
	BOOL bIsAdmin=FALSE;
	if(!GetShortcutFileInfo(pApp, 
		targetFile,
		curDir,
		arg,
		&bIsAdmin))
	{
		return FALSE;
	}

	tstring command(arg);
	if(pCommand && pCommand[0])
	{
		command += _T(" ");
		command += pCommand;
	}
	//LPTSTR pCT = _tcsdup(command.c_str());
	//stlsoft::scoped_handle<void*> ma(pCT, free);
	const unique_ptr<TCHAR, void(__cdecl*)(void*)> ct(static_cast<TCHAR*>(_tcsdup(command.c_str())), free);
	if(FALSE) // bIsAdmin)
	{
		HANDLE hToken = NULL;

		TOKEN_PRIVILEGES tp;
		PROCESS_INFORMATION pi;
		STARTUPINFOW si;

		// Initialize structures.
		ZeroMemory(&tp, sizeof(tp));
		ZeroMemory(&pi, sizeof(pi));
		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);


		LPTSTR lpszUsername = _T("Administrator\0");
		LPTSTR lpszDomain = _T(".");		//"bgt\0";
		LPTSTR lpszPassword = _T("\0");

		if (!OpenProcessToken(
			GetCurrentProcess(), 
			TOKEN_QUERY |
			TOKEN_ADJUST_PRIVILEGES,
			&hToken)) 
		{
			return FALSE;
		}

		// Look up the LUID for the TCB Name privilege.
		if (!LookupPrivilegeValue(
			NULL,
			SE_TCB_NAME, //SE_SHUTDOWN_NAME ,//SE_TCB_NAME,
			&tp.Privileges[0].Luid))
		{
			return FALSE;
		}


		tp.PrivilegeCount = 1;
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;//SE_PRIVILEGE_ENABLED;
		if (!AdjustTokenPrivileges(
			hToken, 
			FALSE,
			&tp,
			0,
			NULL,
			0)) 
		{
			return FALSE;
		}


		TCHAR  szUserName[256] = {0};
		TCHAR  szPassword[256] = {0};
		DWORD  dwResult;
		BOOL   bSave = FALSE;
		// HANDLE hToken;

		dwResult = CredUIPromptForCredentials(
			NULL,					// Dialog customize
			TEXT("target-host"),	// displayed on the top
			NULL,					// reserved
			0, // ERROR_ELEVATION_REQUIRED,	// reason for calling this function
			szUserName, sizeof(szUserName) / sizeof(TCHAR), // in-out username
			szPassword, sizeof(szPassword) / sizeof(TCHAR), // in-out password
			&bSave,					// in-out save checkbutton
			CREDUI_FLAGS_EXPECT_CONFIRMATION | 
			// CREDUI_FLAGS_GENERIC_CREDENTIALS |
			CREDUI_FLAGS_REQUEST_ADMINISTRATOR |
			CREDUI_FLAGS_USERNAME_TARGET_CREDENTIALS |
			CREDUI_FLAGS_EXCLUDE_CERTIFICATES |
//			CREDUI_FLAGS_COMPLETE_USERNAME |
			0
			);
		if (dwResult != NO_ERROR)
			return FALSE;

		if (LogonUser(szUserName, NULL, szPassword, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, &hToken))
		{
			if (bSave)
				CredUIConfirmCredentials(TEXT("target-host"), TRUE);
			CloseHandle(hToken);
		}
		else
		{
			MessageBox(NULL, TEXT("Logon failed"), NULL, MB_ICONWARNING);
			return FALSE;
		}

		SecureZeroMemory(szPassword, sizeof(szPassword));



		//if(LogonUser(lpszUsername,lpszDomain,lpszPassword,
		//	LOGON32_LOGON_INTERACTIVE,LOGON32_PROVIDER_DEFAULT,&hToken) == 0)
		//{
		//	MyError();
		//}
		//else

		STARTUPINFO sInfo;
		PROCESS_INFORMATION ProcessInfo;
		memset(&sInfo,0,sizeof(STARTUPINFO));
		sInfo.cb = sizeof(STARTUPINFO);
		sInfo.dwX = CW_USEDEFAULT;
		sInfo.dwY = CW_USEDEFAULT;
		sInfo.dwXSize = CW_USEDEFAULT;
		sInfo.dwYSize = CW_USEDEFAULT;


		if(!CreateProcessAsUser(
			hToken,
			targetFile.c_str(), // _T("c:\\windows\\system32\\notepad.exe"),
			ct.get(),
			NULL,
			NULL,
			TRUE,
			CREATE_NEW_CONSOLE,
			NULL,
			NULL,
			&sInfo,
			&ProcessInfo))
		{
			return FALSE;
		}
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{


		STARTUPINFO si = { sizeof(STARTUPINFO) };
		PROCESS_INFORMATION pi = {0};
		if (!CreateProcess(
			targetFile.c_str(),
			ct.get(),
			NULL,
			NULL,
			FALSE,
			0,
			NULL,
			pDirectory,
			&si,
			&pi)
			)
		{
			return FALSE;
		}
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}

	return TRUE;
}