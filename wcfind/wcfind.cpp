#include "stdafx.h"

#include "../../lsMisc/CreateFolderIniPath.h"
#include "../../lsMisc/SetCSIDLtoEnv.h"
#include "../../lsMisc/OpenCommon.h"
#include "../../lsMisc/stdosd/PathUtil.h"
#include "../../lsMisc/stdosd/stdosd.h"
#include "../../lsMisc/MFCHelper.h"
#include "../../lsMisc/HighDPI.h"
#include "../../lsMisc/GetProcessList.h"
#include "../../lsMisc/FindTopWindowFromPID.h"
#include "../../lsMisc/GetFullFileNameFromPID.h"
#include "../../lsMisc/GetVersionString.h"
#include "gitrev.h"

#include "resource.h"

// Run wcfind\wcfind\createbuildmachine.bat to create buildmachine.h
#include "buildmachine.h"

#include "../common/LangInfo.h"

#include "wcfind.h"
#include "MainFrm.h"

using namespace Ambiesoft::stdosd;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWcfindApp

BEGIN_MESSAGE_MAP(CWcfindApp, CWinApp)
	//{{AFX_MSG_MAP(CWcfindApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_FILE_CLOSEALLINSTANCES, &CWcfindApp::OnFileCloseallinstances)
	ON_COMMAND(ID_FILE_CLOSEOTHERINSTANCES, &CWcfindApp::OnFileCloseotherinstances)
END_MESSAGE_MAP()


CWcfindApp::CWcfindApp()
{
}


CWcfindApp theApp;





BOOL CWcfindApp::PreInit()
{
	{
		TCHAR szT[MAX_PATH];
		if (!GetFolderIniDirEx(
			m_hInstance,
			szT,
			_countof(szT),
			L"Ambiesoft",
			AfxGetAppName()))
		{
			// I18N(_T("%s is not found. Exiting.")));
			AfxMessageBox(L"Failed to get ini folder.");
			return FALSE;
		}

		wstring strT = stdCombinePath(szT, _T("wcfind.ini"));

		free((void*)m_pszProfileName);
		m_pszProfileName = _tcsdup(strT.c_str());
	}

	CString strLang;
	{
		int iLang = GetProfileInt(SECTION_OPTION, KEY_LANG, 0);
		strLang = GetLangInfo().GetLangAsISO3(iLang).c_str();
	}

	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	i18nInitLangmap(NULL, strLang, AfxGetAppName());

	CCLParse::PARSEERROR error;
	CString strErrorInfo;
	if (!m_clParse.parse(__argc, __targv, error, strErrorInfo, nullptr))
	{
		if (m_clParse.IsHelp())
		{
			AfxMessageBox(CCLParse::getUsage(false), MB_ICONINFORMATION);
			return FALSE;
		}
		if (m_clParse.IsVersion())
		{
			OnAppAbout();
			return FALSE;
		}

		AfxMessageBox(CString(CCLParse::getErrorString(error)) +
			(strErrorInfo.IsEmpty() ? _T("") : (_T(" : ") + strErrorInfo)));
		if (CCLParse::needsShowUsageForError(error))
			AfxMessageBox(CCLParse::getUsage(false), MB_ICONINFORMATION);
		return FALSE;
	}

	{
		wstring left, right, common;
		vector<wstring> vFolders;
		m_clParse.getFoldersAsVector(vFolders);
		if (!checkDupPaths(vFolders, left, right, common))
		{
			wstring message;
			message += I18N(L"These two folder contains same folder. Do you want to continue?");
			message += L"\r\n\r\n";
			message += left;
			message += L"\r\n";
			message += right;
			if (IDYES != AfxMessageBox(message.c_str(), MB_ICONQUESTION | MB_YESNO))
				return FALSE;
		}
	}

#if defined(_DEBUG) && 0
	CString debugMessage;
	debugMessage += L"GetCommandLine():\r\n";
	debugMessage += GetCommandLine();
	debugMessage += L"\r\n\r\n";

	debugMessage += m_clParse.getResult();
	AfxMessageBox(debugMessage, MB_SYSTEMMODAL);
#endif



	if (m_clParse.isLogging())
	{
		m_pLogging = new Logging(m_clParse.getLogFileName());
	}


	AfxEnableControlContainer();

#if _MSC_VER < 1500
#ifdef _AFXDLL
	Enable3dControls();
#else
	Enable3dControlsStatic();
#endif
#endif

	if (!AfxOleInit())
	{
		AfxMessageBox(TEXT ("AfxOleInit Error!"));
		return FALSE;
	}
	

	SetCSIDLtoEnv(L"WCFIND_");

	return TRUE;
}

BOOL CWcfindApp::InitInstance()
{
#ifdef _DEBUG
	{
		// test for leak report
		int* pi = new int;
		*pi = 100;
	}
#endif

	if(!PreInit())
		return FALSE;

	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;

	if (!pFrame->LoadFrame(IDR_MAINFRAME,
		WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL,
		NULL))
	{
		return FALSE;
	}

	if(PathFileExists(m_pszProfileName))
	{
		int left = theApp.GetProfileInt(SECTION_OPTION, KEY_LOCATION_X, 20);
		int top =  theApp.GetProfileInt(SECTION_OPTION, KEY_LOCATION_Y, 20);
		int width = theApp.GetProfileInt(SECTION_OPTION, KEY_LOCATION_WIDTH, 640);
		int height = theApp.GetProfileInt(SECTION_OPTION, KEY_LOCATION_HEIGHT, 480);

		pFrame->SetWindowPos(NULL, left, top, width, height, SWP_NOZORDER|SWP_NOMOVE);
	}

	i18nChangeMenuText(*pFrame->GetMenu());

	// pFrame->SetComboCurrent(getNeedleFileNamesString());
	pFrame->SetComboCurrent(L"TODO:command line here");

	m_timeStart = CTime::GetCurrentTime();
	pFrame->ShowWindow(SW_SHOW);
	pFrame->UpdateWindow();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CWcfindApp message handlers





/////////////////////////////////////////////////////////////////////////////
//

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CString	m_strVersion;
	//}}AFX_DATA

	// ClassWizard
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV のサポート
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonGotowebpage();
	CString m_strGitRev;
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
, m_strGitRev(_T(""))
{
	//{{AFX_DATA_INIT(CAboutDlg)
	m_strVersion = _T("");
	//}}AFX_DATA_INIT

	m_strGitRev = stdStringReplace(GITREV::GetHashMessage(),
		L"\n", L"\r\n").c_str();

}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Text(pDX, IDC_STATIC_VERSION, m_strVersion);
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_EDIT_REV, m_strGitRev);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_GOTOWEBPAGE, &CAboutDlg::OnBnClickedButtonGotowebpage)
END_MESSAGE_MAP()

void CWcfindApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CWcfindApp

BOOL CWcfindApp::InitApplication() 
{
	setlocale(LC_ALL, "");	
	InitHighDPISupport();
	return CWinApp::InitApplication();
}

BOOL CAboutDlg::OnInitDialog() 
{
	CString strMscVer;
	strMscVer.Format(_T("MSC: %d, MACHINE: %s"), _MSC_VER, BUILDMACHINE);
	m_strVersion.Format(_T("wcfind version %s (%s)"), 
		GetVersionString(stdGetModuleFileName().c_str(), 3).c_str(),
		(LPCTSTR)strMscVer);

	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CAboutDlg::OnBnClickedButtonGotowebpage()
{
	OpenCommon(
		m_hWnd,
		L"http://ambiesoft.mooo.com/main/");
}


int CWcfindApp::ExitInstance()
{
	if (m_pLogging)
	{
		delete m_pLogging;
		m_pLogging = nullptr;
	}

#ifndef _DEBUG
	// Workaround for unknown shutdown error
	ExitProcess(0);
#endif

	return CWinApp::ExitInstance();
}


BOOL CWcfindApp::OnIdle(LONG lCount)
{
	if (lCount > 0)
	{
		if (m_pChildView)
			m_pChildView->OnIdle(lCount);
	}
	return CWinApp::OnIdle(lCount);
}

void CWcfindApp::OnFileCloseallinstances()
{
	if (IDYES != AfxMessageBox(I18N(L"Are you sure to close all instances?"), MB_ICONQUESTION | MB_YESNO))
		return;

	DoCloseOtherInstances();
	OnAppExit();
}
void CWcfindApp::OnFileCloseotherinstances()
{
	if (IDYES != AfxMessageBox(I18N(L"Are you sure to close other instances?"), MB_ICONQUESTION | MB_YESNO))
		return;

	DoCloseOtherInstances();
}
void CWcfindApp::DoCloseOtherInstances()
{
	vector<PROCESSENTRY32W> allProcesses;
	GetProcessList(allProcesses);

	wstring thisexe = stdGetModuleFileName<wchar_t>();
	for (auto&& process : allProcesses)
	{
		wstring thatexe = GetFullFileNameFromPID(process.th32ProcessID);
		if (_wcsicmp(thisexe.c_str(), thatexe.c_str()) == 0)
		{
			if (GetCurrentProcessId() != process.th32ProcessID)
			{
				set<HWND> founds = FindTopWindowFromPID(process.th32ProcessID);
				for (auto&& hWnd : founds)
					PostMessage(hWnd, WM_APP_CLOSE, 0, 0);
			}
		}
	}

}
void CWcfindApp::CloseApp()
{
	OnAppExit();
}

