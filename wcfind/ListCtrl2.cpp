// ListCtrl2.cpp : implementation file
//

#include "stdafx.h"
#include "wcfind.h"
#include "ChildView.h"
#include "ListCtrl2.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CListCtrl2

CListCtrl2::CListCtrl2()
{
}

CListCtrl2::~CListCtrl2()
{
}


BEGIN_MESSAGE_MAP(CListCtrl2, CListCtrl)
	//{{AFX_MSG_MAP(CListCtrl2)
	ON_WM_MENUSELECT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListCtrl2 message handlers

void CListCtrl2::OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu) 
{
	CListCtrl::OnMenuSelect(nItemID, nFlags, hSysMenu);
}

LRESULT CListCtrl2::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if (!theApp.IsClosed())
	{
		if (m_pParentView->m_pcm != NULL && message == WM_MENUSELECT)
			return  m_pParentView->WindowProc(message, wParam, lParam);

		if (m_pParentView->m_pcm3)
		{
			LRESULT lres;
			if (SUCCEEDED(m_pParentView->m_pcm3->HandleMenuMsg2(message, wParam, lParam, &lres)))
			{
				return lres;
			}
		}
		else if (m_pParentView->m_pcm2)
		{
			if (SUCCEEDED(m_pParentView->m_pcm2->HandleMenuMsg(message, wParam, lParam)))
			{
				return 0;
			}
		}

		switch (message)
		{
		case WM_MOUSEWHEEL:
		{
			short distance = HIWORD(wParam);
			WORD vk = LOWORD(wParam);
			if (vk & MK_CONTROL)
			{
				m_pParentView->ChangeFontSize(distance > 0 ?
					CChildView::FONTSIZE::LARGER : CChildView::FONTSIZE::SMALLER);
			}
		}
		break;
		}
	}
	return CListCtrl::WindowProc(message, wParam, lParam);
}


BOOL CListCtrl2::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	//if(pMsg->message==WM_MENUSELECT)
	//{
	//	CString s;
	//}	
	return CListCtrl::PreTranslateMessage(pMsg);
}
