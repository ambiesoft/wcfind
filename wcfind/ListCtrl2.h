#if !defined(AFX_LISTCTRL2_H__96FE9450_C202_4EDC_8405_701CA9FE3E4C__INCLUDED_)
#define AFX_LISTCTRL2_H__96FE9450_C202_4EDC_8405_701CA9FE3E4C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ListCtrl2.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CListCtrl2 window
class CChildView;
class CListCtrl2 : public CListCtrl
{
// Construction
public:
	CListCtrl2();
	CChildView* m_pParentView;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CListCtrl2)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CListCtrl2();

	// Generated message map functions
protected:
	//{{AFX_MSG(CListCtrl2)
	afx_msg void OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LISTCTRL2_H__96FE9450_C202_4EDC_8405_701CA9FE3E4C__INCLUDED_)
