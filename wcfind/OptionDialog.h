#include "afxwin.h"
#if !defined(AFX_OPTIONDIALOG_H__380B5AC4_70BE_4D0A_BEB7_68C56A12ED81__INCLUDED_)
#define AFX_OPTIONDIALOG_H__380B5AC4_70BE_4D0A_BEB7_68C56A12ED81__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OptionDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COptionDialog dialog

class COptionDialog : public CDialog
{
// Construction
public:
	BOOL Save();
	COptionDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COptionDialog)
	enum { IDD = IDD_DIALOG_OPTION };
	CStatic	m_lblTimeFormatResult;
	CString	m_strTimeFormat;
	BOOL	m_bPasteOnStartup;
	//}}AFX_DATA

	int m_nLang = -1;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COptionDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL



// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COptionDialog)
	afx_msg void OnChangeEditTimeformat();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	BOOL m_bAutoSort;

	BOOL IsAutoSort() const {
		return m_bAutoSort;
	}
	BOOL m_bFullRowSelect;
	BOOL IsFullRowSelect() const {
		return m_bFullRowSelect;
	}
	afx_msg void OnBnClickedButtonDefaulttimeformat();
	CComboBox m_cmbLang;
	afx_msg void OnBnClickedOk();
	CStatic m_lblRequiresRestart;
	afx_msg void OnCbnSelchangeComboLang();
	BOOL m_bShowErrorDialogWhenErrorOccurs;
	BOOL IsShowErrorDialogWhenErrorOccurs() const {
		return m_bShowErrorDialogWhenErrorOccurs;
	}
	BOOL m_bForewindowAfterFinish;
	BOOL IsForewindowAfterFinish() const{
		return m_bForewindowAfterFinish;
	}
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPTIONDIALOG_H__380B5AC4_70BE_4D0A_BEB7_68C56A12ED81__INCLUDED_)
