
#if !defined(AFX_STDAFX_H__436637A6_F25E_44A2_B668_5FF9B026DF16__INCLUDED_)
#define AFX_STDAFX_H__436637A6_F25E_44A2_B668_5FF9B026DF16__INCLUDED_

#pragma warning (disable : 4786)

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "../../lsMisc/PragmaShowVersions.h"
#define VC_EXTRALEAN

//#ifdef _DEBUG
// VLD can not be installed on Windows10 32bit
// Temporary diabling it
//#include <vld.h>
//#endif

#include <afxwin.h> 
#include <afxext.h> 
#include <afxdisp.h>
#include <afxdtctl.h>
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>
#endif // _AFX_NO_AFXCMN_SUPPORT
#include <afxole.h>
#include <afxstatusbar.h>
#include <afxframewndex.h>

// C Runtime
#include <malloc.h>
#include <process.h>


// #include <checkaccess.h>


// Win32
#include <winuser.h>
#include <mmsystem.h>
#include <Shlwapi.h>
#include <ShlObj.h>
#include <Shobjidl.h>
#include <comdef.h>
#include <comip.h>
_COM_SMARTPTR_TYPEDEF(IContextMenu, __uuidof(IContextMenu));
_COM_SMARTPTR_TYPEDEF(IContextMenu2, __uuidof(IContextMenu2));
_COM_SMARTPTR_TYPEDEF(IContextMenu3, __uuidof(IContextMenu3));



#include <cctype>
#include <cstdarg>
#include <functional>
#include <locale>
#include <memory>
#include <vector>
#include <set>
#include <algorithm>
#include <map>
// #include <regex>
#include <string>

#include <boost/any.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>



#include "../../lsMisc/tstring.h"
#include "../../lsMisc/GetDirFromPath.h"
#include "../../lsMisc/GetLastErrorString.h"
#include "../../lsMisc/OpenCommon.h"
#include "../../lsMisc/SetClipboardText.h"
#include "../../lsMisc/CResizableDialog/ResizableDialog.h"
#include "../../lsMisc/I18N.h"
#include "../../lsMisc/stdosd/OpParser.h"
#include "../../lsMisc/stdosd/stdosd.h"
#include "../../lsMisc/GetClipboardText.h"
#include "../../lsMisc/SetFileOntoClipboard.h"
#include "../../lsMisc/CommandLineString.h"
#include "../../lsMisc/stdosd/blockedbool.h"
// #include "../common/WcfindWord.h"

using namespace Ambiesoft;
using namespace Ambiesoft::stdosd;
using namespace std;

#include "../common/CommonDefine.h"

#define MF_ ((CMainFrame*)theApp.m_pMainWnd)

#define RETURNFALSE do {ASSERT(FALSE);return FALSE;} while(0)

extern UINT g_uCustomClipbrdFormat;
extern bool g_bNT;

// putting name makes Event Global, it must be NULL
// #define STR_RUN_EVENT L"RunThreadWCfindEvent"
extern volatile int gworkId;
extern std::vector<HANDLE> gAllThreads;
#define LOGIT(s) do { theApp.WriteLog(s); } while(false)


// #include "../../lsMisc/PragmaShowVersions.h"
#include "../../lsMisc/DebugNew.h"



#endif // !defined(AFX_STDAFX_H__436637A6_F25E_44A2_B668_5FF9B026DF16__INCLUDED_)
