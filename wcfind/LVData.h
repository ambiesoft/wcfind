// LVData.h: CLVData
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LVDATA_H__78CBB211_A268_4C3B_A615_CCBCAA6E5285__INCLUDED_)
#define AFX_LVDATA_H__78CBB211_A268_4C3B_A615_CCBCAA6E5285__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAKE_LONGLONG(hi, lo)  (  LONGLONG(LONGLONG(hi) << 32 ) | LONGLONG(DWORD(lo) & 0xffffffff)  )

class CLVData  
{
private:
	CString strParent_;
	WIN32_FIND_DATA wfd_;
	CString strFullPath_;
	mutable int iIcon_;
	mutable CString strTypeName_;
	mutable CString strSizeText_;
	DWORD dwError_;
	mutable CString strTimeText_;
	mutable BOOL bSelected_ = FALSE;
	static std::map<std::wstring, std::wstring> cacheFiletype_;

public:
	CLVData(CString strParent, const WIN32_FIND_DATA& wfd, DWORD dwError=0);
	virtual ~CLVData();

	int GetiIcon() const;

	LPCTSTR GetFileName() const {
		return wfd_.cFileName;
	}

	LONGLONG GetSize() const {
		return MAKE_LONGLONG(wfd_.nFileSizeHigh, wfd_.nFileSizeLow);
	}
	LPCTSTR GetSizeText() const {
		if(wfd_.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			return L"";

		if(strSizeText_.IsEmpty())
		{
			StrFormatByteSize64 (MAKE_LONGLONG(wfd_.nFileSizeHigh, wfd_.nFileSizeLow),
				strSizeText_.GetBuffer(32), 32);
			strSizeText_.ReleaseBuffer();
		}
		return strSizeText_;
	}
	LPCTSTR GetFolder() const {
		return strParent_;
	}
	LPCTSTR GetTypeText() const;
	
	FILETIME GetTime() const {
		return wfd_.ftLastWriteTime;
	}
	
	LPCTSTR GetTimeText(LPCTSTR pFormat = NULL) const;

	CString GetFullPath() const {
		return strFullPath_;
	}
	DWORD GetLastError() const {
		return dwError_;
	}

	void ResetTimeFormatText() const {
		strTimeText_.Empty();
	}

	void SetSelected(BOOL bSelected) const {
		bSelected_ = bSelected;
	}
	BOOL GetSelected() const {
		return bSelected_;
	}

	static void CacheFileType(const std::wstring& ext, const std::wstring filetype) {
		cacheFiletype_.insert_or_assign(ext, filetype);
	}
	static std::wstring GetCacheFileType(const std::wstring& ext) {
		return cacheFiletype_[ext];
	}

	BOOL IsDirectory() const {
		return (wfd_.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
	}
};

#endif // !defined(AFX_LVDATA_H__78CBB211_A268_4C3B_A615_CCBCAA6E5285__INCLUDED_)
