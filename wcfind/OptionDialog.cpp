// OptionDialog.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "helper.h"
#include "../common/LangInfo.h"
#include "wcfind.h"
#include "OptionDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COptionDialog dialog


COptionDialog::COptionDialog(CWnd* pParent /*=NULL*/)
	: CDialog(COptionDialog::IDD, pParent)
	, m_bAutoSort(FALSE)
	, m_bFullRowSelect(FALSE)
	, m_bShowErrorDialogWhenErrorOccurs(FALSE)
	, m_bForewindowAfterFinish(FALSE)
{
	//{{AFX_DATA_INIT(COptionDialog)
	m_strTimeFormat = _T("");
	m_bPasteOnStartup = FALSE;
	//}}AFX_DATA_INIT

	m_strTimeFormat = theApp.GetProfileString(SECTION_OPTION, KEY_TIMEFORMAT, _T("%x %X"));
	m_bPasteOnStartup = theApp.GetProfileInt(SECTION_OPTION, KEY_PASTEONSTARTUP, TRUE);
	m_bAutoSort = theApp.GetProfileInt(SECTION_OPTION, KEY_AUTOSORT, TRUE);
	m_bFullRowSelect = theApp.GetProfileInt(SECTION_OPTION, KEY_FULLROWSELCT, FALSE);
	m_nLang = theApp.GetProfileInt(SECTION_OPTION, KEY_LANG, 0);
	m_bShowErrorDialogWhenErrorOccurs = theApp.GetProfileInt(SECTION_OPTION, KEY_SHOWERRORDIALOGWHENERROROCCURS, 0);
	m_bForewindowAfterFinish = theApp.GetProfileInt(SECTION_OPTION, KEY_FOREWINDOWAFTERFINISH, 0);
}


void COptionDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptionDialog)
	DDX_Control(pDX, IDC_STATIC_TIMEFORMATRESULT, m_lblTimeFormatResult);
	DDX_Text(pDX, IDC_EDIT_TIMEFORMAT, m_strTimeFormat);
	DDX_Check(pDX, IDC_CHECK_PASTEONSTARTUP, m_bPasteOnStartup);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CHECK_AUTOSORT, m_bAutoSort);
	DDX_Check(pDX, IDC_CHECK_FULLROWSELECT, m_bFullRowSelect);
	DDX_Control(pDX, IDC_COMBO_LANG, m_cmbLang);
	DDX_Control(pDX, IDC_STATIC_REQUIRES_RESTART, m_lblRequiresRestart);
	DDX_Check(pDX, IDC_CHECK_SHOWERRORATERROR, m_bShowErrorDialogWhenErrorOccurs);
	DDX_Check(pDX, IDC_CHECK_FOREWINDOWAFTERFINISH, m_bForewindowAfterFinish);
}


BEGIN_MESSAGE_MAP(COptionDialog, CDialog)
	//{{AFX_MSG_MAP(COptionDialog)
	ON_EN_CHANGE(IDC_EDIT_TIMEFORMAT, OnChangeEditTimeformat)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_DEFAULTTIMEFORMAT, &COptionDialog::OnBnClickedButtonDefaulttimeformat)
	ON_BN_CLICKED(IDOK, &COptionDialog::OnBnClickedOk)
	ON_CBN_SELCHANGE(IDC_COMBO_LANG, &COptionDialog::OnCbnSelchangeComboLang)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptionDialog message handlers




BOOL isFormatOK2(LPCTSTR pF)
{
	CTime t = CTime::GetCurrentTime();
	t.Format(pF);
	return TRUE;
}

BOOL isFormatOK(LPCTSTR pF)
{
	__try
	{
		isFormatOK2(pF);
		return TRUE;
	}
	__finally
	{
	}
	return FALSE;
}

BOOL COptionDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	i18nChangeChildWindowText(*this);

	int nSel = -1;
	for (int i = 0; i < GetLangInfo().GetCount(); ++i)
	{
		wstring lang = GetLangInfo().GetLangAsString(i);
		m_cmbLang.AddString(lang.c_str());
		if (m_nLang == i)
			nSel = i;
	}
	if (nSel >= 0)
	{
		m_cmbLang.SetCurSel(nSel);
	}

	OnChangeEditTimeformat();

	return TRUE;
}

void COptionDialog::OnChangeEditTimeformat()
{
	UpdateData();

	CString timeText = safe_formatTime(m_strTimeFormat);
	m_lblTimeFormatResult.SetWindowText(timeText);
}

BOOL COptionDialog::Save()
{
	BOOL bFail = FALSE;
	bFail |= !theApp.WriteProfileString(SECTION_OPTION, KEY_TIMEFORMAT, m_strTimeFormat);
	bFail |= !theApp.WriteProfileInt(SECTION_OPTION, KEY_PASTEONSTARTUP, m_bPasteOnStartup);
	bFail |= !theApp.WriteProfileInt(SECTION_OPTION, KEY_AUTOSORT, m_bAutoSort);
	bFail |= !theApp.WriteProfileInt(SECTION_OPTION, KEY_FULLROWSELCT, m_bFullRowSelect);
	bFail |= !theApp.WriteProfileInt(SECTION_OPTION, KEY_LANG, m_nLang);
	bFail |= !theApp.WriteProfileInt(SECTION_OPTION, KEY_SHOWERRORDIALOGWHENERROROCCURS, m_bShowErrorDialogWhenErrorOccurs);
	bFail |= !theApp.WriteProfileInt(SECTION_OPTION, KEY_FOREWINDOWAFTERFINISH, m_bForewindowAfterFinish);
	return !bFail;
}

void COptionDialog::OnBnClickedButtonDefaulttimeformat()
{
	if (IDYES != MessageBox(I18N(L"Do you want to use default time format?"),
		NULL,
		MB_ICONQUESTION | MB_YESNO))
	{
		return;
	}

	m_strTimeFormat = L"%x %X";
	UpdateData(FALSE);
	OnChangeEditTimeformat();
}

void COptionDialog::OnBnClickedOk()
{
	m_nLang = m_cmbLang.GetCurSel();
	CDialog::OnOK();
}

void COptionDialog::OnCbnSelchangeComboLang()
{
	m_lblRequiresRestart.ShowWindow(SW_SHOW);
}
