#include "StdAfx.h"
#include "helper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static void cdecl iph(const wchar_t *, const wchar_t *, const wchar_t *, unsigned int, uintptr_t)
{

}

template<class T>
CString safe_formatTime_impl(LPCTSTR pFormat, const T* pSystemOrFileTime)
{
	CTime t = pSystemOrFileTime ? CTime(*pSystemOrFileTime) : CTime::GetCurrentTime();
	TCHAR szBuff[256] = { 0 };
	_invalid_parameter_handler oldHandler = _set_invalid_parameter_handler(iph);

	CString ret = t.Format(pFormat);

	_set_invalid_parameter_handler(oldHandler);
	return ret;
}

CString safe_formatTime(LPCTSTR pFormat, const SYSTEMTIME* pSystemTime)
{
	return safe_formatTime_impl<SYSTEMTIME>(pFormat, pSystemTime);
}
CString safe_formatTime(LPCTSTR pFormat, const FILETIME* pFileTime)
{
	return safe_formatTime_impl<FILETIME>(pFormat, pFileTime);
}