# Microsoft Developer Studio Project File - Name="wcfind" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=wcfind - Win32 UnicodeDebug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "wcfind.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "wcfind.mak" CFG="wcfind - Win32 UnicodeDebug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "wcfind - Win32 UnicodeDebug" (based on "Win32 (x86) Application")
!MESSAGE "wcfind - Win32 UnicodeRelease" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "wcfind - Win32 UnicodeDebug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "UnicodeDebug"
# PROP BASE Intermediate_Dir "UnicodeDebug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "UnicodeDebug"
# PROP Intermediate_Dir "UnicodeDebug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "L:\boost1.35" /I "." /I "L:\MSSDK\2003-FEB\include" /I "L:\MSSDK\2003-FEB\include\mfc" /I "L:\check" /I "../MyUtility/stlsoft/include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "UNICODE" /D "_UNICODE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x411 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x411 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Shlwapi.lib /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /debug /machine:I386 /out:"C:\Linkout\wcfind\wcfindD.exe" /pdbtype:sept /libpath:"L:\MSSDK\2003-FEB\Lib"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy exe.manifest C:\Linkout\wcfind\wcfind.exe.manifest
# End Special Build Tool

!ELSEIF  "$(CFG)" == "wcfind - Win32 UnicodeRelease"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "UnicodeRelease"
# PROP BASE Intermediate_Dir "UnicodeRelease"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "UnicodeRelease"
# PROP Intermediate_Dir "UnicodeRelease"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /I "L:\boost1.35" /I "." /I "L:\MSSDK\2003-FEB\include" /I "L:\MSSDK\2003-FEB\include\mfc" /I "L:\check" /I "../MyUtility/stlsoft/include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "UNICODE" /D "_UNICODE" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x411 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x411 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 Shlwapi.lib /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /machine:I386 /out:"C:\Linkout\wcfind\wcfind.exe" /libpath:"L:\MSSDK\2003-FEB\Lib"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy exe.manifest C:\Linkout\wcfind\wcfind.exe.manifest
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "wcfind - Win32 UnicodeDebug"
# Name "wcfind - Win32 UnicodeRelease"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ChildView.cpp
# End Source File
# Begin Source File

SOURCE=.\ChildViewThread.cpp
# End Source File
# Begin Source File

SOURCE=.\common\CLParse.cpp
# ADD CPP /Yu
# End Source File
# Begin Source File

SOURCE=..\MyUtility\CreateCompleteDirectory.cpp
# End Source File
# Begin Source File

SOURCE=..\MyUtility\CreateFolderIniPath.cpp
# ADD CPP /Yu
# End Source File
# Begin Source File

SOURCE=.\ErrorDialog.cpp
# End Source File
# Begin Source File

SOURCE=..\MyUtility\GetDirFromPath.cpp
# ADD CPP /Yu
# End Source File
# Begin Source File

SOURCE=..\MyUtility\GetFilesInfo.cpp
# ADD CPP /Yu
# End Source File
# Begin Source File

SOURCE=..\MyUtility\GetLastErrorString.cpp
# ADD CPP /Yu
# End Source File
# Begin Source File

SOURCE=..\MyUtility\GetSpecialFolderPath.cpp
# End Source File
# Begin Source File

SOURCE=..\MyUtility\IsWindowsNT.cpp
# End Source File
# Begin Source File

SOURCE=.\ListCtrl2.cpp
# End Source File
# Begin Source File

SOURCE=.\LVData.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=..\MyUtility\MFCHelper.cpp
# End Source File
# Begin Source File

SOURCE=..\MyUtility\OpenCommon.cpp
# End Source File
# Begin Source File

SOURCE=.\OptionDialog.cpp
# End Source File
# Begin Source File

SOURCE=..\MyUtility\SetClipboardText.cpp
# End Source File
# Begin Source File

SOURCE=..\MyUtility\SetCSIDLtoEnv.cpp
# End Source File
# Begin Source File

SOURCE=.\ShellFolderDS.cpp
# End Source File
# Begin Source File

SOURCE=.\shellhelper.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\MyUtility\stdwin32\stdwin32.cpp
# ADD CPP /Yu
# End Source File
# Begin Source File

SOURCE=.\thread.cpp
# End Source File
# Begin Source File

SOURCE=..\MyUtility\tstring.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\UrlEncode.cpp
# End Source File
# Begin Source File

SOURCE=..\MyUtility\UTF16toUTF8.cpp
# End Source File
# Begin Source File

SOURCE=.\wcfind.cpp

!IF  "$(CFG)" == "wcfind - Win32 UnicodeDebug"

# ADD CPP /FA

!ELSEIF  "$(CFG)" == "wcfind - Win32 UnicodeRelease"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\wcfind.rc
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\ChildView.h
# End Source File
# Begin Source File

SOURCE=.\common\CLParse.h
# End Source File
# Begin Source File

SOURCE=.\CommonDefine.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\CreateCompleteDirectory.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\CreateFolderIniPath.h
# End Source File
# Begin Source File

SOURCE=.\ErrorDialog.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\GetDirFromPath.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\GetFilesInfo.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\GetLastErrorString.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\GetSpecialFolderPath.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\IsWindowsNT.h
# End Source File
# Begin Source File

SOURCE=.\ListCtrl2.h
# End Source File
# Begin Source File

SOURCE=.\LVData.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\MFCHelper.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\OpenCommon.h
# End Source File
# Begin Source File

SOURCE=.\OptionDialog.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\SetClipboardText.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\SetCSIDLtoEnv.h
# End Source File
# Begin Source File

SOURCE=.\ShellFolderDS.h
# End Source File
# Begin Source File

SOURCE=.\shellhelper.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\stdwin32\stdwin32.h
# End Source File
# Begin Source File

SOURCE=.\thread.h
# End Source File
# Begin Source File

SOURCE=..\MyUtility\UTF16toUTF8.h
# End Source File
# Begin Source File

SOURCE=.\wcfind.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\wcfind.ico
# End Source File
# Begin Source File

SOURCE=.\res\wcfind.rc2
# End Source File
# End Group
# Begin Source File

SOURCE=.\Todo.txt
# End Source File
# End Target
# End Project
