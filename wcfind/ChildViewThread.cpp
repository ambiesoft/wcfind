
#include "stdafx.h"
#include <afxpriv.h>



#include "resource.h"
#include "shellhelper.h"

#include "wcfind.h"
#include "ChildView.h"
#include "MainFrm.h"

#include "LVData.h"
#include "thread.h"
#include "OptionDialog.h"
#include "ErrorDialog.h"
// #include "ShellContextMenu.h"

#include "../../lsMisc/MFCHelper.h"

#pragma comment(lib,"winmm.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

struct SortingHolder
{
	bool* pB;
	SortingHolder(bool* pb)
	{
		ASSERT(*pb);
		pB = pb;
	}
	~SortingHolder()
	{
		*pB = false;
	}
};

void CChildView::SortIfNeeded(bool force)
{
	LOGIT(L"Sort");

	static bool sSorting;
	if (sSorting)
	{
		AfxMessageBox(L"Sort ReCalled");
		return;
	}

	sSorting = true;
	SortingHolder holder(&sSorting);

	if(force)
	{
		// m_ctrlFileList.SortItems(compareItems, (DWORD_PTR)&m_lvci);
		SortLVData(m_lvci);
		return;
	}

	static int scount = 0;
	if(++scount > 20)
	{
		scount=0;
		if(m_lvci.IsInitted())
		{
			// m_ctrlFileList.SortItems(compareItems, (DWORD_PTR)&m_lvci);
			SortLVData(m_lvci);
		}
	}
}
void CChildView::SortLVData(LVCOMPAREINFO& cmpinfo)
{
	if (GetSearchResult()->Sort(cmpinfo))
	{
		//m_ctrlFileList.SetItemCountEx(0);
		//m_ctrlFileList.SetItemCountEx(GetSearchResult()->GetCount());
		m_ctrlFileList.Invalidate();
		m_ctrlFileList.UpdateWindow();

		{
			BlockedBool bbColumn(&columnClicking_, true, false);
			// clear selections
			
			// this takes long time
			//for (int i = 0; i < m_ctrlFileListGetItemCount(); ++i)
			//{
			//	m_ctrlFileList.SetItemState(i, 0, LVIS_FOCUSED | LVIS_SELECTED);
			//}
			POSITION pos = m_ctrlFileList.GetFirstSelectedItemPosition();
			while (pos)
			{
				int iItemIndex = m_ctrlFileList.GetNextSelectedItem(pos);
				m_ctrlFileList.SetItemState(iItemIndex, 0, LVIS_FOCUSED | LVIS_SELECTED);
			}
		}

		// set selections
		vector<int> selectedItems;
		GetSearchResult()->GetSelections(&selectedItems);
		for (int i : selectedItems)
			m_ctrlFileList.SetItemState(i, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
	}
}

//LRESULT CChildView::OnSearchHit(WPARAM wParam, LPARAM lParam)
//{
//	LOGIT(L"OnSearchHit");
//	
//	if (theApp.IsClosed())
//		return 0;
//
//	CLVData* pData = (CLVData*)lParam;
//	if ((int)wParam != gworkId)
//	{
//		delete pData;
//		return FALSE;
//	}
//
//	m_ctrlFileList.InsertItem(
//		LVIF_TEXT|LVIF_IMAGE|LVIF_PARAM,
//		m_ctrlFileList.GetItemCount(),
//		LPSTR_TEXTCALLBACK,
//		0,0,
//		I_IMAGECALLBACK,
//		lParam
//		);
//	
//	if(m_pOption->IsAutoSort())
//		Sort();
//
//	return TRUE;
//}
LRESULT CChildView::OnSearchError(WPARAM wParam, LPARAM lParam)
{
	LOGIT(L"OnSearchError");

	if (theApp.IsClosed())
		return 0;

	unique_ptr<CLVData> pData((CLVData*)lParam);
	if ((int)wParam != gworkId)
		return FALSE;

	ASSERT(m_pErrorDialog);

	if(m_pOption->IsShowErrorDialogWhenErrorOccurs())
		m_pErrorDialog->ShowWindow(SW_SHOW);
	
	m_pErrorDialog->Add(*pData);
	return TRUE;
}

LRESULT CChildView::OnSearchProgress(WPARAM wParam, LPARAM lParam)
{
	LOGIT(L"OnSearchProgress");

	if (theApp.IsClosed())
		return 0;

	unique_ptr<wstring> str((wstring*)lParam);

	if( (int)wParam != gworkId )
		return FALSE;

	((CMainFrame*)theApp.m_pMainWnd)->SetMessageText(str.get()->c_str());
	return TRUE;
}




LRESULT CChildView::OnSearchFinished(WPARAM wParam, LPARAM lParam)
{
	LOGIT(L"OnSearchFinished");
	
	if (theApp.IsClosed())
		return 0;

	if ((int)wParam != gworkId)
		return FALSE;

	if (m_lvci.IsInitted())
	{
		SortLVData(m_lvci);
	}

	MF_->SetMessageText(L"");
	MF_->PostMessage(WM_SETMESSAGESTRING, AFX_IDS_IDLEMESSAGE);

	static wstring soundFile = stdCombinePath(stdGetParentDirectory(stdGetModuleFileName<wchar_t>()), L"se_koc05.wav");
	VERIFY(PathFileExists(soundFile.c_str()));
	VERIFY(PlaySound(soundFile.c_str(), NULL, SND_FILENAME | SND_ASYNC));

	m_hCurThread = nullptr;

	if (m_pOption->IsForewindowAfterFinish())
		SetForegroundWindow();
	return 0;
}

void csa2v(const CStringArray& sa, vector<wstring>& vs)
{
	for(int i=0 ; i < sa.GetSize() ; ++i)
		vs.push_back( (LPCTSTR)sa[i] );
}

bool CChildView::DoSearchAgainByFilename(const FileNameArray& saNeedleFileNames)
{
	AfxMessageBox(L"Not Implemented, just do repeating");
	return DoSearch(
		theApp.getFolders());
	
	return false;
}

void CChildView::ResetThreadResult(int workid)
{
	GetSearchResult()->reset(workid);
}

bool CChildView::DoSearch(
						  const CStringArray& saPaths)
{
	++gworkId;
	// m_ctrlFileList.DeleteAllItems();
	m_ctrlFileList.SetItemCountEx(0, LVSICF_NOSCROLL);

	m_hCurThread = NULL;
	VERIFY(SetEvent(m_hRun));

	ResetThreadResult(gworkId);
	ASSERT(GetSearchResult());
	CThreadArg* pArg = new CThreadArg(theApp.getOpParser(), *GetSearchResult(), m_hRun);
	pArg->SetWorkID(gworkId);
	pArg->SetHwnd(*this);
	
	csa2v(saPaths,pArg->vFolders_);

	uintptr_t threadhandle = _beginthreadex(NULL, 0, startOfHeadSearch, (void*)pArg, CREATE_SUSPENDED, NULL);
	if ( threadhandle == 0 )
	{
		AfxMessageBox(I18N(L"beginthreadex failed"));
		return false;
	}
	gAllThreads.push_back((HANDLE)threadhandle);
	// title
	{
		CString strTitle;

		strTitle += theApp.GetTitleFromParser();
		strTitle += L" - ";
		strTitle += L"wcFind";
		
		MF_->SetTitle(strTitle);
		MF_->SetWindowText(strTitle);
	}
	
	ResumeThread((HANDLE)threadhandle);
	m_hCurThread = (HANDLE)threadhandle;

	// m_nSearchState = SEARCHSTATE_SEARCHING;

	return true;
}
void CChildView::OnReplay() 
{
	VERIFY(SetEvent(m_hRun));
}
void CChildView::OnPause() 
{
	VERIFY(ResetEvent(m_hRun));
}

void CChildView::OnUpdatePause(CCmdUI* pCmdUI) 
{
	if(!m_hCurThread)
	{
		pCmdUI->Enable(FALSE);
		return;
	}

	CThreadState thst;
	GetThreadState(&thst);

	pCmdUI->Enable(thst.bActive_ && !thst.bSuspended_);
}
void CChildView::OnUpdateReplay(CCmdUI* pCmdUI) 
{
	if(!m_hCurThread)
	{
		pCmdUI->Enable(FALSE);
		return;
	}

	CThreadState thst;
	GetThreadState(&thst);

	pCmdUI->Enable(thst.bActive_ && thst.bSuspended_);
}

void CChildView::GetThreadState(CThreadState* pState) const
{
	pState->bActive_ = m_hCurThread && WaitForSingleObject(m_hCurThread, 0) == WAIT_TIMEOUT;
	pState->bSuspended_ = WaitForSingleObject(m_hRun, 0) == WAIT_TIMEOUT;
}
CChildView::SEARCHSTATE CChildView::GetSearchState() const
{
	CThreadState thst;
	GetThreadState(&thst);

	if(thst.bActive_)
	{ 
		if (thst.bSuspended_)
			return SEARCHSTATE_SUSPENDED;
		else
			return SEARCHSTATE_SEARCHING;
	}
	else
	{
		// Thread not alive
		return SEARCHSTATE_FINISHED;
	}
}
