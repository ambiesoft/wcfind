// MainFrm.h : CMainFrame
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__F2B0239A_9633_4FCF_94CD_3A8E0130B513__INCLUDED_)
#define AFX_MAINFRM_H__F2B0239A_9633_4FCF_94CD_3A8E0130B513__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "ChildView.h"

class CMainFrame : public CFrameWndEx
{
	using ParentClass = CFrameWndEx;

public:
	CMainFrame();
protected: 
	DECLARE_DYNAMIC(CMainFrame)


	// ClassWizard
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL


public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual void OnUpdateFrameTitle(BOOL bAddToTitle);
public:
	CMFCStatusBar  m_wndStatusBar;
	CMFCToolBar    m_wndToolBar;
	CChildView    m_wndView;
	CComboBoxEx m_wndComboBox;

	BOOL HasFindAgainFileName();
	CString GetFindFileAgainText();
	void SetComboCurrent(LPCTSTR pString);
	BOOL HasComboFocused() const;

protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	void OnUpdateShellContextMenu(CCmdUI* pCmdUI) ;
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnUpdateFindfileGo(CCmdUI *pCmdUI);

	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI *pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI *pCmdUI);
	afx_msg void OnEditCut();
	afx_msg void OnUpdateEditCut(CCmdUI *pCmdUI);
	afx_msg void OnUpdateEditCopyAstest(CCmdUI* pCmdUI);
	afx_msg void OnEditCopyAstest();
	afx_msg BOOL OnQueryEndSession();
	afx_msg LRESULT OnMyAppClose(WPARAM wParam, LPARAM lParam);
	afx_msg void OnSearchErrorClick();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++

#endif // !defined(AFX_MAINFRM_H__F2B0239A_9633_4FCF_94CD_3A8E0130B513__INCLUDED_)
