#include "stdafx.h"
#include "ThreadResults.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace std;


inline bool AmniCompare(LPCTSTR t1, LPCTSTR t2)
{
	return lstrcmpi(t1, t2) < 0;
	// return wstring(t1) < wstring(t2);
}
inline bool AmniCompare(int t1, int t2)
{
	return t1 < t2;
}
inline bool AmniCompare(LONGLONG t1, LONGLONG t2)
{
	//LONGLONG l = t1 - t2;
	//if (l == 0)
	//	return 0;
	//if (l < 0)
	//	return -1;
	//return 1;
	return t1 < t2;
}
inline bool AmniCompare(const FILETIME& t1, const FILETIME& t2)
{
	return CompareFileTime(&t1, &t2) < 0;
}
//int CALLBACK CChildView::compareItems(LPARAM l1, LPARAM l2, LPARAM pData)
//{
//	CLVData* p1 = (CLVData*)l1;
//	CLVData* p2 = (CLVData*)l2;
//
//	LVCOMPAREINFO* pLVCI = (LVCOMPAREINFO*)pData;
//	int retval=0;
//	switch(pLVCI->GetCol())
//	{
//	case 0: retval = AmniCompare(p1->GetFileName(), p2->GetFileName()); break;
//	case 1: retval = AmniCompare(p1->GetSize(), p2->GetSize()); break;
//	case 2: retval = AmniCompare(p1->GetFolder(), p2->GetFolder()); break;
//	case 3: retval = AmniCompare(p1->GetTypeText(),p2->GetTypeText()); break;
//	case 4: retval = AmniCompare(p1->GetTime(), p2->GetTime()); break;
//	case 5: break;
//	default:ASSERT(FALSE);
//	}
//
//	return pLVCI->IsReverse() ? -retval : retval;
//}

class SortOp
{
	LVCOMPAREINFO& cmpinfo_;
public:
	SortOp(LVCOMPAREINFO& cmpinfo) : cmpinfo_(cmpinfo){}

	bool operator()(const shared_ptr<CLVData>& left, const shared_ptr<CLVData>& right)
	{
		return compareItems(left.get(), right.get());
	}

private:
	bool compareItems(const CLVData* p1, const CLVData* p2)
	{
		bool retval;
		if (cmpinfo_.IsReverse())
			retval = compareItemsImple(p2, p1);
		else
			retval = compareItemsImple(p1, p2);
		return retval;
	}
	bool compareItemsImple(const CLVData* p1, const CLVData* p2)
	{
		switch (cmpinfo_.GetCol())
		{
		case 0: return AmniCompare(p1->GetFileName(), p2->GetFileName());
		case 1: return AmniCompare(p1->GetSize(), p2->GetSize()) ;
		case 2: return AmniCompare(p1->GetFolder(), p2->GetFolder());
		case 3: 
		{
			if (p1->IsDirectory() && p2->IsDirectory())
				return AmniCompare(p1->GetFileName(), p2->GetFileName());
			else if (p1->IsDirectory() && !p2->IsDirectory())
				return true;
			else if (!p1->IsDirectory() && p2->IsDirectory())
				return false;
			return AmniCompare(p1->GetTypeText(), p2->GetTypeText());
		}
		break;
		case 4: return AmniCompare(p1->GetTime(), p2->GetTime());
		case 5: break;
		default:ASSERT(FALSE);
		}
	
		return true;
	}
};
bool CThreadResults::Sort(LVCOMPAREINFO& cmpinfo)
{
	MyLocker locker(lock_, MyLocker::LockType::READWRITE_LOCK);
	if (workid_ != gworkId)
		return false;

	if (!bSortDirty_ && lastCmpInfo_==cmpinfo)
		return false;
	
	lastCmpInfo_ = cmpinfo;
	
	sort(results_.begin(), results_.end(), SortOp(cmpinfo));
	
	bSortDirty_ = false;
	return true;
}