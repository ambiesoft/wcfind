#include "stdafx.h"
#include "resource.h"
#include "wcfind.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, ParentClass)

BEGIN_MESSAGE_MAP(CMainFrame, ParentClass)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_UPDATE_COMMAND_UI_RANGE(ID_SHELLMENU_START, ID_SHELLMENU_END, OnUpdateShellContextMenu)
	ON_UPDATE_COMMAND_UI(ID_FINDFILE_GO, &CMainFrame::OnUpdateFindfileGo)
	ON_COMMAND(ID_EDIT_COPY, &CMainFrame::OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, &CMainFrame::OnUpdateEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, &CMainFrame::OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, &CMainFrame::OnUpdateEditPaste)
	ON_COMMAND(ID_EDIT_CUT, &CMainFrame::OnEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, &CMainFrame::OnUpdateEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPYASTEST, OnUpdateEditCopyAstest)
	ON_COMMAND(ID_EDIT_COPYASTEST, OnEditCopyAstest)
	ON_WM_QUERYENDSESSION()
	ON_MESSAGE(WM_APP_CLOSE, OnMyAppClose)
	ON_COMMAND(ID_INDICATOR_SEARCHERROR, OnSearchErrorClick)
END_MESSAGE_MAP()

void CMainFrame::OnUpdateShellContextMenu(CCmdUI* pCmdUI) 
{
	AfxMessageBox(L"fsdjoafj");
}

static UINT indicators[] =
{
	ID_SEPARATOR,
	// ID_INDICATOR_CAPS,
	ID_INDICATOR_LISTITEMSELECTEDCOUNT,
	ID_INDICATOR_LISTITEMCOUNT,
	ID_INDICATOR_SEARCHERROR,
	ID_INDICATOR_SEARCHSTATE,
	// ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

CMainFrame::CMainFrame()
{
}

CMainFrame::~CMainFrame()
{
}


void CMainFrame::OnUpdateFrameTitle(BOOL bAddToTitle)
{
	ParentClass::OnUpdateFrameTitle(bAddToTitle);
}

WNDPROC gpOldComboEditProc;
LRESULT CALLBACK ComboEditBoxProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
//#ifdef _DEBUG
//	TCHAR szBuff[16];
//	wsprintf(szBuff, L"%d\n", msg);
//	TRACE(szBuff);
//#endif

	//if (msg == WM_CHAR && wparam == 13)
	//{
	//	((CMainFrame*)theApp.m_pMainWnd)->m_wndView.OnComboEnter();
	//}
	if ((msg==WM_KEYUP) && wparam==13)
	{
		((CMainFrame*)theApp.m_pMainWnd)->m_wndView.OnComboEnter();
	}

	//return pOldWndProc(hwnd, msg, wparam, lparam);
	return CallWindowProc(gpOldComboEditProc, hwnd,	msg, wparam, lparam);
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (ParentClass::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwCtrlStyle = TBSTYLE_FLAT;// | TBSTYLE_TRANSPARENT;
	DWORD dwStyle = WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC;
	if (!m_wndToolBar.CreateEx(this,
		dwCtrlStyle,
		dwStyle) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
			sizeof(indicators) / sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;
	}
	m_wndStatusBar.EnablePaneDoubleClick();

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	CRect rect;
	int nIndex = m_wndToolBar.CommandToIndex(ID_FINDFILE_COMBO);
	m_wndToolBar.SetButtonInfo(nIndex, ID_FINDFILE_COMBO, TBBS_SEPARATOR, 205);
	m_wndToolBar.GetItemRect(nIndex, &rect);
	rect.top = 1;
	rect.bottom = rect.top + 250 /*drop height*/;
	if (!m_wndComboBox.Create(CBS_DROPDOWN | WS_VISIBLE |
		WS_TABSTOP | WS_VSCROLL, rect, &m_wndToolBar, ID_FINDFILE_COMBO))
	{
		TRACE(_T("Failed to create combo-box\n"));
		return FALSE;
	}
	// m_wndComboBox.GetEditCtrl()->ModifyStyle(0, ES_WANTRETURN);
	gpOldComboEditProc = (WNDPROC)SetWindowLong(*m_wndComboBox.GetEditCtrl(), GWL_WNDPROC, (LONG)ComboEditBoxProc);

	int insertpos = 0;
	COMBOBOXEXITEM cxItem = { 0 };
	cxItem.mask = CBEIF_TEXT;
	cxItem.iItem = insertpos++;
	cxItem.pszText = L"aaa";
	m_wndComboBox.InsertItem(&cxItem);
	m_wndComboBox.SetItemHeight(-1, 30);

	//m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	//EnableDocking(CBRS_ALIGN_ANY);
	// DockControlBar(&m_wndToolBar);

	if (!m_wndView.Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
		CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
	{
		TRACE0("Failed to create view window\n");
		return -1;
	}

	DockPane(&m_wndToolBar);
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !ParentClass::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	// cs.lpszClass = AfxRegisterWndClass(0);
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	ParentClass::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	ParentClass::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame
void CMainFrame::OnSetFocus(CWnd* pOldWnd)
{
	m_wndView.SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	if (m_wndView.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	return ParentClass::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}


void CMainFrame::OnDestroy() 
{
	theApp.SetClosed();

	ASSERT(gpOldComboEditProc);
	SetWindowLong(*m_wndComboBox.GetEditCtrl(), GWL_WNDPROC, (LONG)gpOldComboEditProc);

	BOOL failed = FALSE;
	CRect r;


	if(!IsIconic() && !IsZoomed())
	{
		GetWindowRect(r);

		failed |= !theApp.WriteProfileInt(SECTION_OPTION, KEY_LOCATION_X, r.left);
		failed |= !theApp.WriteProfileInt(SECTION_OPTION, KEY_LOCATION_Y, r.top);
		failed |= !theApp.WriteProfileInt(SECTION_OPTION, KEY_LOCATION_WIDTH, r.Width());
		failed |= !theApp.WriteProfileInt(SECTION_OPTION, KEY_LOCATION_HEIGHT, r.Height());
	}

	ParentClass::OnDestroy();
}

BOOL CMainFrame::HasFindAgainFileName()
{
	return m_wndComboBox.GetEditCtrl()->GetWindowTextLength() != 0;
}

CString CMainFrame::GetFindFileAgainText()
{
	CString str;
	m_wndComboBox.GetEditCtrl()->GetWindowText(str);
	return str;
}



void CMainFrame::OnUpdateFindfileGo(CCmdUI *pCmdUI)
{
	// pCmdUI->Enable(HasFindAgainFileName());
	pCmdUI->Enable(TRUE);
}


void CMainFrame::SetComboCurrent(LPCTSTR pString)
{
	m_wndComboBox.GetEditCtrl()->SetWindowText(pString);
	if (pString && pString[0])
	{
		COMBOBOXEXITEM cxItem = { 0 };
		cxItem.mask = CBEIF_TEXT;
		cxItem.iItem = 0;
		cxItem.pszText = (LPWSTR) pString;
		m_wndComboBox.InsertItem(&cxItem);
	}
}


BOOL CMainFrame::HasComboFocused() const
{
	return m_wndComboBox.IsChild(GetFocus());
}
void CMainFrame::OnEditCopy()
{
	if (HasComboFocused())
	{
		m_wndComboBox.GetEditCtrl()->Copy();
		return;
	}

	m_wndView.OnEditCopy();
}
void CMainFrame::OnUpdateEditCopy(CCmdUI *pCmdUI)
{
	if (HasComboFocused())
	{
		pCmdUI->Enable();
	}
	else
	{
		m_wndView.OnUpdateEditCopy(pCmdUI);
	}
}


void CMainFrame::OnEditPaste()
{
	if (HasComboFocused())
	{
		m_wndComboBox.GetEditCtrl()->Paste();
		return;
	}
}


void CMainFrame::OnUpdateEditPaste(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(HasComboFocused());
}


void CMainFrame::OnEditCut()
{
	if (HasComboFocused())
	{
		m_wndComboBox.GetEditCtrl()->Cut();
		return;
	}

	m_wndView.OnEditCut();
}
void CMainFrame::OnUpdateEditCut(CCmdUI *pCmdUI)
{
	if (HasComboFocused())
	{
		pCmdUI->Enable();
	}
	return m_wndView.OnUpdateEditCut(pCmdUI);
}


void CMainFrame::OnUpdateEditCopyAstest(CCmdUI* pCmdUI)
{
	if (HasComboFocused())
	{
		pCmdUI->Enable();
	}
	else
	{
		m_wndView.OnUpdateEditCopyAstest(pCmdUI);
	}
}
void CMainFrame::OnEditCopyAstest()
{
	if (HasComboFocused())
	{
		m_wndComboBox.GetEditCtrl()->Copy();
		return;
	}

	m_wndView.OnEditCopyAstest();
}
void CMainFrame::OnSearchErrorClick()
{
	m_wndView.ShowErrorDialog();
}


BOOL CMainFrame::OnQueryEndSession()
{
	SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOGPFAULTERRORBOX);
	ExitProcess(0);

	return TRUE;
}
LRESULT CMainFrame::OnMyAppClose(WPARAM wParam, LPARAM lParam)
{
	theApp.CloseApp();
	return 0;
}

