
#include "stdafx.h"


#include "resource.h"
#include "shellhelper.h"

#include "wcfind.h"
#include "ChildView.h"
#include "MainFrm.h"

#include "LVData.h"
#include "thread.h"

// #include "ShellContextMenu.h"
#include "ShellFolderDS.h"

#include "OptionDialog.h"
#include "ErrorDialog.h"

#include "../../lsMisc/GetFilesInfo.h"
#include "../../lsMisc/IsWindowsNT.h"

#include "../../lsMisc/MFChelper.h"
#include "../../lsMisc/stdosd/PathUtil.h"

#include "../common/ColumnHeader.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




/////////////////////////////////////////////////////////////////////////////
// CChildView

#if defined(_MSC_VER) && (_MSC_VER <= 1400)
typedef struct {
    HWND hwnd;
    IContextMenuCB *pcmcb;          // optional: callback object
    LPCITEMIDLIST pidlFolder;   // optional: IDList to folder of the items, computed from psf if NULL
    IShellFolder *psf;              // folder of the items
    UINT cidl;                      // # of items in apidl
    LPCITEMIDLIST * apidl;    // items operating on, used to get IDataObject and IAssociationArray
    IUnknown *punkAssociationInfo;  // optional: IQueryAssociations, specifies where to load extensions from, computed from apidl if NULL
    UINT cKeys;                     // # of items in aKeys, may be zero
    const HKEY *aKeys;              // optional: specifies where to load extensions from
} DEFCONTEXTMENU;
#endif


typedef HRESULT (__stdcall* pfnSHCreateDefaultContextMenu)(const DEFCONTEXTMENU *pdcm, REFIID riid, void **ppv);
pfnSHCreateDefaultContextMenu fnSHCreateDefaultContextMenu;
CChildView::CChildView()
{
#ifdef _DEBUG
	// leak detection test
	new int;
	malloc(10);
#endif
	HMODULE hShell = LoadLibrary(L"Shell32.dll");
	if(hShell)
	{
		fnSHCreateDefaultContextMenu = (pfnSHCreateDefaultContextMenu)GetProcAddress(hShell, "SHCreateDefaultContextMenu");
		FreeLibrary(hShell);
	}

	m_ctrlFileList.m_pParentView = this;
	m_hRun = CreateEvent(NULL, TRUE, TRUE, NULL);
	ASSERT(m_hRun);
	ASSERT(WaitForSingleObject(m_hRun, INFINITE) == WAIT_OBJECT_0);
	ASSERT(WaitForSingleObject(m_hRun, INFINITE) == WAIT_OBJECT_0);
	m_hCurThread = NULL;
	// m_nSearchState = SEARCHSTATE_NONE;

	m_pOption = NULL;
	m_pErrorDialog=NULL;
}

CChildView::~CChildView()
{
	VERIFY(CloseHandle(m_hRun));
	m_hRun = NULL;

	ASSERT(m_pOption==NULL);
}


BEGIN_MESSAGE_MAP(CChildView,CWnd )
	//{{AFX_MSG_MAP(CChildView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_UPDATE_COMMAND_UI(ID_APP_ABOUT, OnUpdateAppAbout)
	ON_COMMAND(ID_PAUSE, OnPause)
	ON_UPDATE_COMMAND_UI(ID_PAUSE, OnUpdatePause)
	ON_COMMAND(ID_RESTART, OnRestart)
	ON_UPDATE_COMMAND_UI(ID_RESTART, OnUpdateRestart)
	ON_WM_CONTEXTMENU()
	ON_WM_MENUSELECT()
	ON_COMMAND(ID_REPLAY, OnReplay)
	ON_UPDATE_COMMAND_UI(ID_REPLAY, OnUpdateReplay)
	ON_COMMAND(ID_RESET, OnReset)
	ON_UPDATE_COMMAND_UI(ID_RESET, OnUpdateReset)
	ON_WM_DESTROY()
	ON_COMMAND(ID_EDIT_OPTION, OnEditOption)
	ON_UPDATE_COMMAND_UI(ID_EDIT_LABEL, OnUpdateEditLabel)
	ON_COMMAND(ID_EDIT_LABEL, OnEditLabel)
	ON_UPDATE_COMMAND_UI(ID_SELECT_ALL, OnUpdateSelectAll)
	ON_COMMAND(ID_SELECT_ALL, OnSelectAll)
	//}}AFX_MSG_MAP
	ON_NOTIFY (LVN_GETDISPINFO, IDC_FILELIST, OnGetDispInfoFileList)
	ON_NOTIFY (NM_DBLCLK, IDC_FILELIST, OnDblclkFileList)
	// ON_NOTIFY(NM_RCLICK, IDC_FILELIST, OnRClickFileList)
	// ON_NOTIFY(LVN_DELETEITEM, IDC_FILELIST, OnDeleteItem)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_FILELIST, OnColumnClick)
	ON_NOTIFY(LVN_BEGINLABELEDITW, IDC_FILELIST, OnBeginEdit)
	ON_NOTIFY(LVN_ENDLABELEDITW, IDC_FILELIST, OnEndEdit)
	// ON_MESSAGE(WM_APP_SEARCHHIT, OnSearchHit)
	ON_MESSAGE(WM_APP_SEARCHERROR, OnSearchError)
	ON_MESSAGE(WM_APP_SEARCHPROGRESS, OnSearchProgress)
	ON_MESSAGE(WM_APP_SEARCHFINISHED, OnSearchFinished)
	ON_MESSAGE(WM_APP_AFTERENDEDIT, OnAfterEndEdit)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_LISTITEMSELECTEDCOUNT, OnUpdateSelectedNum)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_LISTITEMCOUNT, OnUpdateNum)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_SEARCHSTATE, OnUpdateSearchState)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_SEARCHERROR, OnUpdateSearchError)
	ON_UPDATE_COMMAND_UI_RANGE(ID_SHELLMENU_START, ID_SHELLMENU_END, OnUpdateShellContextMenu)
	ON_UPDATE_COMMAND_UI(ID_SHELLCUSTOM_OPENPARENT, OnUpdateSOP)
	ON_COMMAND(ID_VIEW_ERRORDIALOG, &CChildView::OnViewErrordialog)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ERRORDIALOG, &CChildView::OnUpdateViewErrordialog)
	ON_NOTIFY(LVN_BEGINDRAG, IDC_FILELIST, OnBegindragFilelist)
	ON_NOTIFY(LVN_BEGINRDRAG, IDC_FILELIST, OnBegindragFilelist)
	ON_COMMAND(ID_SEARCH_SEARCHAGAINBYSETTINGCLIPBOARDTEXTSFILENAME, &CChildView::OnSearchSearchagainbysettingclipboardtextsfilename)
	ON_UPDATE_COMMAND_UI(ID_SEARCH_SEARCHAGAINBYSETTINGCLIPBOARDTEXTSFILENAME, &CChildView::OnUpdateSearchSearchagainbysettingclipboardtextsfilename)
	ON_COMMAND(ID_FINDFILE_GO, &CChildView::OnFindfileGo)
	ON_NOTIFY(LVN_KEYDOWN, IDC_FILELIST, OnLVNKeydown)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_FILELIST, OnListViewItemChanged)
	ON_NOTIFY(LVN_ODSTATECHANGED, IDC_FILELIST, OnListViewItemChangedRange)
	ON_COMMAND(ID_EDIT_FONT, &CChildView::OnEditFont)
END_MESSAGE_MAP()


void CChildView::OnListViewItemChangedCommon()
{
	if (columnClicking_)
		return;

	ASSERT_VALID(&m_ctrlFileList);

	GetSearchResult()->ClearSelections();
	POSITION pos = m_ctrlFileList.GetFirstSelectedItemPosition();
	if (!pos)
		return;
	

	int nItem = 0;
#ifdef _DEBUG
	vector<int> selectedItems;
#endif
	while (pos)
	{
		nItem = m_ctrlFileList.GetNextSelectedItem(pos);
		ASSERT(nItem >= 0);
		GetSearchResult()->get(nItem).SetSelected(TRUE);
#ifdef _DEBUG
		selectedItems.push_back(nItem);
#endif
	}
#ifdef _DEBUG
	CString strTrace = L"Selected Items:";
	for (int i : selectedItems)
	{
		strTrace += to_string(i).c_str();
		strTrace += ",";
	}
	strTrace += "\r\n";
	TRACE(strTrace);
#endif

}
void CChildView::OnListViewItemChanged(NMHDR* pNMHDR, LRESULT* pResult) {
	OnListViewItemChangedCommon();
}

void CChildView::OnListViewItemChangedRange(NMHDR* pNMHDR, LRESULT* pResult) {
	OnListViewItemChangedCommon();
}

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.style &= ~WS_BORDER;

	return TRUE;
}

HIMAGELIST CChildView::GetSystemImageList(BOOL bLarge)
{
	SHFILEINFO sfi = {0};
	return (HIMAGELIST)SHGetFileInfo(
		_T("c:\\"),
		0,
		&sfi, 
		sizeof (sfi), 
		SHGFI_SYSICONINDEX | (bLarge ? SHGFI_LARGEICON : SHGFI_SMALLICON) );
}

int CChildView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd ::OnCreate(lpCreateStruct) == -1)
		return -1;
	

	ASSERT(m_pOption == NULL);
	m_pOption = new COptionDialog;

	ASSERT(m_pErrorDialog == NULL);
	m_pErrorDialog = new CErrorDialog();
	m_pErrorDialog->Create(m_pErrorDialog->IDD, NULL);

	if(!m_ctrlFileList.Create(
		// WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_OWNERDATA | LVS_SHAREIMAGELISTS | LVS_SHOWSELALWAYS,
		WS_CHILD | WS_VISIBLE | LVS_OWNERDATA | LVS_REPORT | LVS_SHAREIMAGELISTS | LVS_SHOWSELALWAYS | LVS_EDITLABELS,
		CRect (0, 0, 0, 0), 
		this,
		IDC_FILELIST))
		return -1;

	m_ctrlFileList.ModifyStyleEx (NULL, WS_EX_CLIENTEDGE);
	m_ctrlFileList.SetExtendedStyle(
		m_pOption->IsFullRowSelect() ? LVS_EX_FULLROWSELECT : 0		|
		LVS_EX_INFOTIP												|
		LVS_EX_DOUBLEBUFFER											|
		LVS_EX_HEADERDRAGDROP);

	m_ctrlFileList.SetImageList (CImageList::FromHandle (GetSystemImageList (FALSE)), LVSIL_SMALL);
	m_ctrlFileList.SetImageList (CImageList::FromHandle (GetSystemImageList (TRUE)), LVSIL_NORMAL);

	int i=0;

	const int nColCount = GetHeaderCount();
	for(int nColumnIndex=0; nColumnIndex < nColCount ; ++nColumnIndex)
	{
		m_ctrlFileList.InsertColumn (
			nColumnIndex, 
			I18N(gheaderstart[nColumnIndex].m_pName),
			gheaderstart[nColumnIndex].m_nFormat,
			gheaderstart[nColumnIndex].m_nWidth);
	}
	
	std::vector<int> nOrder(GetHeaderCount());
	for (i = 0; i < nColCount; ++i)
	{
		nOrder[i] = i;
	}
	
	LPBYTE pData=NULL;
	UINT nBytes=0;
	if (theApp.GetProfileBinary(SECTION_OPTION, KEY_COLUMNORDER, (LPBYTE*)&pData, &nBytes))
	{
		if (nBytes == sizeof(int) * nOrder.size())
		{
			memcpy(nOrder.data(), pData, nBytes);
			CHeaderCtrl* pHeader = m_ctrlFileList.GetHeaderCtrl();
			m_ctrlFileList.SetColumnOrderArray(nColCount, nOrder.data());
		}
		delete pData;
		pData = NULL;
	}

	// Load font and set
	if (theApp.GetProfileBinary(SECTION_OPTION, KEY_FONTLIST, (LPBYTE*)&pData, &nBytes))
	{
		if (nBytes != sizeof(LOGFONT))
		{
			AfxMessageBox(I18N(L"The sizeof LOGFONT in saved setting is not same as the size of LOGFONT"));
		}
		else
		{
			m_pFontList = new CFont();
			if (!m_pFontList->CreateFontIndirect((const LOGFONT*)pData))
			{
				AfxMessageBox(I18N(L"Failed to create font from LOGFONT"));
			}
			else
			{
				m_ctrlFileList.SetFont(m_pFontList);
			}
			delete pData;
			pData = NULL;
		}
	}

	std::vector<int> nWidths(GetHeaderCount());
	for (i = 0; i < GetHeaderCount(); ++i)
	{
		nWidths[i] = gheaderstart[i].m_nWidth;
	}
	if (theApp.GetProfileBinary(SECTION_OPTION, KEY_COLUMNWIDTH, (LPBYTE*)&pData, &nBytes))
	{
		if (nBytes == (sizeof(int) * nWidths.size()))
		{
			memcpy(nWidths.data(), pData, nBytes);
			CHeaderCtrl* pHeader = m_ctrlFileList.GetHeaderCtrl();
			for (i = 0; i < (int)nWidths.size(); ++i)
			{
				HD_ITEM hi = { 0 };
				hi.mask = HDI_WIDTH;
				hi.cxy = nWidths[i];
				VERIFY(pHeader->SetItem(i, &hi));
			}
		}
		delete pData;
		pData = NULL;
	}

	CString strSortColumn = theApp.getSortColumn();
	if (IsValidSortColumn(strSortColumn))
	{
		int nPos = GetHeaderPos(strSortColumn);
		if (nPos >= 0)
		{
			CHeaderCtrl* pHeader = m_ctrlFileList.GetHeaderCtrl();

			HDITEM hdi = { 0 };
			hdi.mask = HDI_FORMAT;
			VERIFY(pHeader->GetItem(nPos, &hdi));

			int compformat = 0;
			if (theApp.getSortMode() == SORTMODE_ASCENDING)
			{
				compformat = 1;
				hdi.fmt &= ~HDF_SORTDOWN;
				hdi.fmt |= HDF_SORTUP;
			}
			else
			{
				compformat = 2;
				hdi.fmt &= ~HDF_SORTUP;
				hdi.fmt |= HDF_SORTDOWN;
			}

			VERIFY(pHeader->SetItem(nPos, &hdi));
			m_lvci.Init(compformat == 2, false, nPos);
			SortLVData(m_lvci);
		}
	}


	// By this, OnIdle called
	theApp.SetChildViewForIdle(this);


	if (!DoSearch(
		theApp.getFolders()))
	{
		return -1;
	}

	return 0;
}

int CChildView::GetHeaderPos(LPCTSTR pHeaderName)
{
	ASSERT_VALID(&m_ctrlFileList);
	CHeaderCtrl* pHeader = m_ctrlFileList.GetHeaderCtrl();
	for (int i = 0; i < pHeader->GetItemCount(); ++i)
	{
		TCHAR szT[1024]; 
		szT[0] = 0;
		HDITEM hdi = { 0 };
		hdi.mask = HDI_TEXT;
		hdi.pszText = szT;
		static_assert(_countof(szT) == (sizeof(szT) / sizeof(szT[0])), "Must Equal");
		hdi.cchTextMax = _countof(szT);
		if(pHeader->GetItem(i, &hdi))
		{
			if (lstrcmpi(I18N(pHeaderName), szT) == 0)
				return i;
		}
	}
	return -1;
}
void CChildView::OnGetDispInfoFileList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	ASSERT_VALID(&m_ctrlFileList);
	LVITEM& lvItem = ((LV_DISPINFO*)pNMHDR)->item;
	const CLVData& lvData = GetLVData(lvItem.iItem);

	UINT	SHGFI_GETTYPENAME = 0;
	TCHAR * pszBuffer = NULL;
	
	if (lvItem.mask & LVIF_IMAGE)		// image needed
	{
		lvItem.iImage = lvData.GetiIcon();
	}

	if (lvItem.mask & LVIF_TEXT)		// test needed
	{
		switch (lvItem.iSubItem)
		{
		case 0:	// Name
			lvItem.pszText = (LPTSTR)lvData.GetFileName();
			break;

		case 1:	// Size
			lvItem.pszText = (LPTSTR)lvData.GetSizeText();
			break;
		case 2: // Folder
			lvItem.pszText = (LPTSTR)lvData.GetFolder();
			break;
		case 3:	// Type
			lvItem.pszText = (LPTSTR)lvData.GetTypeText();
			break;

		case 4:	// Last Modified
			lvItem.pszText = (LPTSTR)lvData.GetTimeText(m_pOption->m_strTimeFormat);
			break;

		case 5:	// attributes
			break;
		default:
			ASSERT(FALSE);
		}
	}
	*pResult = 0;
}

void CChildView::OnSize(UINT nType, int cx, int cy) 
{
	ASSERT_VALID(&m_ctrlFileList);
	CWnd ::OnSize(nType, cx, cy);
	m_ctrlFileList.MoveWindow (CRect (0, 0, cx, cy), TRUE);	
}

void CChildView::LaunchSelected()
{
	ASSERT_VALID(&m_ctrlFileList);
	POSITION pos = m_ctrlFileList.GetFirstSelectedItemPosition();
	if (!pos)
		return;

	CStringArray arrayFiles;
	int nItem = 0;
	while (pos)
	{
		nItem = m_ctrlFileList.GetNextSelectedItem(pos);
		const CLVData& data = GetLVData(nItem);
		arrayFiles.Add(dqIfSpace(data.GetFullPath()));
	}

	if (arrayFiles.GetSize() >= 10)
	{
		if (IDYES != MessageBox(stdFormat(I18N(L"Are you sure you want to open %d files?"), arrayFiles.GetSize()).c_str(),
			AfxGetAppName(),
			MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2))
		{
			return;
		}
	}

	CWaitCursor wait;
	for (int i = 0; i<arrayFiles.GetSize(); ++i)
	{
		OpenCommon(*this, arrayFiles[i], NULL, NULL, NULL, L"open");
	}
}

void CChildView::OnDblclkFileList (NMHDR*, LRESULT* pResult) 
{
	LaunchSelected();
}

//#ifndef HDF_SORTDOWN
// #define HDF_SORTDOWN 0x0200
// #define HDF_SORTUP   0x0400
//#endif

void CChildView::OnColumnClick(NMHDR* pNMHDR, LRESULT* pResult)
{
	ASSERT_VALID(&m_ctrlFileList);

	BlockedBool bbColumn(&columnClicking_, true, false);
	
	NMLISTVIEW* pLV = (NMLISTVIEW*)pNMHDR;
	
	CHeaderCtrl* pHeader = m_ctrlFileList.GetHeaderCtrl();
	const int hcount = pHeader->GetItemCount();
	ASSERT(hcount == GetHeaderCount());
	int compformat=0;
	for(int i=0 ; i < hcount ; ++i)
	{
		HDITEM hdi = {0};
		hdi.mask = HDI_FORMAT;
		VERIFY(pHeader->GetItem(i, &hdi));

		if(i==pLV->iSubItem)
		{
			if(hdi.fmt & HDF_SORTDOWN)
			{
				compformat = 1;
				hdi.fmt &= ~HDF_SORTDOWN;
				hdi.fmt |= HDF_SORTUP;
			}
			else
			{
				compformat = 2;
				hdi.fmt &= ~HDF_SORTUP;
				hdi.fmt |= HDF_SORTDOWN;
			}
		}
		else
		{
			hdi.fmt &= ~(HDF_SORTUP|HDF_SORTDOWN);
		}

		VERIFY(pHeader->SetItem(i, &hdi));
	}
	
	m_lvci.Init(compformat==2, false, pLV->iSubItem);
	// LVCOMPAREINFO lvci(compformat==2, false, pLV->iSubItem);
	// m_ctrlFileList.SortItems(compareItems, (DWORD_PTR)&m_lvci);
	SortLVData(m_lvci);
}



void CChildView::OnBeginEdit(NMHDR* pNMHDR, LRESULT* pResult)
{
	ASSERT_VALID(&m_ctrlFileList);

	NMLVDISPINFOW* pdi = (NMLVDISPINFOW*)pNMHDR;

	const CLVData& data = GetLVData(pdi->item.iItem);
	*pResult = !PathFileExists(data.GetFullPath());
}

void CChildView::OnEndEdit(NMHDR* pNMHDR, LRESULT* pResult)
{
	ASSERT_VALID(&m_ctrlFileList);

	NMLVDISPINFOW* pdi = (NMLVDISPINFOW*)pNMHDR;

	if(pdi->item.pszText == NULL)
		return;

	CString strNewName = pdi->item.pszText;

	const CLVData& data = GetLVData(pdi->item.iItem);
	*pResult = !PathFileExists(data.GetFullPath());

	wstring parentdir = stdGetParentDirectory((LPCTSTR)data.GetFullPath());
	wstring strFullNewName = stdCombinePath(parentdir.c_str(), (LPCTSTR)strNewName);

	if(PathFileExists(strFullNewName.c_str()))
	{
		CString message;
		message.Format(
			I18N(L"\"%s\" already exists? Are you sure to overwrite?"),
			strFullNewName.c_str());
		if(IDYES != AfxMessageBox(message, MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2))
		{
			*pResult = FALSE;
			return;
		}
	}
	if(!MoveFileEx(data.GetFullPath(), strFullNewName.c_str(), MOVEFILE_REPLACE_EXISTING))
	{
		DWORD dwL = GetLastError();
		wstring error = GetLastErrorString(dwL);
		AfxMessageBox(error.c_str());
		*pResult = FALSE;
		return;
	}

	WIN32_FIND_DATA wfd = {0};
	HANDLE hFind = FindFirstFile (strFullNewName.c_str(), &wfd);
	if(hFind==INVALID_HANDLE_VALUE)
	{
		DWORD dwL = GetLastError();
		wstring error = GetLastErrorString(dwL);
		AfxMessageBox(error.c_str());
		*pResult = FALSE;
		return;
	}
	FindClose (hFind);

	CLVData* pDNew = new CLVData(stdGetParentDirectory(strFullNewName.c_str(), true).c_str(), wfd);
	SetLVData(pdi->item.iItem, pDNew);
	PostMessage(WM_APP_AFTERENDEDIT, (pdi->item.iItem), 0);
	
	*pResult = TRUE;
	return;
}

LRESULT CChildView::OnAfterEndEdit(WPARAM wParam, LPARAM lParam)
{
	ASSERT_VALID(&m_ctrlFileList);
	LOGIT(L"OnAfterEndEdit");

	int iItem = (int)wParam;
	m_ctrlFileList.Update(iItem);
	return 0;
}

void CChildView::OnUpdateAppAbout(CCmdUI* pCmdUI) 
{

}

void CChildView::OnUpdateSearchState(CCmdUI* pCmdUI) 
{
	ASSERT_VALID(&m_ctrlFileList);
	switch(GetSearchState())
	{
	case SEARCHSTATE_SEARCHING:	pCmdUI->SetText(I18N(L"Searching..."));	break;
	case SEARCHSTATE_SUSPENDED:	pCmdUI->SetText(I18N(L"Paused"));	break;
	case SEARCHSTATE_FINISHED:	pCmdUI->SetText(I18N(L"Finished"));		break;
	}
}
void CChildView::OnUpdateSelectedNum(CCmdUI* pCmdUI) 
{
	ASSERT_VALID(&m_ctrlFileList);
	static LPCTSTR f1 = I18N(L"Sel : %d item");
	static LPCTSTR f2 = I18N(L"Sel : %d items");
	int count = m_ctrlFileList.GetSelectedCount();
	CString s;
	s.Format(count==1 ? f1 : f2, count);

	pCmdUI->SetText(s);
}
void CChildView::OnUpdateNum(CCmdUI* pCmdUI) 
{
	ASSERT_VALID(&m_ctrlFileList);
	static LPCTSTR f1 = I18N(L"All : %d item");
	static LPCTSTR f2 = I18N(L"All : %d items");
	int count = m_ctrlFileList.GetItemCount();
	CString s;
	s.Format(count==1 ? f1 : f2, count);

	pCmdUI->SetText(s);
}
void CChildView::OnUpdateSearchError(CCmdUI* pCmdUI)
{
	static LPCTSTR f1 = I18N(L"Error : %d item");
	static LPCTSTR f2 = I18N(L"Error : %d items");
	int count = m_pErrorDialog ? m_pErrorDialog->GetErrorCount() : 0;
	CString s;
	s.Format(count == 1 ? f1 : f2, count);

	pCmdUI->SetText(s);
}
void showpidl(LPITEMIDLIST pidl)
{
	TRACE1("sizeof ITEMIDLIST is %d\r\n", sizeof(ITEMIDLIST));
	TRACE1("sizeof SHITEMID is %d\r\n", sizeof(SHITEMID));

	wstring str;
	if(!pidl)
		str = L"<NULL>";
	else
	{
		for(LPITEMIDLIST p = pidl; p->mkid.cb ; )
		{
			TCHAR szT[512];
			wsprintf(szT, L"size is %d : ",p->mkid.cb);
			TRACE(szT);
			for(USHORT i=0 ; i < p->mkid.cb ; ++i)
			{
				// wsprintf(szT, L"%x", p->mkid.abID[i]);
				wsprintf(szT, L"%x", *( (BYTE*)p+i ) );
				TRACE(szT);
			}
			
			TRACE(L"\r\n");
			p = (LPITEMIDLIST)( ((BYTE*)p) + p->mkid.cb );
		}
	}
}

HRESULT getPIDLsFromPath(const CStringArray& arrayFiles, LPCITEMIDLIST** ppItemIDList)
{
	IShellFolderPtr pSFDesktop;
	SHGetDesktopFolder(&pSFDesktop);

	int nCount = (int)arrayFiles.GetSize();
	LPCITEMIDLIST* pRet= (LPCITEMIDLIST*)calloc( sizeof(LPITEMIDLIST), nCount+1 );
	for(int i=0 ; i < nCount ; ++i)
	{
		LPITEMIDLIST pidl = NULL;
		pSFDesktop->ParseDisplayName (NULL, 0, (LPWSTR)(LPCWSTR)arrayFiles[i], NULL, &pidl, NULL);
		pRet[i] = pidl;
#ifdef _DEBUG
		showpidl(pidl);
#endif
		// get relative pidl via SHBindToParent
		//SHBindToParentEx (pidl, IID_IShellFolder, (void **) &psfFolder, (LPCITEMIDLIST *) &pidlItem);
		//m_pidlArray[i] = CopyPIDL (pidlItem);	// copy relative pidl to pidlArray
		//free (pidlItem);
		//lpMalloc->Free (pidl);		// free pidl allocated by ParseDisplayName
		//psfFolder->Release ();
	}
	*ppItemIDList = pRet;
	return S_OK;
}
void freeppItemIDList(LPCITEMIDLIST* pItemIDList)
{
	if(!pItemIDList)
		return;

	IMallocPtr pMalloc;
	SHGetMalloc(&pMalloc);

	LPCITEMIDLIST* p = pItemIDList;
	while(*p)
	{
		pMalloc->Free((void*) *p);
		p++;
	}
	free(pItemIDList);
}
int countPidls(LPCITEMIDLIST* pItemIDList)
{
	int ret=0;
	while(*pItemIDList)
	{
		ret++;
		pItemIDList++;
	}

	return ret;
}

#define DFM_INVOKECOMMANDEX 12
HRESULT CALLBACK shellcb(
  IShellFolder *psf,
  HWND         hwnd,
  IDataObject  *pdtobj,
  UINT         uMsg,
  WPARAM       wParam,
  LPARAM       lParam)
{
	switch (uMsg)
	{
	case DFM_MERGECONTEXTMENU:
		// Must return S_OK even if we do nothing else or Vista and later
		// won't add standard verbs
		TRACE0("DFM_MERGECONTEXTMENU\r\n");
		return S_OK;
	
	case DFM_INVOKECOMMAND: // Required to invoke default action
		TRACE0("DFM_INVOKECOMMAND\r\n");
		return S_FALSE;

//	case DFM_INVOKECOMMANDEX:
//		TRACE0("DFM_INVOKECOMMANDEX\r\n");
//		return S_FALSE;
//	
//	case DFM_GETDEFSTATICID: // Required for Windows 7 to pick a default
//		TRACE0("DFM_GETDEFSTATICID\r\n");
//		return S_FALSE; 
	
	default:
		TRACE1("DEFAULT %d\r\n", uMsg);
		return E_NOTIMPL; // Required for Windows 7 to show any menu at all
	}

	return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CContextMenuCB::CallBack( 
        /* [unique][in] */ 
          IShellFolder *psf,
        /* [in] */ 
          HWND hwndOwner,
        /* [unique][in] */ 
          IDataObject *pdtobj,
        /* [in] */ 
          UINT uMsg,
        /* [in] */ 
          WPARAM wParam,
        /* [in] */ 
          LPARAM lParam)
{
	return shellcb(psf,hwndOwner,pdtobj,uMsg,wParam,lParam);
}

void getExtsFromPath(const CStringArray& arrayFiles, set<wstring>& exts)
{
	int nCount = (int)arrayFiles.GetSize();
	for(int i=0 ; i < nCount ; ++i)
	{
		int nDot = arrayFiles[i].ReverseFind(L'.');
		if(nDot<0)
			continue;
		int nBS = arrayFiles[i].ReverseFind(L'\\');
		if(nBS > nDot)
			continue;

		wstring ext(arrayFiles[i].Right(arrayFiles[i].GetLength()-nDot));
		std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
		exts.insert(ext);
	}
}
HRESULT getKeysFromPath(const CStringArray& arrayFiles, HKEY** ppKey, UINT* pnKeyCount)
{
	HKEY buf[16] = {0};
	int bufi = 0;
	
	if(ERROR_SUCCESS==RegOpenKeyEx(
		HKEY_CLASSES_ROOT,
		L"*",
		0,					// ulOptions 
		KEY_READ,
		&buf[bufi]))
	{
		++bufi;
	}

	if(!false)
	{
		set<wstring> exts;
		getExtsFromPath(arrayFiles, exts);

		set<wstring>::iterator it=exts.begin();
		for( ; it != exts.end() ; ++it)
		{
			if(ERROR_SUCCESS==RegOpenKeyEx(
				HKEY_CLASSES_ROOT,
				it->c_str(),
				0,					// ulOptions 
				KEY_READ,
				&buf[bufi]))
			{
				++bufi;
				if(bufi >= 16)
					break;

				HKEY hKey = buf[bufi-1];
				DWORD dwType=0;
				BYTE byteBuff[512];
				DWORD dwBuffSize = sizeof(byteBuff);
				if(ERROR_SUCCESS==RegQueryValueEx(
					hKey, 
					NULL,	// default key
					NULL,	// reserved
					&dwType,// type
					byteBuff,// ret
					&dwBuffSize))
				{
					if(dwType==REG_SZ)
					{
						LPCWSTR pFT = (LPCWSTR)byteBuff;

						if(ERROR_SUCCESS==RegOpenKeyEx(
							HKEY_CLASSES_ROOT,
							pFT,
							0,					// ulOptions 
							KEY_READ,
							&buf[bufi]))
						{
							++bufi;
							if(bufi >= 16)
								break;
						}
					}
				}
			}
		}
	}

	*ppKey = (HKEY*)calloc(sizeof(HKEY), 16);
	memcpy(*ppKey, buf, sizeof(buf));
	*pnKeyCount = bufi;
	return S_OK;
}
void freepKey(HKEY* pKey)
{
	if(!pKey)
		return;

	for(int i=0 ; i < 16 ; ++i)
	{
		if(!pKey[i])
			break;

		RegCloseKey(pKey[i]);
	}

	free(pKey);
}


CString removeExt(CString f)
{
	int ind = f.ReverseFind(L'.');
	if(ind <0)
		return f;

	return f.Left(ind);
}

BOOL createFileMenu(HMENU hSendTo, LPCTSTR pDirectory, map<UINT, wstring>& sendtomap)
{
	FILESINFOW fi;
	if(!GetFilesInfoW(pDirectory, fi))
		RETURNFALSE;

	fi.Sort();

	size_t count = fi.GetCount();

	for(size_t i=0 ; i < count ; ++i)
	{
		if( (fi[i].dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
			continue;

		if(!InsertMenu(hSendTo, i, MF_BYPOSITION, ID_SHELLCUSTOM_SENDTO_START + i, removeExt(fi[i].cFileName)))
			RETURNFALSE;

		sendtomap[ID_SHELLCUSTOM_SENDTO_START + i]=fi[i].cFileName;
	}

	return TRUE;
}

BOOL CreateShellMenu(HMENU hmenu, IContextMenuPtr pcm, map<UINT, wstring>& sendtomap, BOOL bUseMulti)
{
	IContextMenu3Ptr pcm3=pcm;
	if(pcm3)
	{
		if(FAILED(pcm3->QueryContextMenu(hmenu, 1,
					ID_SHELLMENU_START, ID_SHELLMENU_END,
					CMF_NORMAL)))
		{
			return FALSE;
		}
	}
	else
	{
		IContextMenu2Ptr pcm2=pcm;
		if(pcm2)
		{
			if(FAILED(pcm2->QueryContextMenu(hmenu, 1,
						ID_SHELLMENU_START, ID_SHELLMENU_END,
						CMF_NORMAL)))
			{
				return FALSE;
			}
		}
		else
		{
			if(FAILED(pcm->QueryContextMenu(hmenu, 1,
						ID_SHELLMENU_START, ID_SHELLMENU_END,
						CMF_NORMAL)))
			{
				return FALSE;
			}
		}
	}

	UINT count = GetMenuItemCount(hmenu);
	int iFirstSep = -1;
	for(UINT i=0 ; i < count ; ++i)
	{
		MENUITEMINFO mii={0};
		mii.cbSize = sizeof(mii);
		mii.fMask = MIIM_TYPE;
		if(!GetMenuItemInfo(hmenu, i, TRUE, &mii))
			return FALSE;

		if( mii.fType & MF_SEPARATOR )
		{
			iFirstSep = i;
			break;
		}
	}

	if(iFirstSep < 0)
	{
		ASSERT(FALSE);
		return FALSE;
	}

	if (bUseMulti)
	{
		HMENU hSendTo = CreatePopupMenu();
		{

			ASSERT(hSendTo);

			TCHAR szSendToPath[MAX_PATH];
			if (!SHGetSpecialFolderPath(NULL, szSendToPath, CSIDL_SENDTO, FALSE))
				RETURNFALSE;

			PathAddBackslash(szSendToPath);
			if (!createFileMenu(hSendTo, szSendToPath, sendtomap))
				RETURNFALSE;

		}

		iFirstSep++;
		if (!InsertMenu(hmenu, iFirstSep++, MF_BYPOSITION | MF_POPUP, (UINT_PTR)hSendTo, I18N(L"Send To")))
			return FALSE;
		if (!InsertMenu(hmenu, iFirstSep++, MF_BYPOSITION | MF_SEPARATOR, 0, NULL))
			return FALSE;
		if (!InsertMenu(hmenu, iFirstSep++, MF_BYPOSITION, ID_SHELLCUSTOM_CUT, I18N(L"Cut")))
			return FALSE;
		if (!InsertMenu(hmenu, iFirstSep++, MF_BYPOSITION, ID_SHELLCUSTOM_COPY, I18N(L"Copy")))
			return FALSE;
		if (!InsertMenu(hmenu, iFirstSep++, MF_BYPOSITION | MF_SEPARATOR, 0, NULL))
			return FALSE;
	}

	if(!InsertMenu(hmenu, 0, MF_BYPOSITION,ID_SHELLCUSTOM_OPENPARENT, I18N(L"Open &Parent Folder")))
	{
		return FALSE;
	}
 
	if(!InsertMenu(hmenu, 1, MF_BYPOSITION|MF_SEPARATOR, 0, NULL))
		return FALSE;

	return TRUE;
}

BOOL SetFileOntoClipboard(const CStringArray& arFiles, BOOL bCut)
{
	int i;

	if(!OpenClipboard(NULL))
		return FALSE;
	if(!EmptyClipboard())
		return FALSE;

	{
		UINT uBuffSize=0;
		HGLOBAL hgDrop = NULL;

		for(i=0 ; i < arFiles.GetSize() ; ++i)
		{
			uBuffSize += arFiles[i].GetLength() + 1;
		}

		uBuffSize = sizeof(DROPFILES) + sizeof(TCHAR) * (uBuffSize + 1);

		hgDrop = GlobalAlloc ( GHND | GMEM_SHARE, uBuffSize );
		if ( NULL == hgDrop )
			return FALSE;

		DROPFILES* pDrop = (DROPFILES*) GlobalLock ( hgDrop );
		if ( NULL == pDrop )
		{
			GlobalFree ( hgDrop );
			return TRUE;
		}

		pDrop->pFiles = sizeof(DROPFILES);

#ifdef _UNICODE
		pDrop->fWide = TRUE;
#endif

		LPTSTR pszBuff = (TCHAR*) (LPBYTE(pDrop) + sizeof(DROPFILES));
		for(i=0 ; i < arFiles.GetSize() ; ++i)
		{
			lstrcpy ( pszBuff, (LPCTSTR)arFiles[i]);
			pszBuff = 1 + _tcschr ( pszBuff, '\0' );
		}

		GlobalUnlock ( hgDrop );

		if(!SetClipboardData(CF_HDROP, hgDrop))
			return FALSE;
	}

	{
		static UINT CF_PREFERREDDROPEFFECT = RegisterClipboardFormat(CFSTR_PREFERREDDROPEFFECT);
		HGLOBAL hgDE = GlobalAlloc ( GHND | GMEM_SHARE, sizeof(DWORD) );
		if ( NULL == hgDE )
			return FALSE;

		DWORD* pDE = (DWORD*) GlobalLock ( hgDE );
		if ( NULL == pDE )
		{
			GlobalFree ( hgDE );
			return FALSE;
		}
		
		*pDE = bCut ? DROPEFFECT_MOVE : DROPEFFECT_COPY;

		GlobalUnlock ( hgDE );

		if(!SetClipboardData(CF_PREFERREDDROPEFFECT, hgDE))
			return FALSE;
	}
	
	CloseClipboard();
	
	return TRUE;
}

BOOL isSameFolders(const CStringArray& files)
{
	if(files.GetSize()==0)
		return FALSE;

	wstring dirbase = GetDirFromPath(files[0]);
	for(int i=1 ; i < files.GetSize() ; ++i)
	{
		wstring dir = GetDirFromPath(files[i]);
		if(0 != lstrcmpi(dirbase.c_str(), dir.c_str()))
			return FALSE;
	}

	return TRUE;
}


void CChildView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	ASSERT_VALID(&m_ctrlFileList);

	POSITION pos = m_ctrlFileList.GetFirstSelectedItemPosition ();
	if (!pos)
		return;

	if (point.x == -1 && point.y == -1) 
	{
		point.x = point.y = 0;
		pWnd->ClientToScreen(&point);
	}

	CStringArray arFiles;
	int nItem = 0;
	while (pos)
	{
		nItem = m_ctrlFileList.GetNextSelectedItem (pos);
		// CLVData* pData = (CLVData*)m_ctrlFileList.GetItemData(nItem);
		const CLVData& data = GetLVData(nItem);
		// ASSERT(pData);
		arFiles.Add (data.GetFullPath());
	}

	// BOOL bUseMulti = !isSameFolders(arFiles);
	BOOL bUseMulti = arFiles.GetSize() > 1;

	LPCITEMIDLIST* pItemIDList = NULL;
	HKEY* pKey=NULL;
	IShellFolderPtr pSFDesktop;
	SHGetDesktopFolder(&pSFDesktop);
	CShellFolderDS spy;
	HRESULT hr = E_FAIL;
	IContextMenuPtr pcm;

	do
	{
		if(bUseMulti)
		{
			if(FAILED(getPIDLsFromPath(arFiles, &pItemIDList)))
			{
				AfxMessageBox(I18N(L"failed to get pidls"));
				break;;
			}
			ASSERT(arFiles.GetSize()==countPidls(pItemIDList));

			UINT nKeyCount=0;
			if(FAILED(getKeysFromPath(arFiles, &pKey, &nKeyCount)))
			{
				AfxMessageBox(I18N(L"failed to get pidls keys"));
				break;
			}

			spy.construct(pSFDesktop, arFiles);
			IShellFolder* pShellFolderDS=NULL;
			spy.InternalQueryInterface(&IID_IShellFolder, (void**)&pShellFolderDS);
			
			if(fnSHCreateDefaultContextMenu)
			{
				DEFCONTEXTMENU dcm = {0};
				dcm.hwnd = *pWnd;
				dcm.pcmcb = &m_cccb;
				dcm.pidlFolder = NULL;
				dcm.psf = pShellFolderDS;
				dcm.cidl = countPidls(pItemIDList);
				dcm.apidl = pItemIDList;
				
				dcm.cKeys = nKeyCount;
				dcm.aKeys = pKey;

				hr = fnSHCreateDefaultContextMenu(&dcm, IID_IContextMenu, (void**)&pcm);
			}
			else
			{
				hr = CDefFolderMenu_Create2(
					NULL,				// An ITEMIDLIST structure for the parent folder. This value can be NULL.
					*pWnd,				//
					countPidls(pItemIDList), // The number of ITEMIDLIST structures in the array pointed to by apidl.
					pItemIDList,		// A pointer to an array of ITEMIDLIST structures, one for each item that is selected.
					pShellFolderDS,	// A pointer to the parent folder's IShellFolder interface. 
										// This IShellFolder must support the IDataObject interface. 
										// If it does not, CDefFolderMenu_Create2 fails and returns E_NOINTERFACE. 
										// This value can be NULL.
					shellcb,			// The LPFNDFMCALLBACK callback object. This value can be NULL if the callback object is not needed.
					0,//nKeyCount,			// The number of registry keys in the array pointed to by ahkeys.
					NULL, //pKey,				// A pointer to an array of registry keys that specify the context menu handlers
										// used with the menu's entries. For more information on context menu handlers, 
										// see Creating Context Menu Handlers. This array can contain a maximum of 16 registry keys.
					&pcm				// The address of an IContextMenu interface pointer that, 
										// when this function returns successfully, points to the IContextMenu object that represents the context menu.
										);
			}										
			spy.InternalRelease();
		}
		else
		{
			hr = GetUIObjectOfFile(*pWnd, 
			arFiles[0],
			IID_IContextMenu,
			(void**)&pcm);
		}
		
		if (SUCCEEDED(hr))
		{
			HMENU hmenu = CreatePopupMenu();
			map<UINT, wstring> sendtomap;
			if(hmenu && CreateShellMenu(hmenu, pcm, sendtomap, bUseMulti))
			{
				pcm->QueryInterface(IID_IContextMenu2, (void**)&m_pcm2);
				pcm->QueryInterface(IID_IContextMenu3, (void**)&m_pcm3);
				m_pcm = pcm;
				
				int iCmd = TrackPopupMenuEx(hmenu, TPM_RETURNCMD,
					point.x, point.y, *pWnd, NULL);
				
				((CMainFrame*)theApp.m_pMainWnd)->SetMessageText(L"");
				if (iCmd == ID_SHELLCUSTOM_OPENPARENT) 
				{
					
					OpenFolder(*this, arFiles[0]);
				}
				else if(ID_SHELLCUSTOM_SENDTO_START <= iCmd && iCmd <= ID_SHELLCUSTOM_SENDTO_END)
				{
					wstring runfile = sendtomap[iCmd];
					ASSERT(runfile.size() != 0);

					TCHAR szSendToPath[MAX_PATH];
					if(SHGetSpecialFolderPath(NULL, szSendToPath, CSIDL_SENDTO, FALSE))
					{
						PathAddBackslash(szSendToPath);
						lstrcat(szSendToPath, runfile.c_str());

						CString arg;
						for(int i=0 ; i<arFiles.GetSize() ; ++i)
						{
							CString s = arFiles[i];
							if(s[0] != _T('"') && s.Find(_T(' ')) >= 0)
								s = _T("\"") + s + _T("\"");

							arg += s;
							arg += _T(" ");
						}
						arg.TrimRight();

						CString param = dqIfSpace(szSendToPath) + L" " + arg;
						int len=arg.GetLength();

						if (IsWinVistaOrHigher())
							OpenCommon(*this, szSendToPath, arg, NULL);
						else
							OpenCommonShortcutSpecial(*this, szSendToPath, arg, NULL);
					}
				}
				else if(iCmd == ID_SHELLCUSTOM_CUT)
				{
					SetFileOntoClipboard(arFiles, TRUE);
				}
				else if(iCmd == ID_SHELLCUSTOM_COPY)
				{
					SetFileOntoClipboard(arFiles, FALSE);
				}
				else if (iCmd > 0) 
				{
					CMINVOKECOMMANDINFOEX info = { 0 };
					info.cbSize = sizeof(info);
					info.fMask = CMIC_MASK_PTINVOKE;
					if (GetKeyState(VK_CONTROL) < 0) 
					{
						info.fMask |= CMIC_MASK_CONTROL_DOWN;
					}
					if (GetKeyState(VK_SHIFT) < 0) 
					{
						info.fMask |= CMIC_MASK_SHIFT_DOWN;
					}
					info.hwnd = *pWnd;
					info.lpVerb = MAKEINTRESOURCEA(iCmd - ID_SHELLMENU_START);
					info.nShow = SW_SHOWNORMAL;
					info.ptInvoke = point;
					
					CWaitCursor wait;
					pcm->InvokeCommand((LPCMINVOKECOMMANDINFO)&info);
				}
							
				m_pcm = NULL;
				m_pcm2 = NULL;
				m_pcm3 = NULL;

				DestroyMenu(hmenu);
			}
		}
	} while(0);

	freepKey(pKey);
	freeppItemIDList(pItemIDList);
}
	

LRESULT CChildView::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	ASSERT_VALID(&m_ctrlFileList);

	//if (m_pcm3) 
	//{
	//	LRESULT lres;
	//	if (SUCCEEDED(m_pcm3->HandleMenuMsg2(message, wParam, lParam,	&lres))) 
	//	{
	//		return lres;
	//	}
	//}
	//else if (m_pcm2) 
	//{
	//	if (SUCCEEDED(m_pcm2->HandleMenuMsg(message, wParam, lParam))) 
	//	{
	//		return 0;
	//	}
	//}

	return CWnd ::WindowProc(message, wParam, lParam);
}

void CChildView::OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu) 
{
	ASSERT_VALID(&m_ctrlFileList);

	CWnd ::OnMenuSelect(nItemID, nFlags, hSysMenu);
	if (m_pcm != NULL)
	{
		if( (nItemID >= ID_SHELLMENU_START) && (nItemID <= ID_SHELLMENU_END) )
		{
			WCHAR szBuf[MAX_PATH];

			if(true)
			{
				if (FAILED(IContextMenu_GetCommandString(m_pcm,
												   nItemID - ID_SHELLMENU_START,
												   GCS_HELPTEXTW, NULL,
												   szBuf, MAX_PATH))) 
				{
					szBuf[0]=0;
				}
			}
			else
			{
				if (FAILED(m_pcm->GetCommandString(
					nItemID - ID_SHELLMENU_START,
					GCS_HELPTEXT,
					NULL,
					(LPSTR)szBuf,
					MAX_PATH))) 
				{
					// lstrcpyn(szBuf, TEXT("No help available."), MAX_PATH);
					szBuf[0]=0;
				}
			}
			((CMainFrame*)theApp.m_pMainWnd)->SetMessageText(szBuf);
		}
		else if(nItemID==ID_SHELLCUSTOM_OPENPARENT)
		{
			((CMainFrame*)theApp.m_pMainWnd)->SetMessageText(I18N(L"Open Parent Folder"));
		}
	}	
}

void CChildView::OnUpdateShellContextMenu(CCmdUI* pCmdUI) 
{
	AfxMessageBox(L"fsdjoafj");
}
void CChildView::OnUpdateSOP(CCmdUI* pCmdUI)
{
	AfxMessageBox(L"fsdjoafj");
}
BOOL CChildView::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) 
{
	return CWnd ::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

BOOL CChildView::PreTranslateMessage(MSG* pMsg) 
{
	return CWnd ::PreTranslateMessage(pMsg);
}


#ifdef DEFINE_GUID
#undef DEFINE_GUID
#endif

#define DEFINE_GUID(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8) \
        EXTERN_C const GUID DECLSPEC_SELECTANY name \
                = { l, w1, w2, { b1, b2,  b3,  b4,  b5,  b6,  b7,  b8 } }
DEFINE_GUID(IID_IContextMenuCB, 0x3409e930, 0x5a39, 0x11d1, 0x83, 0xfa, 0x0, 0xa0, 0xc9, 0xd, 0xc8, 0x49); 


void CChildView::OnReset() 
{
	ASSERT_VALID(&m_ctrlFileList);

#ifdef _DEBUG
	wstring s1 = L"wcfindD.exe";
#else
	wstring s1 = L"wcfind.exe";
#endif

	CCommandLineString cls(GetCommandLine());
	wstring exe = cls[0];
	std::wstring::size_type Pos( exe.find( s1 ) );

    if( Pos == std::string::npos )
	{
		AfxMessageBox(I18N(L"wcfind.exe not found"));
		return;
	}
	
#ifdef _DEBUG
	exe.replace( Pos, s1.length(), L"wcfindstartD.exe" );
#else
	exe.replace( Pos, s1.length(), L"wcfindstart.exe" );
#endif

	vector<wstring> vc;
	vc.push_back(exe);
	vc.push_back(L"-nopastefilename");
	for (size_t i = 1; i < cls.getCount(); ++i)
	{
		vc.push_back(cls[i]);
	}
	wstring cmd = CCommandLineString::getCommandLine(vc);

	//LPWSTR pCmd = _tcsdup(cmd.c_str());
	//STLSOFT_SCODEDFREE_CRT(pCmd);
	unique_ptr<WCHAR, void(__cdecl*)(void*)> pCmd(static_cast<WCHAR*>(_tcsdup(cmd.c_str())), free);

	STARTUPINFO si = {0};
	si.cb = sizeof(si);
	PROCESS_INFORMATION pi = {0};
	if (!CreateProcess(
		NULL,
		pCmd.get(),
		NULL,
		NULL,
		FALSE,
		0,
		NULL,
		NULL,
		&si,
		&pi))
	{
		wstring error = GetLastErrorString(GetLastError());
		error += L"\r\n";
		error += pCmd.get();
		AfxMessageBox(error.c_str());
		return;
	}
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}

void CChildView::OnUpdateReset(CCmdUI* pCmdUI) 
{
	
}

void CChildView::OnRestart() 
{
	ASSERT_VALID(&m_ctrlFileList);
	DoSearch(theApp.getFolders());
}

void CChildView::OnUpdateRestart(CCmdUI* pCmdUI) 
{

}

void CChildView::OnDestroy() 
{
	ASSERT_VALID(&m_ctrlFileList);
	
	theApp.SetChildViewForIdle(nullptr);

	CHeaderCtrl* pHeader = m_ctrlFileList.GetHeaderCtrl();
	const int headerCount = pHeader->GetItemCount();

	std::vector<int> nOrder(headerCount);
	VERIFY(pHeader->GetOrderArray(nOrder.data(), headerCount));
	
	BOOL bFail = FALSE;
	bFail |= FALSE==theApp.WriteProfileBinary(
		SECTION_OPTION,
		KEY_COLUMNORDER,
		(LPBYTE)nOrder.data(),
		sizeof(int)*nOrder.size());
	
	if (m_pFontList)
	{
		LOGFONT lf;
		if (0 != m_pFontList->GetLogFont(&lf))
		{
			bFail |= FALSE == theApp.WriteProfileBinary(
				SECTION_OPTION,
				KEY_FONTLIST,
				(LPBYTE)&lf,
				sizeof(lf));
		}
	}
	std::vector<int> nWidths(headerCount);
	int i=0;
	BOOL bSizeFail = FALSE;
	for (i = 0; i < headerCount; ++i)
	{
		HD_ITEM hi = {0};
		hi.mask = HDI_WIDTH;
		bSizeFail |= FALSE == pHeader->GetItem(i, &hi);
		nWidths[i] = hi.cxy;
	}
	if(!bSizeFail)
	{
		bFail |= FALSE == theApp.WriteProfileBinary(
			SECTION_OPTION,
			KEY_COLUMNWIDTH,
			(LPBYTE)nWidths.data(),
			sizeof(int) * nWidths.size());
	}


	bFail |= FALSE == m_pOption->Save();
	delete m_pOption;
	m_pOption = NULL;

	delete m_pErrorDialog;
	m_pErrorDialog = NULL;

	if(bFail)
		AfxMessageBox(I18N(L"Save Failed"));

	m_hCurThread = nullptr;

	gworkId++;
	VERIFY(SetEvent(m_hRun));
	WaitForMultipleObjects(gAllThreads.size(), gAllThreads.data(), TRUE,
#ifdef _DEBUG
		INFINITE
#else
		10 * 1000
#endif
	);
	for (auto& h : gAllThreads)
		VERIFY(CloseHandle(h));
	CWnd::OnDestroy();
}

void CChildView::OnEditOption() 
{
	ASSERT_VALID(&m_ctrlFileList);

	CString prevTimeFormat = m_pOption->m_strTimeFormat;

	if (IDOK != m_pOption->DoModal())
		return;

	DWORD style = m_ctrlFileList.GetExtendedStyle();

	if (m_pOption->IsFullRowSelect())
		style |= LVS_EX_FULLROWSELECT;
	else
		style &= ~LVS_EX_FULLROWSELECT;

	m_ctrlFileList.SetExtendedStyle(style);

	if (prevTimeFormat != m_pOption->m_strTimeFormat)
	{
		int count = m_ctrlFileList.GetItemCount();
		for (int i = 0; i < count; ++i)
		{
			// CLVData* pData = (CLVData*)m_ctrlFileList.GetItemData(i);
			// ASSERT(pData);
			const CLVData& data = GetLVData(i);
			data.ResetTimeFormatText();
		}
	}
}

void CChildView::OnUpdateEditLabel(CCmdUI* pCmdUI) 
{
	ASSERT_VALID(&m_ctrlFileList);
	pCmdUI->Enable(m_ctrlFileList.GetSelectedCount()!=0);
}

void CChildView::OnEditLabel() 
{
	ASSERT_VALID(&m_ctrlFileList);
	POSITION pos = m_ctrlFileList.GetFirstSelectedItemPosition ();
	if (!pos)
		return;

	CStringArray arrayFiles;
	int nItem = m_ctrlFileList.GetNextSelectedItem (pos);
	m_ctrlFileList.EditLabel(nItem);
}

void CChildView::OnUpdateEditCopyAstest(CCmdUI* pCmdUI) 
{
	ASSERT_VALID(&m_ctrlFileList);
	POSITION pos = m_ctrlFileList.GetFirstSelectedItemPosition ();
	pCmdUI->Enable(pos != NULL);
}

void CChildView::OnEditCopyAstest() 
{
	ASSERT_VALID(&m_ctrlFileList);
	POSITION pos = m_ctrlFileList.GetFirstSelectedItemPosition ();
	if (!pos)
		return;

	CStringArray arrayFiles;
	int nItem = 0;
	while (pos)
	{
		nItem = m_ctrlFileList.GetNextSelectedItem (pos);
		//CLVData* pData = (CLVData*)m_ctrlFileList.GetItemData(nItem);
		//ASSERT(pData);
		const CLVData& data = GetLVData(nItem);
		arrayFiles.Add (data.GetFullPath());
	}

	CString strClipText;
	for(int i=0 ; i < arrayFiles.GetSize() ; ++i)
	{
		strClipText += arrayFiles[i];
		if ((i + 1) != arrayFiles.GetSize())
			strClipText += _T("\r\n");
	}

	if(!SetClipboardText(*this, strClipText))
	{
		AfxMessageBox(I18N(L"Failed to SetClipboardText"));
		return;
	}
}

void CChildView::OnUpdateSelectAll(CCmdUI* pCmdUI) 
{
	ASSERT_VALID(&m_ctrlFileList);
	pCmdUI->Enable(m_ctrlFileList.GetItemCount() != 0);
}

void CChildView::OnSelectAll() 
{
	ASSERT_VALID(&m_ctrlFileList);
	int count = m_ctrlFileList.GetItemCount();
	for(int i=0 ; i < count ; ++i)
		m_ctrlFileList.SetItemState(i, LVIS_SELECTED,LVIS_SELECTED);	
}


void CChildView::OnViewErrordialog()
{
	ASSERT_VALID(&m_ctrlFileList);
	ShowErrorDialog();
}

void CChildView::ShowErrorDialog()
{
	ASSERT_VALID(&m_ctrlFileList);
	ASSERT(m_pErrorDialog);

	if (!m_pErrorDialog->IsWindowVisible())
		m_pErrorDialog->ShowWindow(SW_SHOW);
	else
		m_pErrorDialog->ShowWindow(SW_HIDE);
}
void CChildView::OnUpdateViewErrordialog(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck((m_pErrorDialog && m_pErrorDialog->IsWindowVisible()) ? 1 : 0);
}

// copied from MultiFiler
void CChildView::OnBegindragFilelist(NMHDR* pNMHDR, LRESULT* pResult)
{
	ASSERT_VALID(&m_ctrlFileList);
	TRACE(L"DnD Begins\n");
	NMLISTVIEW*    pNMLV = (NMLISTVIEW*)pNMHDR;
	COleDataSource datasrc;
	HGLOBAL        hgDrop;
	DROPFILES*     pDrop;
	CStringList    lsDraggedFiles;
	POSITION       pos;
	int            nSelItem;
	CString        sFile;
	UINT           uBuffSize = 0;
	TCHAR*         pszBuff;
	FORMATETC      etc = { CF_HDROP, NULL, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };

	*pResult = 0;   // return value ignored

	// For every selected item in the list, put the filename into lsDraggedFiles.

	pos = m_ctrlFileList.GetFirstSelectedItemPosition();

	while (NULL != pos)
	{
		nSelItem = m_ctrlFileList.GetNextSelectedItem(pos);
		const CLVData& data = GetLVData(nSelItem);
		sFile = data.GetFullPath();

		lsDraggedFiles.AddTail(sFile);

		// Calculate the # of chars required to hold this string.
		uBuffSize += (lstrlen(sFile) + 1);
	}

	// Add 1 extra for the final null char, and the size of the DROPFILES struct.
	uBuffSize = sizeof(DROPFILES) + (sizeof(TCHAR) * (uBuffSize + 1));

	// Allocate memory from the heap for the DROPFILES struct.
	hgDrop = GlobalAlloc(GHND | GMEM_SHARE, uBuffSize);
	if (NULL == hgDrop)
		return;

	pDrop = (DROPFILES*)GlobalLock(hgDrop);
	if (NULL == pDrop)
	{
		GlobalFree(hgDrop);
		return;
	}

	// Fill in the DROPFILES struct.
	pDrop->pFiles = sizeof(DROPFILES);

#ifdef _UNICODE
	// If we're compiling for Unicode, set the Unicode flag in the struct to
	// indicate it contains Unicode strings.
	pDrop->fWide = TRUE;
#endif

	// Copy all the filenames into memory after the end of the DROPFILES struct.
	pos = lsDraggedFiles.GetHeadPosition();
	pszBuff = (TCHAR*)(LPBYTE(pDrop) + sizeof(DROPFILES));

	while (NULL != pos)
	{
		lstrcpy(pszBuff, (LPCTSTR)lsDraggedFiles.GetNext(pos));
		pszBuff = 1 + _tcschr(pszBuff, '\0');
	}

	GlobalUnlock(hgDrop);

	// Put the data in the data source.
	datasrc.CacheGlobalData(CF_HDROP, hgDrop, &etc);

	// Add in our own custom data, so we know that the drag originated from our 
	// window.  CMyDropTarget::DragEnter() checks for this custom format, and
	// doesn't allow the drop if it's present.  This is how we prevent the user
	// from dragging and then dropping in our own window.
	// The data will just be a dummy bool.
	HGLOBAL hgBool;

	hgBool = GlobalAlloc(GHND | GMEM_SHARE, sizeof(bool));
	if (NULL == hgBool)
	{
		GlobalFree(hgDrop);
		return;
	}

	// Put the data in the data source.
	{
		FORMATETC etc2 = { CF_HDROP, NULL, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };
		etc2.cfFormat = g_uCustomClipbrdFormat;
		datasrc.CacheGlobalData(g_uCustomClipbrdFormat, hgBool, &etc2);
	}

	// Start the drag 'n' drop!
	TRACE(L"start Calling DoDragDrop\n");
	DROPEFFECT dwEffect = datasrc.DoDragDrop(DROPEFFECT_COPY | DROPEFFECT_MOVE);
	TRACE(L"end Calling DoDragDrop\n");
	// If the DnD completed OK, we remove all of the dragged items from our
	// list.

	switch (dwEffect)
	{
	case DROPEFFECT_COPY:
		TRACE(L"DD Copy\n");
		break;
	case DROPEFFECT_MOVE:
		TRACE(L"DD Move\n");
		//{
		//	// The files were copied or moved.
		//	// Note: Don't call GlobalFree() because the data will be freed by the drop target.

		//	for (nSelItem = m_ctrlFileList.GetNextItem(-1, LVNI_SELECTED);
		//		nSelItem != -1;
		//		nSelItem = m_ctrlFileList.GetNextItem(nSelItem, LVNI_SELECTED))
		//	{
		//		m_ctrlFileList.DeleteItem(nSelItem);
		//		nSelItem--;
		//	}

		//	// Resize the list columns.

		//	//m_ctrlFileList.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER);
		//	//m_ctrlFileList.SetColumnWidth(1, LVSCW_AUTOSIZE_USEHEADER);
		//	//m_ctrlFileList.SetColumnWidth(2, LVSCW_AUTOSIZE_USEHEADER);
		//}
		//break;

	case DROPEFFECT_NONE:
		TRACE(L"DD None\n");
		{
			// This needs special handling, because on NT, DROPEFFECT_NONE
			// is returned for move operations, instead of DROPEFFECT_MOVE.
			// See Q182219 for the details.
			// So if we're on NT, we check each selected item, and if the
			// file no longer exists, it was moved successfully and we can
			// remove it from the list.

			if (g_bNT)
			{
				bool bDeletedAnything = false;

				for (nSelItem = m_ctrlFileList.GetNextItem(-1, LVNI_SELECTED);
					nSelItem != -1;
					nSelItem = m_ctrlFileList.GetNextItem(nSelItem, LVNI_SELECTED))
				{
					//CLVData* pData = (CLVData*)m_ctrlFileList.GetItemData(0);
					//ASSERT(pData);
					const CLVData& data = GetLVData(nSelItem);
					CString sFilename = data.GetFullPath();
					// CString sFilename = m_ctrlFileList.GetItemText(nSelItem, 0);

					if (0xFFFFFFFF == GetFileAttributes(sFilename) &&
						GetLastError() == ERROR_FILE_NOT_FOUND)
					{
						// We couldn't read the file's attributes, and GetLastError()
						// says the file doesn't exist, so remove the corresponding 
						// item from the list.

						// m_ctrlFileList.DeleteItem(nSelItem);
						RemoveLVData(nSelItem);

						nSelItem--;
						bDeletedAnything = true;
					}
				}

				// Resize the list columns if we deleted any items.

				if (bDeletedAnything)
				{
					//m_ctrlFileList.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER);
					//m_ctrlFileList.SetColumnWidth(1, LVSCW_AUTOSIZE_USEHEADER);
					//m_ctrlFileList.SetColumnWidth(2, LVSCW_AUTOSIZE_USEHEADER);

					// Note: Don't call GlobalFree() because the data belongs to 
					// the caller.
					m_ctrlFileList.Invalidate();
					m_ctrlFileList.UpdateWindow();
				}
				else
				{
					// The DnD operation wasn't accepted, or was canceled, so we 
					// should call GlobalFree() to clean up.

					GlobalFree(hgDrop);
					GlobalFree(hgBool);
				}
			}   // end if (NT)
			else
			{
				// We're on 9x, and a return of DROPEFFECT_NONE always means
				// that the DnD operation was aborted.  We need to free the
				// allocated memory.

				GlobalFree(hgDrop);
				GlobalFree(hgBool);
			}
		}
		break;  // end case DROPEFFECT_NONE
	}   // end switch
}

void CChildView::OnUpdateSearchSearchagainbysettingclipboardtextsfilename(CCmdUI *pCmdUI)
{
	ASSERT_VALID(&m_ctrlFileList);
	ASSERT_VALID(&m_ctrlFileList);
	pCmdUI->Enable(IsClipboardFormatAvailable(CF_UNICODETEXT));
}


BOOL PrepareSaFileNames(LPCWSTR pStr, FileNameArray& sa)
{
	if (!pStr || pStr[0] == 0)
	{
		return TRUE;
	}

	int nArgs = 0;
	// LPWSTR* ppArgv = CommandLineToArgvW(pStr, &nArgs);
	unique_ptr<LPWSTR, HLOCAL(WINAPI *)(HLOCAL)>
		arg(::CommandLineToArgvW(pStr, &nArgs), ::LocalFree);

	if (!arg)
	{
		return FALSE;
	}
	// STLSOFT_SCOPEDFREE(ppArgv, void*, LocalFree);

	for (int i = 0; i < nArgs; ++i)
	{
		sa.push_back(FilenameOption(arg.get()[i], FALSE));
	}
	return TRUE;
}
void CChildView::OnSearchSearchagainbysettingclipboardtextsfilename()
{
	wstring strClipboard;
	if (!GetClipboardText(*this, strClipboard))
	{
		AfxMessageBox(I18N(L"Failed to get clipboard text."));
		return;
	}
	if (strClipboard.empty())
	{
		AfxMessageBox(I18N(L"Clipboard is empty."));
		return;
	}
	
	FileNameArray sa;
	if (!PrepareSaFileNames(strClipboard.c_str(), sa))
	{
		AfxMessageBox(I18N(L"Failed to parse input."));
		return;
	}
	DoSearchAgainByFilename(sa);
}





static CString a2l(const CStringArray& a)
{
	CString ret;
	for (int i = 0; i < a.GetSize(); ++i)
	{
		CString t = a[i];
		if (t.Find(L" ") >= 0)
		{
			t = L"\"" + t + L"\"";
		}
		ret += t;
		ret += L" ";
	}
	ret.TrimRight();
	return ret;
}
static CString a2l(const FileNameArray& a)
{
	CStringArray ar;
	for (size_t i = 0; i < a.size(); ++i)
	{
		ar.Add(a[i].GetFilename());
	}
	
	return a2l(ar);
}
void CChildView::OnFindfileGo()
{
	ASSERT_VALID(&m_ctrlFileList);
	CString strSearch = ((CMainFrame*)theApp.m_pMainWnd)->GetFindFileAgainText();
	
	FileNameArray sa;
	if (!PrepareSaFileNames(strSearch, sa))
	{
		AfxMessageBox(I18N(L"Failed to parse input."));
		return;
	}
	DoSearchAgainByFilename(sa);
	((CMainFrame*)theApp.m_pMainWnd)->SetComboCurrent(a2l(sa));
}
void CChildView::OnComboEnter()
{
	ASSERT_VALID(&m_ctrlFileList);
	OnFindfileGo();
}
void CChildView::CutOrCopyFiles(bool bCopy)
{
	ASSERT_VALID(&m_ctrlFileList);
	CEdit* pEdit = m_ctrlFileList.GetEditControl();
	if (pEdit)
	{
		if (bCopy)
			pEdit->Copy();
		else
			pEdit->Cut();
	}
	else
	{
		POSITION pos = m_ctrlFileList.GetFirstSelectedItemPosition();
		if (!pos)
			return;

		vector<wstring> vs;
		int nItem = 0;
		while (pos)
		{
			nItem = m_ctrlFileList.GetNextSelectedItem(pos);
			//CLVData* pData = (CLVData*)m_ctrlFileList.GetItemData(nItem);
			//ASSERT(pData);
			const CLVData& data = GetLVData(nItem);
			vs.push_back(wstring(data.GetFullPath()));
		}

		if (!SetFileOntoClipboard(vs, !bCopy))
		{
			AfxMessageBox(I18N(L"Failed to set file onto clipboard."));
			return;
		}
	}
}
void CChildView::OnEditCopy()
{
	ASSERT_VALID(&m_ctrlFileList);
	CutOrCopyFiles(true);
}
void CChildView::OnUpdateEditCopy(CCmdUI* pCmdUI)
{
	ASSERT_VALID(&m_ctrlFileList);
	pCmdUI->Enable(!!m_ctrlFileList.GetFirstSelectedItemPosition());
}


void CChildView::OnEditCut()
{
	ASSERT_VALID(&m_ctrlFileList);
	CutOrCopyFiles(false);
}
void CChildView::OnUpdateEditCut(CCmdUI *pCmdUI)
{
	ASSERT_VALID(&m_ctrlFileList);
	OnUpdateEditCopy(pCmdUI);
}

BOOL CChildView::OnIdle(LONG lCount)
{
	ASSERT_VALID(&m_ctrlFileList);
	if (m_ctrlFileList.GetItemCount() != GetLVCount())
	{
		m_ctrlFileList.SetItemCountEx(GetLVCount(), LVSICF_NOSCROLL);
	}

	SortIfNeeded();
	return TRUE;
}

void CChildView::OnLVNKeydown(NMHDR* pNMHDR, LRESULT* pResult)
{
	ASSERT_VALID(&m_ctrlFileList);
	LV_KEYDOWN* pKD = (LV_KEYDOWN*)pNMHDR;

	switch (pKD->wVKey )
	{
	case VK_RETURN:
		LaunchSelected();
		break;
	case 'a':
	case 'A':
		// TODO: Select Nexit Item starting with 'a'
		*pResult = TRUE;
		break;
	}
}


void CChildView::OnEditFont()
{
	ASSERT_VALID(&m_ctrlFileList);
	CFont* pFont = m_ctrlFileList.GetFont();
	if (!pFont)
	{
		AfxMessageBox(I18N(L"Font is null"));
		return;
	}
	LOGFONT lf;
	if (pFont->GetLogFont(&lf) == 0)
	{
		AfxMessageBox(I18N(L"Failed to obtain LOGFONT"));
		return;
	}

	CHOOSEFONT cf = { 0 };
	cf.lStructSize = sizeof(cf);
	cf.hwndOwner = *this;
	cf.hDC = NULL;
	cf.lpLogFont = &lf;
	cf.Flags = CF_INITTOLOGFONTSTRUCT;
	if (!ChooseFont(&cf))
		return;

	CFont font;
	if (!font.CreateFontIndirect(cf.lpLogFont))
	{
		AfxMessageBox(I18N(L"Failed to create font"));
		return;
	}

	m_ctrlFileList.SetFont(&font);

	delete m_pFontList;
	m_pFontList = new CFont();
	m_pFontList->Attach(font.Detach());
}

void CChildView::PostNcDestroy()
{
	ASSERT_VALID(&m_ctrlFileList);
	ASSERT(!::IsWindow(m_ctrlFileList));

	delete m_pFontList;
	m_pFontList = nullptr;

	ParentClass::PostNcDestroy();
}

void CChildView::ChangeFontSize(FONTSIZE fontSize)
{
	ASSERT_VALID(&m_ctrlFileList);
	CFont* pFont = m_ctrlFileList.GetFont();
	if (!pFont)
	{
		AfxMessageBox(I18N(L"Font is null"));
		return;
	}

	LOGFONT lf;
	if (pFont->GetLogFont(&lf) == 0)
	{
		AfxMessageBox(I18N(L"Failed to obtain LOGFONT"));
		return;
	}
	lf.lfHeight -= fontSize==FONTSIZE::SMALLER ? -1 : 1;
	if (lf.lfHeight == 0)
		return;

	CFont font;
	if (!font.CreateFontIndirect(&lf))
	{
		AfxMessageBox(I18N(L"Failed to create font"));
		return;
	}

	m_ctrlFileList.SetFont(&font);

	delete m_pFontList;
	m_pFontList = new CFont();
	m_pFontList->Attach(font.Detach());
}


