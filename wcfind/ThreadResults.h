#pragma once

#include "../../lsMisc/Lock.h"

#include "LVData.h"
#include "thread.h"

class LVCOMPAREINFO {
	bool bInitted_;
	bool bReverse_;
	bool bOriginal_;
	int nCol_;

public:
	LVCOMPAREINFO() {
		bInitted_=false;
	}
	void Init(const bool bReverse, const bool bOriginal, const int nCol) {
		bReverse_= bReverse;
		bOriginal_=bOriginal;
		nCol_=nCol;
		bInitted_=true;
	}

	bool IsInitted() const {
		return bInitted_;
	}
	bool IsReverse() const {
		return bReverse_;
	}
	int GetCol() const {
		return nCol_;
	}
	bool operator==(const LVCOMPAREINFO& info) const {
		return
			bInitted_ == info.bInitted_ &&
			bReverse_ == info.bReverse_ &&
			bOriginal_ == info.bOriginal_ &&
			nCol_ == info.nCol_;
	}
};

class CChildView;
class CThreadArg;
class CThreadResults
{
	int workid_ = 0;
	bool bSortDirty_ = true;

	using MyLock = CReadWriteLock<DynamicTrait>;
	using MyLocker = MyLock::CReadWriteLocker;
	mutable MyLock lock_;

	std::vector<std::shared_ptr<CLVData>> results_;

	LVCOMPAREINFO lastCmpInfo_;

	CThreadResults() {}
	virtual ~CThreadResults(){}

	void reset(int workid) {
		MyLocker locker(lock_, MyLocker::LockType::READWRITE_LOCK);
		results_.clear();
		workid_ = workid;
		bSortDirty_ = true;
	}
private:
	void InsertResult(const std::shared_ptr<CLVData>& result)
	{
		MyLocker locker(lock_, MyLocker::LockType::READWRITE_LOCK);
		if (workid_ != gworkId)
			return;
		results_.push_back(result);
		bSortDirty_ = true;
	}

	const CLVData& get(size_t index) const {
		MyLocker locker(lock_, MyLocker::LockType::READ_LOCK);
		ASSERT(workid_ == gworkId);
		return *results_[index].get();
	}
	void set(size_t index, CLVData* pData) {
		MyLocker locker(lock_, MyLocker::LockType::READWRITE_LOCK);
		if (workid_ != gworkId)
			return;

		results_[index] = std::shared_ptr<CLVData>(pData);
		bSortDirty_ = true;
	}
	void remove(size_t index) {
		MyLocker locker(lock_, MyLocker::LockType::READWRITE_LOCK);
		results_.erase(results_.begin() + index);
		bSortDirty_ = true;
	}
	int GetCount() const {
		MyLocker locker(lock_, MyLocker::LockType::READ_LOCK);
		if (workid_ != gworkId)
			return 0;

		return results_.size();
	}

	bool Sort(LVCOMPAREINFO& cmpinfo);

	friend CChildView;
	friend CThreadArg;

public:
	static CThreadResults* GetInstance() {
		static CThreadResults theInstance;
		return &theInstance;
	}

	void ClearSelections() {
		MyLocker locker(lock_, MyLocker::LockType::READWRITE_LOCK);
		for (auto&& a : results_)
			a->SetSelected(FALSE);
	}
	void GetSelections(std::vector<int>* pVec) const {
		MyLocker locker(lock_, MyLocker::LockType::READ_LOCK);
		if (workid_ != gworkId)
			return;
		for (size_t i = 0; i < results_.size(); ++i)
			if (results_[i]->GetSelected())
				pVec->push_back((int)i);
	}
};

