HRESULT GetUIObjectOfFile(HWND hwnd, LPCWSTR pszPath, REFIID riid, void **ppv);
HRESULT IContextMenu_GetCommandString(
									  IContextMenu *pcm, UINT_PTR idCmd, UINT uFlags,
									  UINT *pwReserved, LPWSTR pszName, UINT cchMax);
BOOL OpenCommonShortcutSpecial(HWND hWnd, LPCTSTR pApp, LPCTSTR pCommand=NULL, LPCTSTR pDirectory=NULL);