//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ で生成されたインクルード ファイル。
// wcfind.rc で使用
//
#define IDD_ABOUTBOX                    100
#define ID_INDICATOR_LISTITEMCOUNT      101
#define IDC_FILELIST                    102
#define ID_INDICATOR_SEARCHSTATE        103
#define ID_INDICATOR_LISTITEMSELECTEDCOUNT 104
#define ID_FINDFILE_COMBO               105
#define ID_INDICATOR_SEARCHERROR        106
#define IDR_MAINFRAME                   128
#define IDR_WCFINDTYPE                  129
#define IDD_DIALOG_OPTION               132
#define IDD_DIALOG_ERROR                134
#define IDC_EDIT_TIMEFORMAT             1000
#define IDC_STATIC_TIMEFORMATRESULT     1001
#define IDC_CHECK_TYPE_DIRECTORY        1004
#define IDC_CHECK_PASTEONSTARTUP        1005
#define IDC_EDIT_ERROR                  1006
#define IDC_STATIC_VERSION              1007
#define IDC_BUTTON1                     1008
#define IDC_BUTTON_GOTOWEBPAGE          1008
#define IDC_BUTTON_DEFAULTTIMEFORMAT    1008
#define IDC_CHECK1                      1009
#define IDC_CHECK_AUTOSORT              1009
#define IDC_CHECK2                      1010
#define IDC_CHECK_FULLROWSELECT         1010
#define IDC_STATIC_TIMEFORMAT           1011
#define IDC_COMBO_LANG                  1012
#define IDC_STATIC_REQUIRES_RESTART     1013
#define IDC_EDIT1                       1015
#define IDC_EDIT_REV                    1015
#define IDC_CHECK3                      1016
#define IDC_CHECK_SHOWERRORATERROR      1016
#define IDC_CHECK4                      1017
#define IDC_CHECK_FOREWINDOWAFTERFINISH 1017
#define ID_PAUSE                        32771
#define ID_REPLAY                       32772
#define ID_SHELLMENU_START              32773
#define ID_SHELLMENU_END                33773
#define ID_SHELLCUSTOM_OPENPARENT       33774
#define ID_SHELLCUSTOM_SENDTO_START     33775
#define ID_SHELLCUSTOM_SENDTO_END       34775
#define ID_SHELLCUSTOM_CUT              34776
#define ID_SHELLCUSTOM_COPY             34777
#define ID_RESTART                      34778
#define ID_RESET                        34779
#define ID_EDIT_OPTION                  34780
#define ID_EDIT_LABEL                   34783
#define ID_EDIT_COPYASTEST              34784
#define ID_SELECT_ALL                   34785
#define ID_VIEW_ERRORDIALOG             34786
#define ID_SEARCH_SEARCHAGAINBYSETTINGCLIPBOARDTEXTSFILENAME 34787
#define ID_FINDFILE_GO                  34790
#define ID_FILE_CLOSEALLINSTANCES       34794
#define ID_FILE_CLOSEOTHERINSTANCES     34795
#define ID_EDIT_FONT                    34796

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         34797
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           107
#endif
#endif
