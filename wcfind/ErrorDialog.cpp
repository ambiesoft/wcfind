﻿#include "stdafx.h"
#include "resource.h"
#include "wcfind.h"
#include "ErrorDialog.h"
#include "LVData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CErrorDialog::CErrorDialog(CWnd* pParent /*=NULL*/)
	: CResizableDialog(CErrorDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CErrorDialog)
		// 
	//}}AFX_DATA_INIT
}


void CErrorDialog::DoDataExchange(CDataExchange* pDX)
{
	CResizableDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CErrorDialog)
		// 
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CErrorDialog, CResizableDialog)
	//{{AFX_MSG_MAP(CErrorDialog)
		// 
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CErrorDialog::Add(CLVData& lvData)
{
	CString strText=_T("\"");
	strText += lvData.GetFullPath();
	strText += _T("\" ");
	strText += GetLastErrorString(lvData.GetLastError()).c_str();
	strText += _T("\r\n");
	LPCTSTR pText=strText;

    // get edit control from dialog
    HWND hwndOutput = ::GetDlgItem( *this, IDC_EDIT_ERROR );

    // get the current selection
    //DWORD StartPos, EndPos;
    //::SendMessage( hwndOutput, EM_GETSEL, reinterpret_cast<WPARAM>(&StartPos), reinterpret_cast<WPARAM>(&EndPos) );

    //// move the caret to the end of the text
    //int outLength = ::GetWindowTextLength( hwndOutput );
    //::SendMessage( hwndOutput, EM_SETSEL, outLength, outLength );

	::SendMessage(hwndOutput, EM_SETSEL, -1, -1);
    // insert the text at the new caret position
    ::SendMessage( hwndOutput, EM_REPLACESEL, TRUE, reinterpret_cast<LPARAM>(pText) );
    ::SendMessage( hwndOutput, EM_SETSEL, -1, -1 );

	count_++;
}
int CErrorDialog::GetErrorCount() const
{
	return count_;
}
BOOL CErrorDialog::OnInitDialog()
{
	CResizableDialog::OnInitDialog();

	CString strTitle;
	strTitle.Format(L"%s | %s",
		I18N(L"Search Error"), AfxGetAppName());
	SetWindowText(strTitle);

	AddAnchor(IDC_EDIT_ERROR, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDOK, BOTTOM_RIGHT);

	return TRUE;  // return TRUE unless you set the focus to a control
}
