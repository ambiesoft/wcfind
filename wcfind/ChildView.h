
#if !defined(AFX_CHILDVIEW_H__53D01192_6220_431D_B745_C2F425F5C308__INCLUDED_)
#define AFX_CHILDVIEW_H__53D01192_6220_431D_B745_C2F425F5C308__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "ListCtrl2.h"
#include "ThreadResults.h"



#ifndef __IContextMenuCB_INTERFACE_DEFINED__
#define __IContextMenuCB_INTERFACE_DEFINED__

/* interface IContextMenuCB */
/* [local][unique][uuid][object] */ 


EXTERN_C const IID IID_IContextMenuCB;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("3409E930-5A39-11d1-83FA-00A0C90DC849")
    IContextMenuCB : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE CallBack( 
            /* [unique][in] */ 
              IShellFolder *psf,
            /* [in] */ 
              HWND hwndOwner,
            /* [unique][in] */ 
              IDataObject *pdtobj,
            /* [in] */ 
              UINT uMsg,
            /* [in] */ 
              WPARAM wParam,
            /* [in] */ 
              LPARAM lParam) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IContextMenuCBVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IContextMenuCB * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IContextMenuCB * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IContextMenuCB * This);
        
        HRESULT ( STDMETHODCALLTYPE *CallBack )( 
            IContextMenuCB * This,
            /* [unique][in] */ 
            __in_opt  IShellFolder *psf,
            /* [in] */ 
            __in  HWND hwndOwner,
            /* [unique][in] */ 
            __in_opt  IDataObject *pdtobj,
            /* [in] */ 
            __in  UINT uMsg,
            /* [in] */ 
            __in  WPARAM wParam,
            /* [in] */ 
            __in  LPARAM lParam);
        
        END_INTERFACE
    } IContextMenuCBVtbl;

    interface IContextMenuCB
    {
        CONST_VTBL struct IContextMenuCBVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IContextMenuCB_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IContextMenuCB_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IContextMenuCB_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IContextMenuCB_CallBack(This,psf,hwndOwner,pdtobj,uMsg,wParam,lParam)	\
    ( (This)->lpVtbl -> CallBack(This,psf,hwndOwner,pdtobj,uMsg,wParam,lParam) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IContextMenuCB_INTERFACE_DEFINED__ */



























































class CContextMenuCB : public IContextMenuCB
{
	virtual HRESULT STDMETHODCALLTYPE QueryInterface(
		REFIID riid,
		void **ppvObject)
	{
		if(IsEqualIID(IID_IContextMenuCB, riid))
		{
			*ppvObject = this;
			return S_OK;
		}
		return E_NOINTERFACE;
	}
	virtual ULONG STDMETHODCALLTYPE AddRef( void){return 1;}
	virtual ULONG STDMETHODCALLTYPE Release( void){return 1;}

    virtual HRESULT STDMETHODCALLTYPE CallBack( 
        /* [unique][in] */ 
          IShellFolder *psf,
        /* [in] */ 
          HWND hwndOwner,
        /* [unique][in] */ 
          IDataObject *pdtobj,
        /* [in] */ 
          UINT uMsg,
        /* [in] */ 
          WPARAM wParam,
        /* [in] */ 
          LPARAM lParam);
};


class COptionDialog;
class CErrorDialog;

class CLVData;


class CChildView : public CWnd
{
	using ParentClass = CWnd;

	bool columnClicking_ = false;
	CFont* m_pFontList = nullptr;

	CThreadResults* GetSearchResult() {
		return CThreadResults::GetInstance();
	}
	void ResetThreadResult(int workid);
	void SortLVData(LVCOMPAREINFO& cmpinfo);
public:
	BOOL OnIdle(LONG lCount);
	const CLVData& GetLVData(int index)
	{
		ASSERT(GetSearchResult());
		return GetSearchResult()->get(index);
	}
	void SetLVData(int index, CLVData* pData)
	{
		GetSearchResult()->set(index, pData);
		m_ctrlFileList.SetItemCountEx(GetSearchResult()->GetCount(), LVSICF_NOSCROLL);
	}
	void RemoveLVData(int index)
	{
		GetSearchResult()->remove(index);
		m_ctrlFileList.SetItemCountEx(GetSearchResult()->GetCount(), LVSICF_NOSCROLL);
	}
	int GetLVCount() {
		return GetSearchResult() ? GetSearchResult()->GetCount() : 0;
	}
	CChildView();

	// void MakeAttributeString (DWORD dwAttributes, TCHAR * pszString);
	bool DoSearch(
		const CStringArray& saPaths);



	bool DoSearchAgainByFilename(const FileNameArray& saNeedleFileNames);
	void OnComboEnter();

	HIMAGELIST GetSystemImageList (BOOL bLarge = FALSE);
	CListCtrl2 m_ctrlFileList;
	HANDLE m_hCurThread = NULL;
	HANDLE m_hRun = NULL;
	IContextMenuPtr m_pcm;
	IContextMenu2Ptr m_pcm2;
	IContextMenu3Ptr m_pcm3;

	CContextMenuCB m_cccb;

	enum SEARCHSTATE {
		SEARCHSTATE_NONE = 0,
		SEARCHSTATE_SEARCHING,
		SEARCHSTATE_SUSPENDED,
		SEARCHSTATE_FINISHED,
	};
	SEARCHSTATE GetSearchState() const;

	struct CThreadState {
		bool bActive_;
		bool bSuspended_;
	};
	void GetThreadState(CThreadState* pState) const;
protected:
	COptionDialog* m_pOption;
	CErrorDialog* m_pErrorDialog;

	LVCOMPAREINFO m_lvci;
	// static int CALLBACK compareItems(LPARAM l1, LPARAM l2, LPARAM pData);
	void SortIfNeeded(bool force=false);
	int GetHeaderPos(LPCTSTR pHeaderName);
	void LaunchSelected();
	void OnListViewItemChangedCommon();
public:
	// ClassWizard
	//{{AFX_VIRTUAL(CChildView)
	public:
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

public:
	virtual ~CChildView();


public:
	//{{AFX_MSG(CChildView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnUpdateAppAbout(CCmdUI* pCmdUI);
	afx_msg void OnPause();
	afx_msg void OnUpdatePause(CCmdUI* pCmdUI);
	afx_msg void OnRestart();
	afx_msg void OnUpdateRestart(CCmdUI* pCmdUI);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu);
	afx_msg void OnReplay();
	afx_msg void OnUpdateReplay(CCmdUI* pCmdUI);
	afx_msg void OnReset();
	afx_msg void OnUpdateReset(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	afx_msg void OnEditOption();
	afx_msg void OnUpdateEditLabel(CCmdUI* pCmdUI);
	afx_msg void OnEditLabel();
	afx_msg void OnUpdateSelectAll(CCmdUI* pCmdUI);
	afx_msg void OnSelectAll();
	//}}AFX_MSG
	afx_msg void OnGetDispInfoFileList (NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkFileList (NMHDR* pNMHDR, LRESULT* pResult);
	// afx_msg void OnRClickFileList(NMHDR* pNMHDR, LRESULT* pResult);
	// afx_msg void OnDeleteItem(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnColumnClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBeginEdit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEndEdit(NMHDR* pNMHDR, LRESULT* pResult);
	// afx_msg LRESULT OnSearchHit(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSearchError(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSearchProgress(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSearchFinished(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnAfterEndEdit(WPARAM wParam, LPARAM lParam);
	afx_msg void OnUpdateSelectedNum(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNum(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSearchState(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSearchError(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShellContextMenu(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSOP(CCmdUI* pCmdUI);
	afx_msg void OnBegindragFilelist(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLVNKeydown(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnListViewItemChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnListViewItemChangedRange(NMHDR* pNMHDR, LRESULT* pResult);
	DECLARE_MESSAGE_MAP()

	friend class CListCtrl2;
	
	afx_msg void OnViewErrordialog();
	afx_msg void OnUpdateViewErrordialog(CCmdUI *pCmdUI);
	afx_msg void OnSearchSearchagainbysettingclipboardtextsfilename();
	afx_msg void OnUpdateSearchSearchagainbysettingclipboardtextsfilename(CCmdUI *pCmdUI);
	afx_msg void OnFindfileGo();
	
	void CutOrCopyFiles(bool bCopy);
public:
	void OnEditCopy();
	void OnUpdateEditCopy(CCmdUI* pCmdUI);

	void OnEditCut();
	void OnUpdateEditCut(CCmdUI *pCmdUI);

	void OnEditCopyAstest();
	void OnUpdateEditCopyAstest(CCmdUI* pCmdUI);

	void ShowErrorDialog();
	afx_msg void OnEditFont();

	enum class FONTSIZE {
		SMALLER,
		LARGER,
	};
	void ChangeFontSize(FONTSIZE fontSize);
	virtual void PostNcDestroy();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++

#endif // !defined(AFX_CHILDVIEW_H__53D01192_6220_431D_B745_C2F425F5C308__INCLUDED_)
