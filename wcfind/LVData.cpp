// LVData.cpp: CLVData クラスのインプリメンテーション
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "wcfind.h"
#include "helper.h"
#include "LVData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

std::map<std::wstring, std::wstring> CLVData::cacheFiletype_;

CLVData::CLVData(CString strParent, const WIN32_FIND_DATA& wfd, DWORD dwError)
{
	strParent_ = strParent;
	strFullPath_ = strParent + wfd.cFileName;
	wfd_=wfd;
	dwError_ = dwError;
	iIcon_ = -1;
}

CLVData::~CLVData()
{

}

int CLVData::GetiIcon() const {
		
	if (iIcon_ == -1)
	{
		SHFILEINFO sfi = {0};
		if(0==SHGetFileInfo(strFullPath_,
			NULL, 
			&sfi,
			sizeof (sfi), 
			SHGFI_SYSICONINDEX))
		{
			DWORD dwLE = GetLastError();
			if(dwLE != 0)
			{
				wstring error = GetLastErrorString(dwLE);
				ASSERT(FALSE);
			}
		}

		iIcon_ = sfi.iIcon;
		if (iIcon_ == -1)
			iIcon_ = -2;
	}

	return iIcon_;
}
LPCTSTR CLVData::GetTypeText() const {

	if (strTypeName_.IsEmpty())
	{
		if ((wfd_.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
		{
			ASSERT((wfd_.dwFileAttributes & FILE_ATTRIBUTE_NORMAL) == 0);
			strTypeName_ = L"Folder";
		}
		else
		{
			// File
			wstring ext = stdGetFileExtension((LPCWSTR)strFullPath_);
			if (ext.empty())
			{
				strTypeName_ = stdGetFileName((LPCWSTR)strFullPath_);
			}
			else
			{
				wstring ftCached = GetCacheFileType(ext);
				if (!ftCached.empty())
				{
					strTypeName_ = ftCached.c_str();
				}
				else
				{
					SHFILEINFO sfi = { 0 };
					if (0 == SHGetFileInfo(strFullPath_,
						NULL,
						&sfi,
						sizeof(sfi),
						SHGFI_TYPENAME))
					{
						DWORD dwLE = GetLastError();
						if (dwLE != 0)
						{
							wstring error = GetLastErrorString(dwLE);
							ASSERT(FALSE);
						}
					}
					CacheFileType(ext, sfi.szTypeName);
					strTypeName_ = sfi.szTypeName;
					if (strTypeName_.IsEmpty())
						strTypeName_ = L"<Unknown>";
				}
			}
		}
	}

	return strTypeName_;
}
LPCTSTR CLVData::GetTimeText(LPCTSTR pFormat) const
{
	if (!strTimeText_.IsEmpty())
	{
		return strTimeText_;
	}

	static const TCHAR* spDefaultFormat = _T("%x %X");
	if (!pFormat)
		pFormat = spDefaultFormat;

	//CTime Time(wfd_.ftLastWriteTime);
	//strTimeText_ = Time.Format(pFormat);
	strTimeText_ = safe_formatTime(pFormat, &wfd_.ftLastWriteTime);
	return strTimeText_;
}
