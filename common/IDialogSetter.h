#pragma once

class IDialogSetter
{
public:
	virtual void SetDirectory(const std::wstring& dir) = 0;
	virtual void SetFileName(bool bExact, bool bReg, bool bFull, const std::wstring& name) = 0;
	virtual void SetFileContent(const std::wstring& cont) = 0;
	virtual void SetMTime(int mTime) = 0;
	virtual void SetType(DWORD type) = 0;
	virtual void SetSize(const std::wstring& size) = 0;
};
