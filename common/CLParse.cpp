#include "stdafx.h"

#include "CLParse.h"
#include "../../lsMisc/tstring.h"

#include "../../lsMisc/UrlEncode.h"
#include "../../lsMisc/UTF16toUTF8.h"
#include "../../lsMisc/IsFilenamable.h"
#include "../../lsMisc/CommandLineString.h"
#include "ColumnHeader.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

enum PARSE_BLOCK{
	PARSE_BLOCK_FOLDER,
	PARSE_BLOCK_EXPRESSION,
};



CString CCLParse::getUsage(bool bStartDialog) 
{
	CString message;
	message += CString() + I18N(L"Usage:") + L"\r\n";
	message += L"wcfind [GlobalOption] FOLDER1 [FOLDER2 ...] [SearchOption]\r\n";
	message += L"\r\n";

	message += L"  [GlobalOption]\r\n";
	if (bStartDialog)
	{
		message += L"    -pastefilename\r\n";
		message += CString() + L"       " + I18N(L"paste clipboard text into filename search input") + "\r\n";
	}
	message += L"    -sortcolumn SORTCOLUMN\r\n";
	message += CString() + L"       " +  
		stdFormat(I18N(L"sort by SORTCOLUMN, one of %s."), (LPCWSTR)getSortColumnNamesAsHelpString()).c_str() + L"\r\n";
	message += L"    -sortmode SORTMODE\r\n";
	message += CString() + L"       " + I18N(L"SORTMODE is one of ASC or DESC") + L"\r\n";
	message += L"    -log [file]\r\n";
	message += CString() + L"       " + I18N(L"log trace to file") + "\r\n";

	message += L"\r\n";


	message += L"  [SearchOption]\r\n";
	message += L"    [-name WORD1 [-name WORD2 ...]]\r\n";
	message += L"    [-nameu UTF8URLENCODEDWORD1 [-nameu UTF8URLENCODEDWORD2 ...]]\r\n";
	message += L"    [-rname WORD1 [-rname WORD2 ...]]\r\n";
	message += L"    [-rnameu UTF8URLENCODEDWORD1 [-rnameu UTF8URLENCODEDWORD2 ...]]\r\n";
	message += L"    [-nameclipboard]\r\n";

	message += L"    [-fullname WORD1 [-fullname WORD2 ...]]\r\n";
	message += L"    [-fullnameu UTF8URLENCODEDWORD1 [-fullnameu UTF8URLENCODEDWORD2 ...]]\r\n";
	message += L"    [-rfullname WORD1 [-rfullname WORD2 ...]]\r\n";
	message += L"    [-rfullnameu UTF8URLENCODEDWORD1 [-rfullnameu UTF8URLENCODEDWORD2 ...]]\r\n";

	message += L"    [-mtime PASTDAYS]\r\n";
	message += L"    [-content WORD1 [-content WORD2]]\r\n";
	message += L"    [-contentu UTF8URLENCODEDWORD1 [-contentu UTF8URLENCODEDWORD2]]\r\n";
	message += L"    [-size [+or-]FILESIZE]\r\n";

	message += L"\r\n";

	message += CString() + L"      " + I18N(L"FILESIZE: Files of equal size to FILESIZE") + L"\r\n";
	message += CString() + L"      " + I18N(L"+FILESIZE: Files which size is bigger or equal than FIELSIZE") + L"\r\n";
	message += CString() + L"      " + I18N(L"-FILESIZE: Files which size is less than FIELSIZE") + L"\r\n";

	return message;
}

CCLParse::CCLParse() : opParser_(nullptr, true, true)
{
	//m_bMtime = false;
	//m_nMtime=0;

	// m_bType = false;
	//m_dwType = INVALIDE_FILE_TYPE;
	m_nPasteFilename = 0;

	//m_bSize = false;
	//m_nSizeSign = 0;
	//m_lSize = 0;

	m_sortMode = SORTMODE_NONE;
}

//BOOL CCLParse::parseFolder(int iStart, int argc, TCHAR** argv)
//{
//
//	bool bTreatNextAsArg = false;
//	for (int i = iStart; i < argc; ++i)
//	{
//		CString strArg = argv[i];
//		if (strArg == L"--")
//		{
//			if (bTreatNextAsArg)
//			{
//				// "-" after "-", treat it argument
//
//			}
//		}
//	}
//	return TRUE;
//}

BOOL CCLParse::parse(int argc, TCHAR** argv, PARSEERROR& error, CString& strAddtionalErrorInfo, IDialogSetter* pDialogSetter)
{
	int i = 1;

	if (!parseGlobalOption(i, argc, argv, error, strAddtionalErrorInfo))
		return FALSE;

	if (!parseFolder(i, argc, argv, error, strAddtionalErrorInfo, pDialogSetter))
		return FALSE;

	if (!parseSearchOption(i, argc, argv, error, strAddtionalErrorInfo, pDialogSetter))
		return FALSE;

	return TRUE;
}



BOOL hasNextArg(int i, int argc)
{
	return i < argc;
}

enum GlobalOptionType {
	GlobalOption_Null,
	GlobalOption_Log,
	GlobalOption_Pastefilename,
	GlobalOption_NoPastefilename,
	GlobalOption_SortColumn,
	GlobalOption_SortMode,
	GlobalOption_Help,
	GlobalOption_Version,
};
static map<CString, GlobalOptionType> globalOptions{
	{ L"-log", GlobalOption_Log },
	{ L"-pastefilename", GlobalOption_Pastefilename },
	{ L"-nopastefilename", GlobalOption_NoPastefilename },
	{ L"-sortcolumn", GlobalOption_SortColumn },
	{ L"-sortmode", GlobalOption_SortMode },
	{ L"-h", GlobalOption_Help },
	{ L"-help", GlobalOption_Help },
	{ L"--help", GlobalOption_Help },
	{ L"/?", GlobalOption_Help },
	{ L"-v", GlobalOption_Version },
	{ L"-version", GlobalOption_Version },
	{ L"--version", GlobalOption_Version },
};
GlobalOptionType GetGlobalOptionType(const CString& strArg)
{
	if (globalOptions.find(strArg) == globalOptions.end())
		return GlobalOption_Null;
	return globalOptions[strArg];
}
BOOL isParseGlobalOption(CString arg)
{
	if (arg.IsEmpty())
		return FALSE;

	if (arg[0] != L'-')
		return FALSE;

	return globalOptions.find(arg) != globalOptions.end();
}

bool isParseFolder(const CString& s)
{
	if (s.IsEmpty())
		return false;

	if (s[0] == L'-')
		return false;

	// begin paren is a start of search
	if (s == L"(")
		return false;

	return true;
}
BOOL CCLParse::parseFolder(
	int& i,
	int argc, 
	TCHAR** argv,
	PARSEERROR& error, 
	CString& strAddtionalErrorInfo,
	IDialogSetter* pDialogSetter)
{
	for (; i < argc; ++i)
	{
		CString strArg = getArg(i, argv);
		if (strArg.IsEmpty())
			return FALSE;
		if (!isParseFolder(strArg))
			return TRUE;

		strArg.Replace(L'/', L'\\');
		strArg.TrimRight(L'\\');

		if (pDialogSetter)
			pDialogSetter->SetDirectory((LPCWSTR)strArg);

		saFolders_.Add(strArg);
	}
	
	if (saFolders_.GetSize() == 0)
	{
		error = PARSEERROR_NOFOLDER_SPECIFIED;
		return FALSE;
	}
	return TRUE;
}
BOOL CCLParse::parseGlobalOption(int& i, int argc, TCHAR** argv, PARSEERROR& error, CString& strAddtionalErrorInfo)
{
	for (; i < argc; ++i)
	{
		CString strArg = getArg(i, argv);
		if (strArg.IsEmpty())
			return FALSE;
		if (!isParseGlobalOption(strArg))
			return TRUE;

		switch (GetGlobalOptionType(strArg))
		{
		case GlobalOption_Log: // if (strArg == _T("-log"))
		{
			if (!strLogFileName_.IsEmpty())
			{
				error = PARSEERROR_MULTIPLE_LOG_SPECIFIED;
				return FALSE;
			}
			if (!hasNextArg(i, argc))
			{
				error = PARSEERROR_NO_LOGFILENAME;
				return FALSE;
			}
			++i;
			strArg = getArg(i, argv);
			if (strArg.IsEmpty())
			{
				error = PARSEERROR_NO_LOGFILENAME;
				return FALSE;
			}
			if (!IsRelativePathNamble(strArg) && !IsFullPathNamble(strArg))
			{
				error = PARSEERROR_INVALID_LOGFILENAME;
				return FALSE;
			}
			strLogFileName_ = strArg;
		}
		break;

		case GlobalOption_Pastefilename: // else if (strArg == _T("-pastefilename"))
		{
			if (m_nPasteFilename != -1)
				m_nPasteFilename = 1;
		}
		break;

		case GlobalOption_NoPastefilename: // else if (strArg == _T("-nopastefilename"))
		{
			m_nPasteFilename = -1;
		}
		break;

		case GlobalOption_SortColumn: // else if (strArg == _T("-sortcolumn"))
		{
			if (!m_strSortColumn.IsEmpty())
			{
				error = PARSEERROR_MULTIPLE_SORTCOLUMN_SPECIFIED;
				return FALSE;
			}
			if (!hasNextArg(i, argc))
			{
				error = PARSEERROR_NO_SORTCOLUMNVALUE;
				return FALSE;
			}
			++i;
			strArg = getArg(i, argv);
			if (strArg.IsEmpty())
			{
				error = PARSEERROR_NO_SORTCOLUMNVALUE;
				return FALSE;
			}

			if (strArg == L"LastModified")
				strArg = L"Last Modified";
			if (!IsValidSortColumn(strArg))
			{
				error = PARSEERROR_INVALID_SORTCOLUMN;
				strAddtionalErrorInfo = strArg;
				return FALSE;
			}
			m_strSortColumn = strArg;
		}
		break;

		case GlobalOption_SortMode: // else if (strArg == _T("-sortmode"))
		{
			if (m_sortMode != SORTMODE_NONE)
			{
				error = PARSEERROR_MULTIPLE_SORTMODE_SPECIFIED;
				return FALSE;
			}
			if (!hasNextArg(i, argc))
			{
				error = PARSEERROR_NO_SORTMODEVALUE;
				return FALSE;
			}
			++i;
			strArg = getArg(i, argv);
			if (strArg.IsEmpty())
			{
				error = PARSEERROR_NO_SORTMODEVALUE;
				return FALSE;
			}

			if (lstrcmpi(strArg, L"ASC") == 0)
			{
				m_sortMode = SORTMODE_ASCENDING;
			}
			else if (lstrcmpi(strArg, L"DESC") == 0)
			{
				m_sortMode = SORTMODE_DESCENDING;
			}
			else
			{
				error = PARSEERROR_INVALID_SORTMODE;
				strAddtionalErrorInfo = strArg;
				return FALSE;
			}
		}
		break;

		case GlobalOption_Help: // else if (strArg == L"-h" || strArg == L"-help" || strArg == L"--help" || strArg == L"/?")
		{
			m_bHelp = true;
		}
		break;

		case GlobalOption_Version:
		{
			m_bVersion = true;
		}
		break;

		default:
		{
			// unknown option
			strAddtionalErrorInfo = strArg;
			error = PARSEERROR_UNKNOWNOPTION_GLOBALOPTION;
			return FALSE;
		}
		break;
		}
	}
	return TRUE;
}



CString CCLParse::getResult() const
{
	wstring result;
	int i=0;

	result += L"m_strSortColumn:" + m_strSortColumn;
	result += L"\r\n";

	result += L"m_sortMode:" + stdToString(m_sortMode);
	result += L"\r\n";
	result += L"\r\n";

	if (!strLogFileName_.IsEmpty())
	{
		result += L"logfile: ";
		result += strLogFileName_;
		result += L"\r\n";
		result += L"\r\n";
	}

	result += _T("saFolders_\r\n");
	for(i=0 ; i < saFolders_.GetSize(); ++i)
	{
		result += stdToString(i) + L":";
		result += saFolders_[i];
		result += _T("\r\n");
	}
	result += _T("\r\n");



	result += _T("m_bPasteFilename: ");
	result += stdToString(m_nPasteFilename);
	result += _T("\r\n");
	result += _T("\r\n");

	return result.c_str();
}

void CCLParse::getFoldersAsVector(std::vector<std::wstring>& vOut)
{
	for (int i = 0; i < saFolders_.GetSize(); ++i)
	{
		vOut.push_back((LPCWSTR)saFolders_[i]);
	}
}

CString CCLParse::getTitle()
{
	CString strFolders;
	{
		for (int i = 0; i < saFolders_.GetSize(); ++i)
		{
			CString t = saFolders_[i];
			if (t.IsEmpty())
				break;
			if (t[0] == L'-')
				break;
			strFolders += t;
			strFolders += L" ";
		}
	}

	CString strFindee = toStdWstringFromUtf8(opParser_.ToString()).c_str();

	// finally, concat findee and folder
	CString ret;
	if (!strFindee.IsEmpty())
	{
		ret += strFindee;
		ret += L" - ";
	}
	ret += strFolders;

	return CString(Ambiesoft::stdosd::stdTrim(wstring((LPCWSTR)ret)).c_str());

}
// static
CString CCLParse::getCommnadLineForTitle_obsolete()
{
	try
	{
		//CString ret;
		//CCommandLineString cls(::GetCommandLine());

		//for (size_t i = 1; i < cls.getCount(); ++i)
		//{
		//	CString t = cls.getArg(i).c_str();
		//	if (t == L"-nameu")
		//	{
		//		ret += t;
		//		ret += L" ";

		//		++i;
		//		t = cls.getArg(i).c_str();

		//		ret += UrlDecodeStd<wstring>((LPCWSTR)t).c_str();
		//		ret += L" ";
		//		continue;
		//	}
		//	ret += t;
		//	ret += L" ";
		//}

		//return CString(Ambiesoft::stdosd::stdTrim(wstring((LPCWSTR)ret)).c_str());

		CString strFolders;
		// Fisrt find folders
		{
			CCommandLineString cls(::GetCommandLine());
			for (size_t i = 1; i < cls.getCount(); ++i)
			{
				CString t = cls.getArg(i).c_str();
				if (t.IsEmpty())
					break;
				if (t[0] == L'-')
					break;
				strFolders += t;
				strFolders += L" ";
			}
		}

		CString strFileFindee;
		CString strContentFindee;
		// second, find findee word
		{
			CCommandLineString cls(::GetCommandLine());

			for (size_t i = 1; i < cls.getCount(); ++i)
			{
				CString t = cls.getArg(i).c_str();
				if (t == L"-nameu" || t == L"-rnameu")
				{
					++i;
					t = cls.getArg(i).c_str();
					strFileFindee += UrlDecodeStd<wstring>((LPCWSTR)t).c_str();
					strFileFindee += L" ";
				}
				else if (t == L"-name" || t == L"-rname")
				{
					++i;
					t = cls.getArg(i).c_str();
					strFileFindee += t;
					strFileFindee += L" ";
				}
				else if (t == L"-content")
				{
					++i;
					t = cls.getArg(i).c_str();
					strContentFindee += t;
					strContentFindee += L" ";
				}
				else if (t == L"-contentu")
				{
					++i;
					t = cls.getArg(i).c_str();
					strContentFindee += UrlDecodeStd<wstring>((LPCWSTR)t).c_str();
					strContentFindee += L" ";
				}
			}
		}

		// finally, concat findee and folder
		CString ret;
		if (!strFileFindee.IsEmpty())
		{
			ret += strFileFindee;
			ret += L" - ";
		}
		if (!strContentFindee.IsEmpty())
		{
			ret += strContentFindee;
			ret += L" - ";
		}
		ret += strFolders;
		
		return CString(Ambiesoft::stdosd::stdTrim(wstring((LPCWSTR)ret)).c_str());
	}
	catch (...){}
	return CString();
}