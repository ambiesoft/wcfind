#pragma once


//#include <afxwin.h> 
//#include <afxext.h> 
#include <afxcoll.h>
//#include <afxdtctl.h>

#include "../../lsMisc/stdosd/OpParser.h"
#include "../../lsMisc/stdosd/CBool.h"
#include "ColumnHeader.h"
// #include "thread.h"
#include "WcfindWord.h"
#include "IDialogSetter.h"
//                                                Operand     Context      Findee                  Findee full path     lastError  Cancelled                               
#define WCOpParserType Ambiesoft::Logic::OpParser<WcfindWord, void*,       const WIN32_FIND_DATA&, const std::wstring&, DWORD&,    bool&      >

class CWcfindFolder
{
	CString m_strPath;
	CBool m_bFollowJunction;
};

class CWcToken
{
	int m_nType;
	CString m_strToken;
};

class COutStack
{

};

class FilenameOption
{
	BOOL m_bReg;
	CString m_strFilename;

public:
	FilenameOption(const CString& s, BOOL bReg) : 
		m_strFilename(s), m_bReg(bReg){}

	FilenameOption(const FilenameOption& f) {
		this->m_bReg = f.m_bReg;
		this->m_strFilename = f.m_strFilename;
	}

	CString GetFilename() const {
		return m_strFilename;
	}
	BOOL IsRegex() const{
		return m_bReg;
	}
};

typedef std::vector<FilenameOption> FileNameArray;

#define INVALIDE_FILE_TYPE 0xFFFFFFFF
class CCLParse
{
	enum PARSE_PHASE {
		PARSE_PHASE_START = 0,
		PARSE_PHASE_FILENAME,
		PARSE_PHASE_FILENAME_U,
		PARSE_PHASE_RFILENAME,
		PARSE_PHASE_RFILENAME_U,
		PARSE_PHASE_EXACTFILENAME,
		PARSE_PHASE_EXACTFILENAME_U,
		PARSE_PHASE_REXACTFILENAME,
		PARSE_PHASE_REXACTFILENAME_U,
		PARSE_PHASE_FULLNAME,
		PARSE_PHASE_FULLNAME_U,
		PARSE_PHASE_RFULLNAME,
		PARSE_PHASE_RFULLNAME_U,

		PARSE_PHASE_FILECONTENT,
		PARSE_PHASE_FILECONTENT_U,

		PARSE_PHASE_BOM,
		
		PARSE_PHASE_MTIME,
		PARSE_PHASE_TYPE,
		PARSE_PHASE_SIZE,


		PARSE_PHASE_OPERATOR,
	};

	CString getArg(int i, TCHAR** argv)
	{
		CString strArg = argv[i];
		strArg.TrimLeft();
		strArg.TrimRight();
		return strArg;
	}

	bool m_bHelp = false;
	bool m_bVersion = false;

public:
	WCOpParserType opParser_;
	CString strLogFileName_;
	CStringArray saFolders_;

	int m_nPasteFilename = 0;

	CString m_strSortColumn;
	SORTMODE m_sortMode;

	bool IsHelp() const {
		return m_bHelp;
	}
	bool IsVersion() const {
		return m_bVersion;
	}

public:
	CCLParse();

	enum PARSEERROR {
		PARSEERROR_NONE=0,
		PARSEERROR_UNKNOWNOPTION_GLOBALOPTION,
		PARSEERROR_UNKNOWNOPTION_FOLDEROPTION,
		PARSEERROR_UNKNOWNOPTION_SEARCHOPTION,
		PARSEERROR_INVALID_REGEX,
		PARSEERROR_NOFOLDER_SPECIFIED,
		PARSEERROR_MTIME_ARGNOTNUMBER,
		PARSEERROR_MTIME_MULTIPLEARGS,
		PARSEERROR_TYPE_MULTIPLEARGS,
		PARSEERROR_TYPE_ARG_INVALID,
		PARSEERROR_SIZE_ARG_INVALID,
		PARSEERROR_NO_LOGFILENAME,
		PARSEERROR_INVALID_LOGFILENAME,
		PARSEERROR_MULTIPLE_LOG_SPECIFIED,
		PARSEERROR_MULTIPLE_SORTCOLUMN_SPECIFIED,
		PARSEERROR_NO_SORTCOLUMNVALUE,
		PARSEERROR_INVALID_SORTCOLUMN,

		PARSEERROR_MULTIPLE_SORTMODE_SPECIFIED,
		PARSEERROR_NO_SORTMODEVALUE,
		PARSEERROR_INVALID_SORTMODE,

		PARSEERROR_INVALID_BOM,

		PARSEERROR_INVALID_SYNTAX,
		PARSEERROR_CLIPBOARD_READ_FAILED,
	};
	static CString getErrorString(PARSEERROR error) {
		switch(error)
		{
		case PARSEERROR_NONE:				return I18N(L"No Error");
		case PARSEERROR_UNKNOWNOPTION_GLOBALOPTION: return I18N(L"Unknown global option");
		case PARSEERROR_UNKNOWNOPTION_FOLDEROPTION: return I18N(L"Unknown folder option");
		case PARSEERROR_UNKNOWNOPTION_SEARCHOPTION: return I18N(L"Unknown search option");
		case PARSEERROR_INVALID_REGEX:				return I18N(L"Invalid regular expression");
		case PARSEERROR_NOFOLDER_SPECIFIED:	return I18N(L"No folder specified");
		case PARSEERROR_MTIME_ARGNOTNUMBER:	return I18N(L"mtime arg is not a number");
		case PARSEERROR_MTIME_MULTIPLEARGS:	return I18N(L"Multiple mtime option");
		case PARSEERROR_TYPE_MULTIPLEARGS:	return I18N(L"Multiple type option");
		case PARSEERROR_TYPE_ARG_INVALID:	return I18N(L"Invalid type args");
		case PARSEERROR_SIZE_ARG_INVALID:	return I18N(L"Invalid size args");
		case PARSEERROR_NO_LOGFILENAME:		return I18N(L"No file for log specified");
		case PARSEERROR_INVALID_LOGFILENAME:return I18N(L"Invalid file name for log");
		case PARSEERROR_MULTIPLE_LOG_SPECIFIED:return I18N(L"Multiple files for log specified");
		case PARSEERROR_MULTIPLE_SORTCOLUMN_SPECIFIED: return I18N(L"Multiple sortcolumn specified");
		case PARSEERROR_NO_SORTCOLUMNVALUE: return I18N(L"No sortcolumn specified");
		case PARSEERROR_INVALID_SORTCOLUMN: return I18N(L"Invalid sortcolumn specified");

		case PARSEERROR_MULTIPLE_SORTMODE_SPECIFIED: return I18N(L"Multiple sortmode specified");
		case PARSEERROR_NO_SORTMODEVALUE: return I18N(L"No sortmode specified");
		case PARSEERROR_INVALID_SORTMODE: return I18N(L"Invalid sortmode specified");

		case PARSEERROR_INVALID_BOM: return I18N(L"Invalid bom specified");
		case PARSEERROR_INVALID_SYNTAX: return I18N(L"Invalid Sysntax");
		case PARSEERROR_CLIPBOARD_READ_FAILED: return I18N(L"Failed to read from clipbaord");
		default:ASSERT(FALSE);
		}
		return L"";
	}
	static bool needsShowUsageForError(PARSEERROR error) {
		return PARSEERROR_CLIPBOARD_READ_FAILED != error;
	}
	static CString getUsage(bool bStartDialog);


	BOOL parse(int argc, TCHAR** argv, PARSEERROR& error, CString& strAddtionalErrorInfo, IDialogSetter* pDialogSetter);
	BOOL parseGlobalOption(int& i, int argc, TCHAR** argv, PARSEERROR& error, CString& strAddtionalErrorInfo);
	BOOL parseFolder(int& i, int argc, TCHAR** argv, PARSEERROR& error, CString& strAddtionalErrorInfo, IDialogSetter* pDialogSetter);
	BOOL parseSearchOption(int& i, int argc, TCHAR** argv, PARSEERROR& error, CString& strAddtionalErrorInfo, IDialogSetter* pDialogSetter);

	const CStringArray& getFolders() {
		return saFolders_;
	}
	void getFoldersAsVector(std::vector<std::wstring>& vOut);

	//const std::vector<FilenameOption>& getNeedleFileNames() {
	//	return saNeedleFileNames_;
	//}
	//const CStringArray& getNeedleContents() {
	//	return saNeedleFileContents_;
	//}

	bool isPasteFilename() const {
		return m_nPasteFilename == 1;
	}
	bool isNoPasteFilename() const {
		return m_nPasteFilename == -1;
	}

	static bool checkRegex(LPCWSTR pReg)
	{
		try
		{
			boost::wregex re(pReg);
			return true;
		}
		catch (boost::regex_error&)
		{
		}
		return false;
	}

	CString getTitle();

	CString getSortColumn() const {
		return m_strSortColumn;
	}
	SORTMODE getSortMode() const {
		return m_sortMode;
	}
private:
	CString a2l(const std::vector<FilenameOption>& a) const
	{
		CString ret;
		for(size_t i=0 ; i < a.size() ; ++i)
		{
			CString t = a[i].GetFilename();
			if(t.Find(L" ") >= 0)
			{
				t = L"\"" + t + L"\"";
			}
			ret += t;
			ret += L" ";
		}
		ret.TrimRight();
		return ret;
	}
	CString a2t(const CStringArray& a) const
	{
		CString ret;
		for(int i=0 ; i < a.GetSize() ; ++i)
		{
			CString t = a[i];
			ret += t;
			ret += L"\r\n";
		}

		return ret;
	}
public:
	bool isLogging() const {
		return !strLogFileName_.IsEmpty();
	}
	CString getLogFileName() const {
		return strLogFileName_;
	}

	CString getResult() const;

	static CString getCommnadLineForTitle_obsolete();
};