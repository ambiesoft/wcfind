#pragma once


class CLangInfo
{
private:
	CLangInfo(){}
public:
	enum {
		MYLANG_SYSTEMDEFAULT,
		MYLANG_ENGLISH,
		MYLANG_JAPANESE,

		MYLANG_NUMBEROFLANG,
	};

	int GetCount() const {
		return MYLANG_NUMBEROFLANG;
	}

	std::wstring GetLangAsString(int i) const {
		switch (i)
		{
		case MYLANG_SYSTEMDEFAULT: return I18N(L"System Default");
		case MYLANG_ENGLISH: return I18N(L"English");
		case MYLANG_JAPANESE: return I18N(L"Japanese");
		}
		ASSERT(false);
		return std::wstring();
	}

	std::wstring GetLangAsISO3(int i) const {
		switch (i)
		{
		case MYLANG_SYSTEMDEFAULT: break;
		case MYLANG_ENGLISH: return L"ENU";
		case MYLANG_JAPANESE: return L"JPN";
		default:
			ASSERT(false);
		}
		return std::wstring();
	}
	static CLangInfo& instance() {
		static CLangInfo langInfo;
		return langInfo;
	}
};

inline CLangInfo& GetLangInfo() {
	return CLangInfo::instance();
}