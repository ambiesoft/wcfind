#pragma once

#include <Windows.h>
#include <boost/any.hpp>

//#include "../../lsMisc/OpParser.h"
//#include "../../lsMisc/CBool.h"
//#include "ColumnHeader.h"


class WcfindWord
{
public:
	using SizePairType = std::pair<__int64, int>;
	enum WCFIND_WORD_TYPE
	{
		UNKNOWN,

		FILENAME,
		RFILENAME,

		EXACTFILENAME,
		REXACTFILENAME,
		
		FULLNAME,
		RFULLNAME,

		FILECONTENT,

		BOM,

		MTIME,
		TYPE,
		SIZE,

		//LEFTBRACKET,
		//RIGHTBRACKET,
		//AND,
		//OR,
	};

	WCFIND_WORD_TYPE tt_ = UNKNOWN;
	boost::any ws_;
	time_t currentTime_ = 0;
	HWND hWnd_ = nullptr;

	WcfindWord() {}
	WcfindWord(WCFIND_WORD_TYPE tt, const boost::any& w) :
		tt_(tt), ws_(w), currentTime_(::time(NULL)) {
	}
	void overMe(const WcfindWord& that) {
		tt_ = that.tt_;
		ws_ = that.ws_;
		currentTime_ = that.currentTime_;
		hWnd_ = that.hWnd_;
	}
	WcfindWord(const WcfindWord& that) {
		if (this != &that)
		{
			overMe(that);
		}
	}
	const WcfindWord& operator=(const WcfindWord& that) {
		if (this != &that)
		{
			overMe(that);
		}
		return *this;
	}

	void SetHwnd(HWND h) {
		hWnd_ = h;
	}
	DWORD GetType() const {
		ASSERT(tt_ == TYPE);
		ASSERT(ws_.type() == typeid(DWORD));
		return boost::any_cast<DWORD>(ws_);
	}
	std::wstring GetTypeString() const;
	
	int GetMTime() const {
		ASSERT(tt_ == MTIME);
		ASSERT(ws_.type() == typeid(int));
		return boost::any_cast<int>(ws_);
	}
	std::wstring GetMTimeString() const;

	__int64 GetSize() const {
		ASSERT(tt_ == SIZE);
		ASSERT(ws_.type() == typeid(SizePairType));
		return boost::any_cast<SizePairType>(ws_).first;
	}
	int GetSizeSign() const {
		ASSERT(tt_ == SIZE);
		ASSERT(ws_.type() == typeid(SizePairType));
		return boost::any_cast<SizePairType>(ws_).second;
	}
	std::wstring GetSizeSignString() const;
	std::wstring GetSizeString() const;

	std::wstring GetFileName() const {
		ASSERT(tt_ == FILENAME || tt_ == EXACTFILENAME ||
			tt_ == RFILENAME || tt_ == REXACTFILENAME || tt_==FULLNAME || tt_ == RFULLNAME);
		ASSERT(ws_.type() == typeid(wstring));
		return boost::any_cast<wstring>(ws_);
	}
	std::wstring GetFileNameString() const;

	std::wstring GetFileContent() const {
		ASSERT(tt_ == FILECONTENT);
		ASSERT(ws_.type() == typeid(wstring));
		return boost::any_cast<wstring>(ws_);
	}
	std::wstring GetFileContentString() const;

	std::string ToString() const;
};
