#include "stdafx.h"



#include "CLParse.h"
// #include <stlsoft/smartptr/scoped_handle.hpp>
#include "../../lsMisc/tstring.h"

#include "../../lsMisc/UrlEncode.h"
#include "../../lsMisc/UTF16toUTF8.h"
#include "../../lsMisc/IsAllDigit.h"
#include "../../lsMisc/GetClipboardText.h"
//#include "ColumnHeader.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


bool IsOperator(wstring op)
{
	if (op == L"(")
		return true;

	if (op == L")")
		return true;

	if (op == L"and")
		return true;

	if (op == L"or")
		return true;

	return false;
}
BOOL CCLParse::parseSearchOption(
	int& i,
	int argc,
	TCHAR** argv, 
	PARSEERROR& error,
	CString& strAddtionalErrorInfo,
	IDialogSetter* pDialogSetter)
{
	// vector<shared_ptr<WcfindWord> > tokens;
	try
	{
		PARSE_PHASE phase = PARSE_PHASE_START;
		ASSERT(opParser_.empty());
		for (; i < argc; ++i)
		{
			wstring strArg = getArg(i, argv);
			if (strArg.empty())
				continue;
			if (phase == PARSE_PHASE_START && (strArg[0] == _T('-') || IsOperator(strArg)))
			{
				// option
				if (false)
				{
				}
				else if (strArg == L"(")
				{
					phase = PARSE_PHASE_OPERATOR;
					opParser_.AddBeginningParenthesis();
				}
				else if (strArg == L")")
				{
					phase = PARSE_PHASE_OPERATOR;
					opParser_.AddEndingParenthesis();
				}
				else if (strArg == L"and")
				{
					phase = PARSE_PHASE_OPERATOR;
					opParser_.AddAnd();
				}
				else if (strArg == L"or")
				{
					phase = PARSE_PHASE_OPERATOR;
					opParser_.AddOr();
				}
				else if (strArg == _T("-name"))
				{
					phase = PARSE_PHASE_FILENAME;
					continue;
				}
				else if (strArg == _T("-nameu"))
				{
					phase = PARSE_PHASE_FILENAME_U;
					continue;
				}
				else if (strArg == _T("-rname"))
				{
					phase = PARSE_PHASE_RFILENAME;
					continue;
				}
				else if (strArg == _T("-rnameu"))
				{
					phase = PARSE_PHASE_RFILENAME_U;
					continue;
				}
				else if (strArg == _T("-nameclipboard"))
				{
					if (!GetClipboardText(NULL, strArg))
					{
						error = PARSEERROR_CLIPBOARD_READ_FAILED;
						return FALSE;
					}
					strArg = stdTrim(stdGetFirstLine(strArg, true));
					opParser_.AddPredicator(WcfindWord(WcfindWord::FILENAME, strArg));
					phase = PARSE_PHASE_START;
					continue;
				}
				else if (strArg == _T("-exactname"))
				{
					phase = PARSE_PHASE_EXACTFILENAME;
					continue;
				}
				else if (strArg == _T("-exactnameu"))
				{
					phase = PARSE_PHASE_EXACTFILENAME_U;
					continue;
				}
				else if (strArg == _T("-rexactname"))
				{
					phase = PARSE_PHASE_REXACTFILENAME;
					continue;
				}
				else if (strArg == _T("-rexactnameu"))
				{
					phase = PARSE_PHASE_REXACTFILENAME_U;
					continue;
				}
				else if (strArg == _T("-fullname"))
				{
					phase = PARSE_PHASE_FULLNAME;
					continue;
				}
				else if (strArg == _T("-fullnameu"))
				{
					phase = PARSE_PHASE_FULLNAME_U;
					continue;
				}
				else if (strArg == _T("-rfullname"))
				{
					phase = PARSE_PHASE_RFULLNAME;
					continue;
				}
				else if (strArg == _T("-rfullnameu"))
				{
					phase = PARSE_PHASE_RFULLNAME_U;
					continue;
				}

				else if (strArg == _T("-content"))
				{
					phase = PARSE_PHASE_FILECONTENT;
					continue;
				}
				else if (strArg == _T("-contentu"))
				{
					phase = PARSE_PHASE_FILECONTENT_U;
					continue;
				}
				else if (strArg == _T("-bom"))
				{
					phase = PARSE_PHASE_BOM;
					continue;
				}
				else if (strArg == _T("-mtime"))
				{
					phase = PARSE_PHASE_MTIME;
					continue;
				}
				else if (strArg == _T("-type"))
				{
					phase = PARSE_PHASE_TYPE;
					continue;
				}
				else if (strArg == _T("-size"))
				{
					phase = PARSE_PHASE_SIZE;
					continue;
				}

				else
				{
					// unknown option
					strAddtionalErrorInfo = strArg.c_str();
					error = PARSEERROR_UNKNOWNOPTION_SEARCHOPTION;
					return FALSE;
				}
			}


			


			switch (phase)
			{
			case PARSE_PHASE_FILENAME_U:
			case PARSE_PHASE_RFILENAME_U:
			case PARSE_PHASE_EXACTFILENAME_U:
			case PARSE_PHASE_REXACTFILENAME_U:
			case PARSE_PHASE_FULLNAME_U:
			case PARSE_PHASE_RFULLNAME_U:
			{
				// decode url and fall through
				unique_ptr<char> p8(UTF16toUTF8Ex(strArg.c_str()));
				unique_ptr<char> pE(UrlDecodeEx(p8.get()));
				unique_ptr<wchar_t> pW(UTF8toUTF16Ex(pE.get()));

				strArg = pW.get();
			}
			// through

			case PARSE_PHASE_FILENAME:
			case PARSE_PHASE_RFILENAME:
			case PARSE_PHASE_EXACTFILENAME:
			case PARSE_PHASE_REXACTFILENAME:
			case PARSE_PHASE_FULLNAME:
			case PARSE_PHASE_RFULLNAME:
			{
				WcfindWord::WCFIND_WORD_TYPE wcfType;
				bool bReg = false;
				bool bExact = false;
				bool bFull = false;
				switch (phase)
				{
				case PARSE_PHASE_FILENAME_U:
				case PARSE_PHASE_FILENAME:
					wcfType = WcfindWord::FILENAME;
					break;
				case PARSE_PHASE_RFILENAME:
				case PARSE_PHASE_RFILENAME_U:
					bReg = true;
					wcfType = WcfindWord::RFILENAME;
					break;
				case PARSE_PHASE_EXACTFILENAME:
				case PARSE_PHASE_EXACTFILENAME_U:
					bExact = true;
					wcfType = WcfindWord::EXACTFILENAME;
					break;
				case PARSE_PHASE_REXACTFILENAME:
				case PARSE_PHASE_REXACTFILENAME_U:
					bReg = true;
					bExact = true;
					wcfType = WcfindWord::REXACTFILENAME;
					break;
				case PARSE_PHASE_FULLNAME:
				case PARSE_PHASE_FULLNAME_U:
					bFull = true;
					wcfType = WcfindWord::FULLNAME;
					break;
				case PARSE_PHASE_RFULLNAME:
				case PARSE_PHASE_RFULLNAME_U:
					bReg = true;
					bFull = true;
					wcfType = WcfindWord::RFULLNAME;
					break;
				default:
					ASSERT(false);
				}

				if (bReg)
				{
					if (!checkRegex(strArg.c_str()))
					{
						error = PARSEERROR_INVALID_REGEX;
						return FALSE;
					}
				}

				if (pDialogSetter)
					pDialogSetter->SetFileName(bExact, bReg, bFull, strArg);

				opParser_.AddPredicator(WcfindWord(wcfType, strArg));
			}
			break;



			case PARSE_PHASE_FILECONTENT_U:
			{
				unique_ptr<char> p8(UTF16toUTF8Ex(strArg.c_str()));
				unique_ptr<char> pE(UrlDecodeEx(p8.get()));
				unique_ptr<wchar_t> pW(UTF8toUTF16Ex(pE.get()));

				strArg = pW.get();
			}
			// fall
			case PARSE_PHASE_FILECONTENT:
			{
				if (pDialogSetter)
					pDialogSetter->SetFileContent(strArg);

				opParser_.AddPredicator(WcfindWord(WcfindWord::FILECONTENT, strArg));
			}
			break;


			case PARSE_PHASE_BOM:
			{
				wstring bom;
				if (lstrcmpi(strArg.c_str(), L"utf8") == 0 ||
					lstrcmpi(strArg.c_str(), L"utf-8"))
				{
					bom = L"utf8";
				}
				else
				{
					error = PARSEERROR_INVALID_BOM;
					return FALSE;
				}
				
				opParser_.AddPredicator(WcfindWord(WcfindWord::BOM, bom));
			}
			break;

			case PARSE_PHASE_MTIME:
			{
				tstring t = strArg;
				if (!IsAllDigit(t.c_str()))
				{
					error = PARSEERROR_MTIME_ARGNOTNUMBER;
					return FALSE;
				}
				int nMtime = _wtoi(t.c_str());

				if (pDialogSetter)
					pDialogSetter->SetMTime(nMtime);

				opParser_.AddPredicator(WcfindWord(WcfindWord::MTIME, nMtime));
			}
			break;

			case PARSE_PHASE_TYPE:
			{
				DWORD dwType = 0;
				for (size_t i = 0; i < strArg.size(); ++i)
				{
					switch (strArg[i])
					{
						case _T('d'):
						case _T('D'):
						{
							dwType = FILE_ATTRIBUTE_DIRECTORY;
						}
						break;
						case _T('f'):
						case _T('F'):
						{
							dwType = FILE_ATTRIBUTE_NORMAL;
						}
						break;

						default:
						{
							error = PARSEERROR_TYPE_ARG_INVALID;
							strAddtionalErrorInfo = strArg[i];
							return FALSE;
						}
					}
				}
				if (pDialogSetter)
					pDialogSetter->SetType(dwType);

				opParser_.AddPredicator(WcfindWord(WcfindWord::TYPE, dwType));
			}
			break;

			case PARSE_PHASE_SIZE:
			{
				if (strArg.empty())
				{
					error = PARSEERROR_SIZE_ARG_INVALID;
					return FALSE;
				}

				__int64 lSize = 0;
				int nSizeSign = 0;
				if (!stdGetUnittedSize(strArg.c_str(), &nSizeSign, &lSize))
				{
					error = PARSEERROR_SIZE_ARG_INVALID;
					strAddtionalErrorInfo = strArg[i];
					return FALSE;
				}

				if (lSize < 0)
					lSize = -lSize;

				if (pDialogSetter)
					pDialogSetter->SetSize(strArg);

				opParser_.AddPredicator(WcfindWord(WcfindWord::SIZE, WcfindWord::SizePairType(lSize, nSizeSign)));
			}
			break;

			case PARSE_PHASE_OPERATOR:
			{
				// nothing
			}
			break;

			default:
			{
				ASSERT(FALSE);
				error = PARSEERROR_UNKNOWNOPTION_SEARCHOPTION;
				strAddtionalErrorInfo = strArg.c_str();
				return FALSE;
			}
			break;
			}
			
			phase = PARSE_PHASE_START;
		}

		bool tempB;
		DWORD dwLE;
		WIN32_FIND_DATA tempwfd;
		opParser_.TryEvaluate(NULL, tempwfd, wstring(), dwLE, tempB);
	}
	catch (Ambiesoft::Logic::OpParserError&)
	{
		error = PARSEERROR_INVALID_SYNTAX;
		return FALSE;
	}
	//if(saNeedleFileNames_.size()==0)
	//	saNeedleFileNames_.push_back(FilenameOption( L"",FALSE));

	error = PARSEERROR_NONE;
	return TRUE;
}
