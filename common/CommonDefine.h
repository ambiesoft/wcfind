#pragma once

#define SECTION_OPTION				_T("OPTION")
#define KEY_LOCATION_X				_T("X")
#define KEY_LOCATION_Y				_T("Y")
#define KEY_LOCATION_WIDTH			_T("WIDTH")
#define KEY_LOCATION_HEIGHT			_T("HEIGHT")

#define KEY_LOCATION_STARTDIALOG_X	_T("StartDialogX")
#define KEY_LOCATION_STARTDIALOG_Y	_T("StartDialogY")

#define KEY_COLUMNORDER				_T("ColumnOrder")
#define KEY_TIMEFORMAT				_T("TimeFormat")
#define KEY_PASTEONSTARTUP			_T("PasteOnStartup")
#define KEY_AUTOSORT				_T("AutoSort")
#define KEY_FULLROWSELCT			_T("FullRowSelect")

#define KEY_COLUMNWIDTH				_T("ColumnWidth")

#define KEY_LANG					_T("Language")
#define KEY_FONTLIST				_T("FontList")

#define KEY_SHOWERRORDIALOGWHENERROROCCURS	_T("ShowErrorDialogWhenErrorOccurs")
#define KEY_FOREWINDOWAFTERFINISH	_T("ForewindowAfterFinish")
