#include "stdafx.h"
#include "ColumnHeader.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

HEADERSTARTUPINFO gheaderstart[] = {
	{
		L"Name",
		LVCFMT_LEFT,
		250,
		0
	},
	{
		L"Size",
		LVCFMT_RIGHT,
		80,
		1
	},
	{
		L"Folder",
		LVCFMT_LEFT,
		180,
		1
	},
	{
		L"Type",
		LVCFMT_LEFT,
		250,
		2
	},
	{
		L"Last Modified",
		LVCFMT_LEFT,
		130,
		3
	},
	{
		L"Attributes",
		LVCFMT_RIGHT,
		100,
		4
	},
};

SORTMODEINFO gSortModeInfo[] = {
	{
		L"Ascending",
		SORTMODE_ASCENDING,
		L"ASC",
	},
	{
		L"Descending",
		SORTMODE_DESCENDING,
		L"DESC",
	},
};
int GetHeaderCount()
{
	return sizeof(gheaderstart) / sizeof(gheaderstart[0]);
}
int GetHeaderByteCount()
{
	return sizeof(gheaderstart);
}

bool IsValidSortColumn(LPCTSTR pName)
{
	for each (HEADERSTARTUPINFO& var in gheaderstart)
	{
		if (0 == lstrcmpi(var.m_pName, pName))
			return true;
	}
	return false;
}

CString getSortColumnNamesAsHelpString()
{
	CString ret;
	for (int i=0 ; i < _countof(gheaderstart); ++i)
	{
		ret += CString() + L"'" + gheaderstart[i].m_pName + L"', ";
	}
	ret = stdTrimEnd(wstring((LPCWSTR)ret), L", ").c_str();
	return ret;
}

void loadSortCombo(CComboBox* pCmb)
{
	for each (HEADERSTARTUPINFO & var in gheaderstart)
	{
		pCmb->AddString(I18N(var.m_pName));
	}
}

CString getColumnName(const int i)
{
	ASSERT(0 <= i && i < _countof(gheaderstart));
	return gheaderstart[i].m_pName;
}

int getIndexFromName(LPCWSTR pName)
{
	if (!pName || pName[0] == 0)
		return -1;
	
	for (int i = 0; i < _countof(gheaderstart); ++i)
	{
		if (lstrcmpi(pName, gheaderstart[i].m_pName) == 0)
			return i;
	}
	return -1;
}

void loadSortModeCombo(CComboBox* pCmd)
{
	for (auto&& info : gSortModeInfo)
		pCmd->AddString(I18N(info.m_pName));
}
int getIndexFromSortMode(const SORTMODE sortMode)
{
	return (int)sortMode;
}
SORTMODE getSortModeFromIndex(const int iIndex)
{
	return (SORTMODE)iIndex;
}
CString getSortmodeCommandLine(const SORTMODE sortMode)
{
	for (int i = 0; i < _countof(gSortModeInfo); ++i)
	{
		if (sortMode == gSortModeInfo[i].m_pMode)
			return gSortModeInfo[i].m_pCommandLineName;
	}
	return CString();
}