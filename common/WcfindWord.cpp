#pragma once

#include "StdAfx.h"

#include <string>

#include "../../lsMisc/UTF16toUTF8.h"

#include "WcfindWord.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace std;
using namespace Ambiesoft;

string WcfindWord::ToString() const
{
	wstring result;
	switch (tt_)
	{
	case FILENAME:
	case RFILENAME:
	case EXACTFILENAME:
	case FULLNAME:
	case RFULLNAME:
		result = GetFileNameString();
		break;
	case FILECONTENT:
		result = GetFileContentString();
		break;
	case BOM:
		result = L"<BOM>";
		break;
	case MTIME:
		// result = boost::lexical_cast<wstring>(GetMTime());
		result = GetMTimeString();
		break;
	case TYPE:
		result = GetTypeString();
		break;
	case SIZE:
		// result = boost::lexical_cast<wstring>(GetSize());
		result = GetSizeString();
		break;

	default:
		ASSERT(FALSE);
		break;
	}

	return toStdUtf8String(stdAddDQIfNecessary(result));
}

std::wstring WcfindWord::GetTypeString() const 
{
	switch (GetType())
	{
	case FILE_ATTRIBUTE_NORMAL:	return L"Type:File";
	case FILE_ATTRIBUTE_DIRECTORY: return L"Type:Directory";
	}
	return L"Type:Unknown";
}
std::wstring WcfindWord::GetMTimeString() const
{
	return L"Time:" + to_wstring(GetMTime());
}
std::wstring WcfindWord::GetSizeSignString() const
{
	return GetSizeSign() == 0 ? L"" :
		(GetSizeSign() > 0 ? L"+" : L"-");
}
std::wstring WcfindWord::GetSizeString() const
{
	return L"Size:" + GetSizeSignString() + to_wstring(GetSize());
}
std::wstring WcfindWord::GetFileNameString() const
{
	switch (tt_)
	{
	case FILENAME:
		return L"Name:" + GetFileName();
	case EXACTFILENAME:
		return L"ExactName:" + GetFileName();
	case RFILENAME:
		return L"RegName:" + GetFileName();
	case REXACTFILENAME:
		return L"RegExactName:" + GetFileName();
	case FULLNAME:
		return L"FullName:" + GetFileName();
	case RFULLNAME:
		return L"RegFullName:" + GetFileName();
	}
	ASSERT(FALSE);
	return L"UnknownName";
}
std::wstring WcfindWord::GetFileContentString() const
{
	const int kMax = 10;
	return L"Cont:" + (GetFileContent().size() <= kMax ?
		(GetFileContent()) :
		(GetFileContent().substr(0, kMax) + L"..."));
}