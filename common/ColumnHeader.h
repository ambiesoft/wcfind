#pragma once

struct HEADERSTARTUPINFO {
	LPCWSTR m_pName;
	int m_nFormat;
	int m_nWidth;
	int m_nUnused;
};

enum SORTMODE {
	SORTMODE_NONE = -1,
	SORTMODE_ASCENDING = 0,
	SORTMODE_DESCENDING = 1,
};
struct SORTMODEINFO {
	LPCWSTR m_pName;
	SORTMODE m_pMode;
	LPCWSTR m_pCommandLineName;
};

extern HEADERSTARTUPINFO gheaderstart[];
extern SORTMODEINFO gSortModeInfo[];

int GetHeaderCount();
int GetHeaderByteCount();

bool IsValidSortColumn(LPCTSTR pName);
CString getSortColumnNamesAsHelpString();
void loadSortCombo(CComboBox* pCmb);
CString getColumnName(const int i);
int getIndexFromName(LPCWSTR pName);

void loadSortModeCombo(CComboBox* pCmd);
int getIndexFromSortMode(const SORTMODE sortMode);
SORTMODE getSortModeFromIndex(const int iIndex);
CString getSortmodeCommandLine(const SORTMODE sortMode);