﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace regrename
{
    class Program
    {
        static void showUsage()
        {
        }

        static void showErrorAndExit(string error)
        {
            Console.Error.WriteLine(error);
            System.Environment.Exit(1);
        }

        static void Main(string[] args)
        {
            string file = null;
            string reg = null;
            string toreg = null;

            foreach (string arg in args)
            {
                if (false)
                {
                }
                else if (arg.StartsWith("/file:"))
                {
                    string[] s2 = arg.Split(new char[] { ':' }, 2);
                    file = s2[1];
                }
                else if (arg.StartsWith("/reg:"))
                {
                    string[] s2 = arg.Split(new char[] { ':' }, 2);
                    reg = s2[1];
                }
                else if(arg.StartsWith("/toreg:"))
                {
                    string[] s2 = arg.Split(new char[] { ':' }, 2);
                    toreg = s2[1];
                }
            }

            if(string.IsNullOrEmpty(file))
            {
                showErrorAndExit(Properties.Resources.FILE_NOT_SPECIFIED);
            }

            if(string.IsNullOrEmpty(reg))
            {
                showErrorAndExit(Properties.Resources.REG_NOT_SPECIFIED);
            }

            if(string.IsNullOrEmpty(toreg))
            {
                showErrorAndExit(Properties.Resources.TOREG_NOT_SPECIFIED);
            }

            
            FileInfo fi = new FileInfo(file);
            string filename = fi.Name;


        }
    }
}
