// DetailFilesDialog.cpp : implementation file
//

#include "stdafx.h"
#include "wcfindstart.h"
#include "DetailFilesDialog.h"
#include "afxdialogex.h"


// CDetailFilesDialog dialog

IMPLEMENT_DYNAMIC(CDetailFilesDialog, CDialogEx)

CDetailFilesDialog::CDetailFilesDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDetailFilesDialog::IDD, pParent)
{

}

CDetailFilesDialog::~CDetailFilesDialog()
{
}

void CDetailFilesDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_MAIN, m_lstMain);
}


BEGIN_MESSAGE_MAP(CDetailFilesDialog, CDialogEx)
END_MESSAGE_MAP()


// CDetailFilesDialog message handlers


BOOL CDetailFilesDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	m_lstMain.InsertColumn(0, L"Blob", 0, 30);
	m_lstMain.InsertColumn(1, L"Regex", 0, 30);
	m_lstMain.InsertColumn(2, L"Reverse", 0, 30);

	int iItem = m_lstMain.InsertItem(0, L"");
	m_lstMain.SetCheckbox(0, 0, 1);
	m_lstMain.SetCheckbox(0, 1, 1);
	m_lstMain.SetCheckbox(0, 2, 1);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
