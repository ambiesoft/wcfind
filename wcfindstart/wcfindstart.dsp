# Microsoft Developer Studio Project File - Name="wcfindstart" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** 編集しないでください **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=wcfindstart - Win32 UnicodeDebug
!MESSAGE これは有効なﾒｲｸﾌｧｲﾙではありません。 このﾌﾟﾛｼﾞｪｸﾄをﾋﾞﾙﾄﾞするためには NMAKE を使用してください。
!MESSAGE [ﾒｲｸﾌｧｲﾙのｴｸｽﾎﾟｰﾄ] ｺﾏﾝﾄﾞを使用して実行してください
!MESSAGE 
!MESSAGE NMAKE /f "wcfindstart.mak".
!MESSAGE 
!MESSAGE NMAKE の実行時に構成を指定できます
!MESSAGE ｺﾏﾝﾄﾞ ﾗｲﾝ上でﾏｸﾛの設定を定義します。例:
!MESSAGE 
!MESSAGE NMAKE /f "wcfindstart.mak" CFG="wcfindstart - Win32 UnicodeDebug"
!MESSAGE 
!MESSAGE 選択可能なﾋﾞﾙﾄﾞ ﾓｰﾄﾞ:
!MESSAGE 
!MESSAGE "wcfindstart - Win32 UnicodeDebug" ("Win32 (x86) Application" 用)
!MESSAGE "wcfindstart - Win32 UnicodeRelease" ("Win32 (x86) Application" 用)
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "wcfindstart - Win32 UnicodeDebug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "wcfindstart___Win32_UnicodeDebug"
# PROP BASE Intermediate_Dir "wcfindstart___Win32_UnicodeDebug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "UnicodeDebug"
# PROP Intermediate_Dir "UnicodeDebug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "L:\MSSDK\2003-FEB\include" /I "../../MyUtility/stlsoft/include" /I "." /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "UNICODE" /D "_UNICODE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x411 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x411 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"C:\Linkout\wcfind\wcfindstart.exe" /pdbtype:sept
# ADD LINK32 /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /debug /machine:I386 /out:"C:\Linkout\wcfind\wcfindstartD.exe" /pdbtype:sept /libpath:"L:\MSSDK\2003-FEB\Lib"

!ELSEIF  "$(CFG)" == "wcfindstart - Win32 UnicodeRelease"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "wcfindstart___Win32_UnicodeRelease"
# PROP BASE Intermediate_Dir "wcfindstart___Win32_UnicodeRelease"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "UnicodeRelease"
# PROP Intermediate_Dir "UnicodeRelease"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "L:\MSSDK\2003-FEB\include" /I "../../MyUtility/stlsoft/include" /I "." /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "UNICODE" /D "_UNICODE" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x411 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x411 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386 /out:"C:\Linkout\wcfind\wcfindstart.exe"
# ADD LINK32 /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /machine:I386 /out:"C:\Linkout\wcfind\wcfindstart.exe" /libpath:"L:\MSSDK\2003-FEB\Lib"

!ENDIF 

# Begin Target

# Name "wcfindstart - Win32 UnicodeDebug"
# Name "wcfindstart - Win32 UnicodeRelease"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AboutDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\browseFolder.cpp
# ADD CPP /Yu
# End Source File
# Begin Source File

SOURCE=..\common\CLParse.cpp
# ADD CPP /Yu
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\CreateFolderIniPath.cpp
# ADD CPP /Yu
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\GetClipboardText.cpp
# ADD CPP /Yu
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\IsFileExists.cpp
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\MFCHelper.cpp
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\SetCSIDLtoEnv.cpp
# End Source File
# Begin Source File

SOURCE=.\ShowCommandDialog.cpp
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\sqlite3.c
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\sqliteserialize.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD BASE CPP /Yc"stdafx.h"
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\stdwin32\stdwin32.cpp
# ADD CPP /Yu
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\UrlEncode.cpp
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\UTF16toUTF8.cpp
# End Source File
# Begin Source File

SOURCE=.\wcfindstart.cpp
# End Source File
# Begin Source File

SOURCE=.\wcfindstart.rc
# End Source File
# Begin Source File

SOURCE=.\wcfindstartDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AboutDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\browseFolder.h
# End Source File
# Begin Source File

SOURCE=..\common\CLParse.h
# End Source File
# Begin Source File

SOURCE=..\CommonDefine.h
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\CreateFolderIniPath.h
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\GetClipboardText.h
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\IsFileExists.h
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\MFCHelper.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\SetCSIDLtoEnv.h
# End Source File
# Begin Source File

SOURCE=.\ShowCommandDialog.h
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\sqlite3.h
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\sqliteserialize.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\..\MyUtility\UrlEncode.h
# End Source File
# Begin Source File

SOURCE=.\wcfindstart.h
# End Source File
# Begin Source File

SOURCE=.\wcfindstartDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\wcfindstart.ico
# End Source File
# Begin Source File

SOURCE=.\res\wcfindstart.rc2
# End Source File
# End Group
# End Target
# End Project
