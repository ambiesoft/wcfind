// wcfindstartDlg.h
//

#if !defined(AFX_WCFINDSTARTDLG_H__FDCAB93C_5006_454A_A615_B25B2FC80557__INCLUDED_)
#define AFX_WCFINDSTARTDLG_H__FDCAB93C_5006_454A_A615_B25B2FC80557__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "../common/IDialogSetter.h"
#include "afxwin.h"

class CCLParse;

/////////////////////////////////////////////////////////////////////////////
// CWcfindstartDlg

class CWcfindstartDlg : public CDialog, public IDialogSetter
{
	const static int MAX_SAVE_SIZECOMBO = 128;
	const static int MAX_SAVE_FILECOMBO = 128;
public:
	// Interface
	virtual void SetDirectory(const std::wstring& dir)
	{
		if (!m_strFolders.IsEmpty())
			m_strFolders += L"\r\n";
		
		m_strFolders += dir.c_str();
	}
	virtual void SetFileName(bool bExact, bool bReg, bool bFull, const std::wstring& name)
	{
		m_bExactFileName = !!bExact;
		m_bRegexFilename = !!bReg;
		m_bFullName = !!bFull;

		if (!m_strFileName.IsEmpty())
			m_strFileName += L" ";

		m_strFileName += name.c_str();
	}
	virtual void SetFileContent(const std::wstring& cont)
	{
		if (!m_strFileContents.IsEmpty())
			m_strFileContents += L"\r\n";

		m_strFileContents += cont.c_str();
	}
	virtual void SetMTime(int mTime)
	{
		m_bMtime = TRUE;
		m_nMtime = mTime;
	}
	virtual void SetType(DWORD type)
	{
		m_bType = TRUE;
		if (type == FILE_ATTRIBUTE_NORMAL)
			m_bTypeFile = TRUE;
		else if (type == FILE_ATTRIBUTE_DIRECTORY)
			m_bTypeDirectory = TRUE;
		else
			ASSERT(false);
	}
	virtual void SetSize(const std::wstring& size)
	{
		m_bSize = TRUE;
		m_strSize = size.c_str();
	}
public:
	CWcfindstartDlg(CWnd* pParent = NULL);

	//{{AFX_DATA(CWcfindstartDlg)
	enum { IDD = IDD_WCFINDSTART_DIALOG };
	CButton	m_chkSize;
	CEdit	m_editContents;
	CButton	m_btnInsertEnv;
	CComboBox	m_cmbFileName;
	CButton	m_chkTypeFile;
	CButton	m_chkTypeDirectory;
	CButton	m_chkType;
	CEdit	m_editLastModified;
	CButton	m_checkLastModifed;
	CEdit	m_editFolders;
	BOOL	m_bMtime;
	int		m_nMtime;
	CString	m_strFileName;
	BOOL	m_bSize;
	BOOL	m_bType;
	BOOL	m_bTypeDirectory;
	BOOL	m_bTypeFile;
	CString	m_strSize;
	//}}AFX_DATA

	// ClassWizard
	//{{AFX_VIRTUAL(CWcfindstartDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	//}}AFX_VIRTUAL

protected:
	HICON m_hIcon;

	//{{AFX_MSG(CWcfindstartDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	afx_msg void OnButtonBrowserfolder();
	afx_msg void OnCheckLastmodified();
	afx_msg void OnCheckType();
	afx_msg void OnDestroy();
	afx_msg void OnButtonInsertWcfindenv();
	afx_msg void OnButtonShowcommandline();
	afx_msg void OnCheckSize();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	BOOL getParameters(CString& param, CString& strError);


	BOOL loadCombo(CComboBox& combo, LPCTSTR pSection);
	BOOL saveCombo(CComboBox& combo, LPCTSTR pSection, const int maxCount = -1);
public:
	CCLParse* m_pCLParse;
	std::vector<wstring> vCsEnvs;
	afx_msg void OnBnClickedButtonDetailFilename();
	BOOL m_bRegexFilename;
	BOOL m_bExactFileName;
	CString m_strFolders;
	CString m_strFileContents;
	CComboBox m_cmbSize;
	afx_msg void OnBnClickedButtonMatchwhollword();
	CComboBox m_cmbFilenameOrFullName;
	BOOL m_bFullName = FALSE;
	afx_msg void OnBnClickedCheckExactfilename();
	afx_msg void OnBnClickedCheckFilenameRegex();
	CButton m_chkExactName;
	CButton m_chkRegex;
	CComboBox m_cmbSort;
	CComboBox m_cmbSortMode;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++

#endif // !defined(AFX_WCFINDSTARTDLG_H__FDCAB93C_5006_454A_A615_B25B2FC80557__INCLUDED_)
