﻿#include "stdafx.h"
#include "../../lsMisc/EnableTextTripleClickSelectAll.h"

#include "wcfindstart.h"
#include "ShowCommandDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CShowCommandDialog ダイアログ


CShowCommandDialog::CShowCommandDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CShowCommandDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CShowCommandDialog)
	m_strCommandLine = _T("");
	//}}AFX_DATA_INIT
}


void CShowCommandDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CShowCommandDialog)
	DDX_Text(pDX, IDC_EDIT_COMMANDLINE, m_strCommandLine);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_EDIT_COMMANDLINE, m_editCommandLine);
}


BEGIN_MESSAGE_MAP(CShowCommandDialog, CDialog)
	//{{AFX_MSG_MAP(CShowCommandDialog)
		// メモ - ClassWizard はこの位置にマッピング用のマクロを追加または削除します。
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CShowCommandDialog メッセージ ハンドラ


BOOL CShowCommandDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	EnableTextTripleClickSelectAll(m_editCommandLine);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
