#include "stdafx.h"

#include "../common/CLParse.h"

#include "../../lsMisc/GetClipboardText.h"
#include "../../lsMisc/browseFolder.h"
#include "../../lsMisc/sqliteserialize.h"
#include "../../lsMisc/UrlEncode.h"
#include "../../lsMisc/UTF16toUTF8.h"
#include "../../lsMisc/MFChelper.h"
#include "../../lsMisc/ScopedDialogDisabler.h"
#include "../../lsMisc/EnableTextTripleClickSelectAll.h"
#include "../../lsMisc/CommandLineString.h"

#include "wcfindstart.h"
#include "wcfindstartDlg.h"
#include "AboutDlg.h"
#include "DetailFilesDialog.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CWcfindstartDlg

CWcfindstartDlg::CWcfindstartDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWcfindstartDlg::IDD, pParent)
	, m_bRegexFilename(FALSE)
	, m_bExactFileName(FALSE)
	, m_strFolders(_T(""))
	, m_strFileContents(_T(""))
{
	//{{AFX_DATA_INIT(CWcfindstartDlg)
	m_bMtime = FALSE;
	m_nMtime = 0;
	m_strFileName = _T("");
	m_bSize = FALSE;
	m_bType = FALSE;
	m_bTypeDirectory = FALSE;
	m_bTypeFile = FALSE;
	m_strSize = _T("");
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWcfindstartDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWcfindstartDlg)
	DDX_Control(pDX, IDC_CHECK_SIZE, m_chkSize);
	DDX_Control(pDX, IDC_EDIT_CONTENT, m_editContents);
	DDX_Control(pDX, IDC_BUTTON_INSERT_WCFINDENV, m_btnInsertEnv);
	DDX_Control(pDX, IDC_COMBO_FILENAME, m_cmbFileName);
	DDX_Control(pDX, IDC_CHECK_TYPE_FILE, m_chkTypeFile);
	DDX_Control(pDX, IDC_CHECK_TYPE_DIRECTORY, m_chkTypeDirectory);
	DDX_Control(pDX, IDC_CHECK_TYPE, m_chkType);
	DDX_Control(pDX, IDC_EDIT_LASTMODIFIED, m_editLastModified);
	DDX_Control(pDX, IDC_CHECK_LASTMODIFIED, m_checkLastModifed);
	DDX_Control(pDX, IDC_EDIT_FOLDERS, m_editFolders);
	DDX_Check(pDX, IDC_CHECK_LASTMODIFIED, m_bMtime);
	DDX_Text(pDX, IDC_EDIT_LASTMODIFIED, m_nMtime);
	DDX_CBString(pDX, IDC_COMBO_FILENAME, m_strFileName);
	DDX_Check(pDX, IDC_CHECK_SIZE, m_bSize);
	DDX_Check(pDX, IDC_CHECK_TYPE, m_bType);
	DDX_Check(pDX, IDC_CHECK_TYPE_DIRECTORY, m_bTypeDirectory);
	DDX_Check(pDX, IDC_CHECK_TYPE_FILE, m_bTypeFile);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CHECK_FILENAME_REGEX, m_bRegexFilename);
	DDX_Check(pDX, IDC_CHECK_EXACTFILENAME, m_bExactFileName);
	DDX_Text(pDX, IDC_EDIT_FOLDERS, m_strFolders);
	DDX_Text(pDX, IDC_EDIT_CONTENT, m_strFileContents);
	DDX_Control(pDX, IDC_COMBO_SIZE, m_cmbSize);
	DDX_Text(pDX, IDC_COMBO_SIZE, m_strSize);
	DDX_Control(pDX, IDC_COMBO_FILENAMEORFULLNAME, m_cmbFilenameOrFullName);
	DDX_Control(pDX, IDC_CHECK_EXACTFILENAME, m_chkExactName);
	DDX_Control(pDX, IDC_CHECK_FILENAME_REGEX, m_chkRegex);
	DDX_Control(pDX, IDC_COMBO_SORT, m_cmbSort);
	DDX_Control(pDX, IDC_COMBO_SORTMODE, m_cmbSortMode);
}

BEGIN_MESSAGE_MAP(CWcfindstartDlg, CDialog)
	//{{AFX_MSG_MAP(CWcfindstartDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_BROWSERFOLDER, OnButtonBrowserfolder)
	ON_BN_CLICKED(IDC_CHECK_LASTMODIFIED, OnCheckLastmodified)
	ON_BN_CLICKED(IDC_CHECK_TYPE, OnCheckType)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_INSERT_WCFINDENV, OnButtonInsertWcfindenv)
	ON_BN_CLICKED(IDC_BUTTON_SHOWCOMMANDLINE, OnButtonShowcommandline)
	ON_BN_CLICKED(IDC_CHECK_SIZE, OnCheckSize)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_DETAIL_FILENAME, &CWcfindstartDlg::OnBnClickedButtonDetailFilename)
	ON_BN_CLICKED(IDC_BUTTON_MATCHWHOLLWORD, &CWcfindstartDlg::OnBnClickedButtonMatchwhollword)
	ON_BN_CLICKED(IDC_CHECK_EXACTFILENAME, &CWcfindstartDlg::OnBnClickedCheckExactfilename)
	ON_BN_CLICKED(IDC_CHECK_FILENAME_REGEX, &CWcfindstartDlg::OnBnClickedCheckFilenameRegex)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWcfindstartDlg


BOOL CWcfindstartDlg::OnInitDialog()
{
	if(m_strFileName.IsEmpty())
	{
		if (!m_pCLParse->isNoPasteFilename() &&
			(m_pCLParse->isPasteFilename() || theApp.GetProfileInt(SECTION_OPTION, KEY_PASTEONSTARTUP, FALSE)))
		{
			wstring strClip;
			if(GetClipboardText(*this, strClip))
			{
				m_strFileName = stdTrimStart(stdGetFirstLine(strClip, true), L" \t").c_str();
			}
		}
	}

	CDialog::OnInitDialog();

	i18nChangeChildWindowText(*this);

	m_cmbFilenameOrFullName.AddString(I18N(L"Find from filename"));
	m_cmbFilenameOrFullName.AddString(I18N(L"Find from fullname"));
	m_cmbFilenameOrFullName.SetCurSel(m_bFullName ? 1 : 0);

	VERIFY(loadCombo(m_cmbFileName, L"FileName"));
	VERIFY(loadCombo(m_cmbSize, L"Sizes"));
	if (m_cmbSize.GetCount() == 0)
	{
		// Insert default Items
		m_cmbSize.AddString(L"+10M");
		m_cmbSize.AddString(L"+100M");
		m_cmbSize.AddString(L"+1000M");
		m_cmbSize.AddString(L"+5G");
	}

	loadSortCombo(&m_cmbSort);
	const CString strSortColumn = m_pCLParse->getSortColumn();
	if (!strSortColumn.IsEmpty())
	{
		int iIndex = getIndexFromName(strSortColumn);
		if (iIndex >= 0)
			m_cmbSort.SetCurSel(iIndex);
	}

	loadSortModeCombo(&m_cmbSortMode);
	const SORTMODE sortMode = m_pCLParse->getSortMode();
	if (sortMode != SORTMODE_NONE)
	{
		int iIndex = getIndexFromSortMode(sortMode);
		if (iIndex >= 0)
			m_cmbSortMode.SetCurSel(iIndex);
	}

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);
	

	CString strFolders;
	for(int i=0 ; i < m_pCLParse->saFolders_.GetSize() ; ++i)
	{
		strFolders += m_pCLParse->saFolders_[i];
		strFolders += CRLF;
	}
	m_editFolders.SetWindowText(strFolders);

	OnCheckLastmodified();
	OnCheckType();
	OnCheckSize();

	if (PathFileExists(theApp.m_pszProfileName))
	{
		int left = theApp.GetProfileInt(SECTION_OPTION, KEY_LOCATION_STARTDIALOG_X, 20);
		int top = theApp.GetProfileInt(SECTION_OPTION, KEY_LOCATION_STARTDIALOG_Y, 20);
		//int width = theApp.GetProfileInt(SECTION_OPTION, KEY_LOCATION_WIDTH, 640);
		//int height = theApp.GetProfileInt(SECTION_OPTION, KEY_LOCATION_HEIGHT, 480);

		CRect r;
		GetWindowRect(r);

		SetWindowPos(NULL, left, top, r.Width(), r.Height(), SWP_NOZORDER | SWP_NOSIZE);
	}

	EnableTextTripleClickSelectAll(m_cmbFileName);

	if(strFolders.IsEmpty())
		return TRUE;

	m_cmbFileName.SetFocus();
	return FALSE;
}

void CWcfindstartDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}


void CWcfindstartDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this);

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CWcfindstartDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

CString myUrlEncode(const CString& strIN)
{
	unique_ptr<char> pUTF8(UTF16toUTF8Ex(strIN));
	unique_ptr<char> pOut(UrlEncodeEx(pUTF8.get()));
	unique_ptr<wchar_t> pOutW(UTF8toUTF16Ex(pOut.get()));

	return pOutW.get();
}

BOOL IsNeedEncoding(LPCWSTR pStr)
{
	if (!pStr || pStr[0] == 0)
		return FALSE;

	for (; *pStr; ++pStr)
	{
		if (*pStr == L' ')
			return TRUE;

		if (!iswalnum(*pStr))
			return TRUE;

		if (*pStr > 127)
			return TRUE;
	}
	return FALSE;
}

BOOL CWcfindstartDlg::getParameters(CString& param, CString& strError)
{
	CString ret;

	// sort
	{
		int nSel = m_cmbSort.GetCurSel();
		if (nSel >= 0)
		{
			ret += CString() + L"-sortcolumn " + stdAddDQIfNecessary((LPCWSTR)getColumnName(nSel)).c_str();
			ret += _T(' ');
		}
	}

	// sort mode
	{
		int nSel = m_cmbSortMode.GetCurSel();
		if (nSel >= 0)
		{
			CString strSortModeCmdLine = getSortmodeCommandLine(getSortModeFromIndex(nSel));
			if (strSortModeCmdLine.IsEmpty())
			{
				strError = I18N(L"Illegal Sort Mode");
				return FALSE;
			}

			ret += CString() + L"-sortmode " + stdAddDQIfNecessary((LPCWSTR)strSortModeCmdLine).c_str();
			ret += _T(' ');
		}
	}

	// target folder
	{
		
		CStringArray lines;
		getLinesFromEdit(m_editFolders,lines,false);

		for(int i=0 ; i < lines.GetSize() ; ++i)
		{
			ret += lines[i];
			ret += _T(' ');
		}


		CString strCheck(ret);
		strCheck.Replace(L" ", L"");
		if(strCheck.IsEmpty())
		{
			strError = I18N(L"No Folders specified");
			return FALSE;
		}
		ret.TrimRight(L' ');
	}
	


	// filename
	if (!m_strFileName.IsEmpty())
	{
		if (m_bExactFileName)
		{
			CString option = L"-";
			if (m_bRegexFilename)
				option += L"r";

			option += L"exactname";

			CString filearg;
			if (IsNeedEncoding(m_strFileName))
			{
				filearg = myUrlEncode(m_strFileName);
				option += L"u";
			}
			else
			{
				filearg = dqIfSpace(m_strFileName);
			}

			ret += L" ";
			ret += option;
			ret += L" ";
			ret += filearg;
		}
		else
		{
			CCommandLineString cmd(m_strFileName);
			for (size_t i = 0; i < cmd.getCount(); ++i)
			{
				CString s = cmd.getArg(i).c_str();
				CString isR;
				if (m_bRegexFilename)
				{
					if (!CCLParse::checkRegex(s))
					{
						strError = I18N(L"Invalid Regular Expression:") + s;
						return FALSE;
					}
					isR = L"r";
				}

				CString isU;
				bool bNeedEncoding = IsNeedEncoding(s);
				if (bNeedEncoding)
					isU = L"u";

				CString nameOrfull;
				if (m_cmbFilenameOrFullName.GetCurSel() == 0)
					nameOrfull = L"name";
				else
					nameOrfull = L"fullname";

				ret += L" -" + isR + nameOrfull + isU;
				ret += L" ";
				if (bNeedEncoding)
					ret += myUrlEncode(s);
				else
					ret += dqIfSpace(s);
			}
		}
	}
	// invert
	//if (m_bV)
	//{
	//	ret += _T(" -v");
	//}
	
	// content
	{
		CStringArray lines;
		getLinesFromEdit(m_editContents,lines,true);
		for(int i=0 ; i < lines.GetSize() ; ++i)
		{
			ret += _T(" -contentu ");
			ret += myUrlEncode(lines[i]);
		}
	}

	// type
	if(m_chkType.GetCheck() != 0)
	{
		CString strCheck;
		if(m_chkTypeFile.GetCheck() != 0)
		{
			strCheck += _T("f ");
		}
		if(m_chkTypeDirectory.GetCheck() != 0)
		{
			strCheck += _T("d ");
		}
		strCheck.TrimRight();
		
		if(!strCheck.IsEmpty())
		{
			ret += _T(" -type ");
			ret += dqIfSpace(strCheck);
		}
	}
	
	// mtime
	if(m_bMtime)
	{
		TCHAR szT[16];
		wsprintf(szT, L"%d", m_nMtime);

		ret += _T(" -mtime ");
		ret += szT;
	}

	// size
	if(m_bSize)
	{
		CString strT;
		m_cmbSize.GetWindowText(strT);

		__int64 result=0;
		int sign =0;
		if(!stdGetUnittedSize((LPCWSTR)strT, strT.GetLength(), &sign, &result))
		{
			strError = I18N(L"Illegal size");
			return FALSE;
		}

		ret += _T(" -size ");
		ret += strT;
	}


	ret.Trim(L' ');
	param = ret;
	return TRUE;
}



void CWcfindstartDlg::OnOK() 
{
	{
		ScopedDialogDisabler scopedDisabler(*this, TRUE);

		UpdateData();




		SHELLEXECUTEINFO sei = { 0 };

		sei.cbSize = sizeof(sei);
		sei.fMask = 0; //SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS ;
		sei.hwnd = *this;
		sei.lpVerb = NULL; // open

		sei.lpFile = theApp.GetWcFindExe();

		CString param;
		CString strError;
		if (!getParameters(param, strError))
		{
			AfxMessageBox(strError);
			return;
		}


		sei.lpParameters = param;

		sei.lpDirectory = NULL;
		sei.nShow = SW_SHOW;
		sei.hInstApp = NULL;
		sei.lpIDList = NULL;
		sei.lpClass = NULL;
		sei.hkeyClass = NULL;
		sei.dwHotKey = 0;
		sei.hIcon = NULL;
		sei.hProcess = NULL;

		if (!ShellExecuteEx(&sei))
		{
			return;
		}
	}
	CDialog::OnOK();
}

void CWcfindstartDlg::OnButtonBrowserfolder() 
{
	static TCHAR sszT[1024];
	if(!browseFolder(*this, I18N(L"Select Folder"), sszT))
		return;

	CString folder(sszT);
	folder += CRLF;

	// Append \r\n if it does not exist at the end.
	CString r;
	m_editFolders.GetWindowText(r);
	if (!r.IsEmpty())
	{
		if (r.Right(1) != L'\n')
		{
			// does not end with \n, so append it.
			r += CRLF;
			m_editFolders.SetWindowText(r);
		}
	}

    int nLength = m_editFolders.GetWindowTextLength();
	m_editFolders.SetSel(nLength, nLength);
	m_editFolders.ReplaceSel(folder);
}

void CWcfindstartDlg::OnCheckLastmodified() 
{
	m_editLastModified.EnableWindow(m_checkLastModifed.GetCheck()==1);
}

void CWcfindstartDlg::OnCheckType() 
{
	BOOL bChecked = m_chkType.GetCheck()==1;	
	m_chkTypeFile.EnableWindow(bChecked);
	m_chkTypeDirectory.EnableWindow(bChecked);
}


static CString getFisrtLine(LPCTSTR pOrig)
{

	CString ret;
	if(pOrig==NULL)
		return ret;

	LPTSTR p = _tcsdup(pOrig);
	LPTSTR q=_tcschr(p, _T('\r'));
	if(q)
	{
		*q=0;
		ret = p;
	}
	else
	{
		q=_tcschr(p, _T('\n'));
		if(q)
		{
			*q=0;
			ret = p;
		}
		else
		{
			ret=p;
		}
	}
	free(p);
	return ret;
}

BOOL CWcfindstartDlg::loadCombo(CComboBox& combo, LPCTSTR pSection)
{
	if (!PathFileExists(theApp.GetFileNameDBPath()))
		return TRUE;

	vector<wstring> ss;
	int i = 0;
	if (!sqlGetPrivateProfileStringArray(L"Option", pSection, ss, theApp.GetFileNameDBPath()))
		return FALSE;

	BOOL bFailed = FALSE;
	for (vector<wstring>::iterator it = ss.begin(); it != ss.end(); ++it)
	{
		if (combo.FindString(0, it->c_str()) < 0)
			bFailed |= (-1 == combo.InsertString(i++, it->c_str()));
	}
	return !bFailed;
}

BOOL CWcfindstartDlg::saveCombo(CComboBox& combo, LPCTSTR pSection, const int maxCount)
{
	CString strCurrent;
	combo.GetWindowText(strCurrent);

	vector<wstring> ws;
	if(!strCurrent.IsEmpty())
		ws.push_back((LPCWSTR)getFisrtLine(strCurrent));

	const int nCount = maxCount < 0 ? 
		combo.GetCount() : 
		std::min(combo.GetCount(), maxCount);
	for ( int i=0 ; i < nCount ; ++i)
	{
		CString s;
		combo.GetLBText(i, s);
		if(!s.IsEmpty())
			ws.push_back((LPCWSTR)s);
	}

	return sqlWritePrivateProfileStringArray(L"Option", pSection, ws, theApp.GetFileNameDBPath());
}

void CWcfindstartDlg::OnDestroy() 
{
	BOOL bFailed = FALSE;
	bFailed |= !saveCombo(m_cmbFileName, L"FileName", MAX_SAVE_FILECOMBO);
	bFailed |= !saveCombo(m_cmbSize, L"Sizes", MAX_SAVE_SIZECOMBO);

	if (!IsIconic() && !IsZoomed())
	{
		CRect r;
		GetWindowRect(r);

		bFailed |= !theApp.WriteProfileInt(SECTION_OPTION, KEY_LOCATION_STARTDIALOG_X, r.left);
		bFailed |= !theApp.WriteProfileInt(SECTION_OPTION, KEY_LOCATION_STARTDIALOG_Y, r.top);
	}

	if(bFailed)
	{
		AfxMessageBox(I18N(L"Failed to save file names"));
	}


	CDialog::OnDestroy();
}


void CWcfindstartDlg::OnButtonInsertWcfindenv() 
{
	// Dialog to value
	UpdateData(TRUE);

	CMenu menu;
	menu.CreatePopupMenu();

	map<int, wstring> mapIS;

	int id = IDM_MENU_CIDSL_START;
	for(vector<wstring>::iterator it = vCsEnvs.begin() ; it != vCsEnvs.end(); ++it)
	{
		wstring val = stdGetenv(it->c_str());
		if(!val.empty())
		{
			menu.AppendMenu(MF_STRING, id, (*it + L" (" + val + L")").c_str());
			mapIS[id]=it->c_str();
			
			++id;
			if(id >= IDM_MENU_CIDSL_END)
				break;
		}
	}

	CRect r;
	m_btnInsertEnv.GetClientRect(r);
	m_btnInsertEnv.ClientToScreen(r);

	BOOL wID = menu.TrackPopupMenu(TPM_RETURNCMD,
		r.right,
		r.top,
		this,
		NULL);

	if(wID==0)
		return;

	CString s;
	
	s = mapIS[wID].c_str();
	s = L"%" + s + L"%";
	
	if (!m_strFolders.IsEmpty())
	{
		m_strFolders = m_strFolders.TrimRight();
		m_strFolders += L"\r\n";
	}
	m_strFolders += s;
	
	// Value to Dialog
	UpdateData(FALSE);
}

#include "ShowCommandDialog.h"
void CWcfindstartDlg::OnButtonShowcommandline() 
{
	UpdateData();
	CString param;
	CString strError;
	if(!getParameters(param, strError))
	{
		AfxMessageBox(strError);
		return;
	}

	CShowCommandDialog dlg;
	dlg.m_strCommandLine = param;
	dlg.DoModal();
}

void CWcfindstartDlg::OnCheckSize() 
{
	BOOL bChecked = m_chkSize.GetCheck()==1;	
	m_cmbSize.EnableWindow(bChecked);
}


void CWcfindstartDlg::OnBnClickedButtonDetailFilename()
{
	CDetailFilesDialog dlg;
	dlg.DoModal();
}


void CWcfindstartDlg::OnBnClickedButtonMatchwhollword()
{
	UpdateData(TRUE);
	m_strFileName = L"\\b" + m_strFileName + L"\\b";
	m_bRegexFilename = TRUE;
	UpdateData(FALSE);
}

void CWcfindstartDlg::OnBnClickedCheckExactfilename()
{
	if (m_chkExactName.GetCheck())
		m_chkRegex.SetCheck(0);
}


void CWcfindstartDlg::OnBnClickedCheckFilenameRegex()
{
	if (m_chkRegex.GetCheck())
		m_chkExactName.SetCheck(0);
}
