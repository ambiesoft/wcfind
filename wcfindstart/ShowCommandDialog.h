#include "afxwin.h"
#if !defined(AFX_SHOWCOMMANDDIALOG_H__3DD5C886_65E1_4DD1_BDE2_ACD4D861178F__INCLUDED_)
#define AFX_SHOWCOMMANDDIALOG_H__3DD5C886_65E1_4DD1_BDE2_ACD4D861178F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ShowCommandDialog.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CShowCommandDialog ダイアログ

class CShowCommandDialog : public CDialog
{
// コンストラクション
public:
	CShowCommandDialog(CWnd* pParent = NULL);   // 標準のコンストラクタ

// ダイアログ データ
	//{{AFX_DATA(CShowCommandDialog)
	enum { IDD = IDD_DIALOG_SHOWCOMMAND };
	CString	m_strCommandLine;
	//}}AFX_DATA


// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CShowCommandDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CShowCommandDialog)
		// メモ: ClassWizard はこの位置にメンバ関数を追加します。
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_editCommandLine;
	virtual BOOL OnInitDialog();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_SHOWCOMMANDDIALOG_H__3DD5C886_65E1_4DD1_BDE2_ACD4D861178F__INCLUDED_)
