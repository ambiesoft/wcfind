#if !defined(AFX_STDAFX_H__7C8FAF93_C171_4B22_A36D_955CED99C4BF__INCLUDED_)
#define AFX_STDAFX_H__7C8FAF93_C171_4B22_A36D_955CED99C4BF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN

//#ifdef _DEBUG
//#include <vld.h>
//#endif

#include <afx.h>
#include <afxwin.h>
#include <afxext.h>
#include <afxdtctl.h>
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <Rpcdce.h>
#include <Rpc.h>

#include <algorithm>
#include <cctype>
#include <cstdarg>
#include <functional>
#include <locale>

#pragma warning (disable : 4786)
#include <string>
#include <vector>
#include <map>
#include <memory>
#include <algorithm>

#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/any.hpp>

using namespace std;



#include "../../lsMisc/stdosd/stdosd.h"


#define CRLF _T("\r\n")
#include "../../lsMisc/I18N.h"

#include "../common/CommonDefine.h"
#include <afxcontrolbars.h>


using namespace Ambiesoft;
using namespace Ambiesoft::stdosd;


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++

#endif // !defined(AFX_STDAFX_H__7C8FAF93_C171_4B22_A36D_955CED99C4BF__INCLUDED_)
