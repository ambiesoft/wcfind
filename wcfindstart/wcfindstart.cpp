#include "stdafx.h"
#include "../../lsMisc/CreateFolderIniPath.h"
#include "../../lsMisc/tstring.h"
#include "../../lsMisc/SetCSIDLtoEnv.h"
#include "../../lsMisc/HighDPI.h"
#include "../../lsMisc/OpenCommon.h"

#include "../common/CLParse.h"
#include "../common/LangInfo.h"

#include "wcfindstart.h"
#include "wcfindstartDlg.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWcfindstartApp

BEGIN_MESSAGE_MAP(CWcfindstartApp, CWinApp)
	//{{AFX_MSG_MAP(CWcfindstartApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()


CWcfindstartApp::CWcfindstartApp()
{
	m_pszDBProfileName = NULL;
	
	InitHighDPISupport();
}

CWcfindstartApp theApp;

BOOL CWcfindstartApp::InitInstance()
{
	{
		TCHAR szT[MAX_PATH];


		if (!GetFolderIniDirEx(
			m_hInstance,
			szT,
			_countof(szT),
			L"Ambiesoft",
			AfxGetAppName()))
		{
			// I18N(_T("%s is not found. Exiting.")));
			AfxMessageBox(L"Failed to get ini folder.");
			return FALSE;
		}

		wstring strT = stdCombinePath(szT, _T("wcfind.ini"));

		free((void*)m_pszProfileName);
		m_pszProfileName = _tcsdup(strT.c_str());

		strT = stdCombinePath(szT, _T("wcfind.db"));
		free((void*)m_pszDBProfileName);
		m_pszDBProfileName = _tcsdup(strT.c_str());
	}



	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CString strLang;
	{
		int iLang = GetProfileInt(SECTION_OPTION, KEY_LANG, 0);
		strLang = GetLangInfo().GetLangAsISO3(iLang).c_str();
	}
	i18nInitLangmap(NULL, strLang, L"wcfind");

	CWcfindstartDlg dlg;
	CCLParse clparse;
	CCLParse::PARSEERROR error;
	CString strErrorInfo;
	if (!clparse.parse(__argc, __targv, error, strErrorInfo, &dlg))
	{
		if(error != CCLParse::PARSEERROR_NOFOLDER_SPECIFIED)
		{
			AfxMessageBox(CString(CCLParse::getErrorString(error)) + _T(" : ") + strErrorInfo);
			AfxMessageBox(CCLParse::getUsage(true), MB_ICONINFORMATION);
			return FALSE;
		}
	}
	if(clparse.IsHelp())
	{
		AfxMessageBox(CCLParse::getUsage(true), MB_ICONINFORMATION);
		return FALSE;
	}
	if (clparse.IsVersion())
	{
		if (!OpenCommon(nullptr, GetWcFindExe(), L"-v"))
		{
			AfxMessageBox(I18N(L"Failed to launch wcfind.exe"));
		}
		return FALSE;
	}
	
	

#if _MSC_VER < 1800
#ifdef _AFXDLL
	Enable3dControls();	
#else
	Enable3dControlsStatic();
#endif
#endif

	
	SetCSIDLtoEnv(L"WCFIND_", NULL, &dlg.vCsEnvs);

	
	dlg.m_pCLParse = &clparse;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
	}
	else if (nResponse == IDCANCEL)
	{
	}

	return FALSE;
}

int CWcfindstartApp::ExitInstance() 
{
	free((void*)m_pszDBProfileName);
	m_pszDBProfileName=NULL;

	return CWinApp::ExitInstance();
}
