//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by wcfindstart.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_WCFINDSTART_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDR_MENU_TMP                    129
#define IDD_DIALOG_SHOWCOMMAND          130
#define IDD_DIALOG_DETAIL_FILENAMES     132
#define IDC_EDIT_FOLDERS                1001
#define IDC_BUTTON_BROWSERFOLDER        1002
#define IDC_STATIC_FILENAME             1003
#define IDC_CHECK_LASTMODIFIED          1004
#define IDC_EDIT_LASTMODIFIED           1005
#define IDC_CHECK_TYPE                  1006
#define IDC_CHECK_TYPE_FILE             1007
#define IDC_CHECK_TYPE_DIRECTORY        1008
#define IDC_COMBO_FILENAME              1010
#define IDC_BUTTON_INSERT_WCFINDENV     1012
#define IDC_BUTTON_SHOWCOMMANDLINE      1013
#define IDC_EDIT_COMMANDLINE            1014
#define IDC_EDIT_CONTENT                1015
#define IDC_CHECK_SIZE                  1016
#define IDC_STATIC_SEARCHDIR            1018
#define IDC_BUTTON_DETAIL_FILENAME      1019
#define IDC_LIST_MAIN                   1020
#define IDC_CHECK_CONTENT_INVERT        1022
#define IDC_CHECK_FILENAME_REGEX        1023
#define IDC_CHECK_EXACTFILENAME         1024
#define IDC_COMBO_SIZE                  1025
#define IDC_BUTTON1                     1026
#define IDC_BUTTON_MATCHWHOLLWORD       1026
#define IDC_COMBO1                      1027
#define IDC_COMBO_FILENAMEORFULLNAME    1027
#define IDC_STATIC_NAME                 1028
#define IDC_STATIC_CONTENT              1029
#define IDC_COMBO2                      1031
#define IDC_COMBO_SORT                  1031
#define IDC_COMBO_SORT2                 1032
#define IDC_COMBO_SORTMODE              1032
#define IDM_MENU_CIDSL_START            32771
#define IDM_MENU_CIDSL_END              33770

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         33771
#define _APS_NEXT_CONTROL_VALUE         1032
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
