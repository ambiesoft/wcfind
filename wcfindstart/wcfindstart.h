#if !defined(AFX_WCFINDSTART_H__A225C514_7D19_476E_BD59_A322CB19D3EF__INCLUDED_)
#define AFX_WCFINDSTART_H__A225C514_7D19_476E_BD59_A322CB19D3EF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"

class CWcfindstartApp : public CWinApp
{
	CString m_strWcFind;
public:
	CWcfindstartApp();

	// ClassWizard
	//{{AFX_VIRTUAL(CWcfindstartApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL


	//{{AFX_MSG(CWcfindstartApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
	LPCTSTR m_pszDBProfileName;

public:
	LPCTSTR GetFileNameDBPath() const {
		return m_pszDBProfileName;
	}
	LPCWSTR GetWcFindExe() {
		if (m_strWcFind.IsEmpty())
		{
			TCHAR szT[MAX_PATH];
			GetModuleFileName(NULL, szT, sizeof(szT) / sizeof(szT[0]));
			*(_tcsrchr(szT, _T('\\')) + 1) = 0;
#ifdef _DEBUG
			_tcscat(szT, _T("wcfindD.exe"));
#else
			_tcscat(szT, _T("wcfind.exe"));
#endif
			m_strWcFind = szT;
		}
		return m_strWcFind;
	}

};

extern CWcfindstartApp theApp;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++

#endif // !defined(AFX_WCFINDSTART_H__A225C514_7D19_476E_BD59_A322CB19D3EF__INCLUDED_)
