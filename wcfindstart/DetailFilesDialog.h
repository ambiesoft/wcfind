#pragma once
#include "afxcmn.h"

#include "../XListCtrl/XListCtrl/XListCtrl.h"

// CDetailFilesDialog dialog

class CDetailFilesDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CDetailFilesDialog)

public:
	CDetailFilesDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDetailFilesDialog();

// Dialog Data
	enum { IDD = IDD_DIALOG_DETAIL_FILENAMES };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CXListCtrl m_lstMain;
	virtual BOOL OnInitDialog();
};
